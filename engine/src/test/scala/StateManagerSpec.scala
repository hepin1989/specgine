package com.specdevs.specgine.test

import com.specdevs.specgine.core._
import com.specdevs.specgine.input.{InputReceiver,InputProxy,AbstractInputManager}

import org.scalatest.{FunSpec,Matchers}

class GroupA extends StateGroup {
  var created = false
  var disposed = false
  var initialized = false
  def create(): Unit = {
    created = true
  }
  def dispose(): Unit = {
    disposed = true
  }
  def initialize(): Unit = {
    initialized = true
  }
}

class DummyStateOne extends State with DummyCallbacksImplementation {
  var lastDt = -1.0f
  var lastAlpha = -1.0f
  var lastX = 0
  var lastY = 0
  def process(dt: Float): Unit = {
    lastDt = dt
  }
  def render(alpha: Float): Unit = {
    lastAlpha = alpha
  }
  def resize(x: Int, y: Int): Unit = {
    lastX = x
    lastY = y
  }
}

class DummyStateTwo extends DummyStateOne with InputReceiver with InputProxy {
  var lastDtF = -1.0f
  var lastAlphaF = -1.0f
  var lastAlphaSF = -1.0f
  var proxy: Option[AbstractInputManager] = None
  override def processFrozen(dt: Float): Unit = {
    lastDtF = dt
  }
  override def renderFrozen(alpha: Float, simulationAlpha: Float): Unit = {
    lastAlphaF = alpha
    lastAlphaSF = simulationAlpha
  }
  def addReceivers(manager: AbstractInputManager): Unit = {
    proxy = Some(manager)
  }
}

class LoadingScreenOne extends LoadingScreen {
  var lastDt = -1.0f
  var lastAlpha = -1.0f
  var lastProcess = 0.0f
  var lastX = 0
  var lastY = 0

  def processLoading(dt: Float): Unit = {
    lastDt = dt
  }

  def renderLoading(alpha: Float, process: Float): Unit = {
    lastAlpha = alpha
    lastProcess = process
  }

  def reset(): Unit = {
    lastDt = -1.0f
    lastAlpha = -1.0f
    lastProcess = 0.0f
  }

  def resize(x: Int, y: Int): Unit = {
    lastX = x
    lastY = y
  }
}

class StateManagerSpec extends FunSpec with Matchers {
  implicit val sm = new StateManager
  val smu = new StateManagerUser with GetSetUser {}
  val am = new DummyAssetManager
  val cm = new DummyConfigManager
  val im = new DummyInputManager
  val ls1 = new LoadingScreenOne
  val ls2 = new LoadingScreenOne
  val gr1 = new GroupA
  val gr2 = new GroupA
  val gr3 = new GroupA
  val gr4 = new GroupA
  val gr5 = new GroupA
  val st1 = new DummyStateOne
  val st2 = new DummyStateTwo
  val st3 = new DummyStateOne
  val st4 = new DummyStateOne
  val st5 = new DummyStateOne

  describe("StateManager") {
    it("should check for initialization of AssetManager") {
      val thrown = intercept[IllegalStateException] {
        sm.assetManager
      }
      assert(thrown.getMessage === "You haven't set assetManager.")
      sm.assetManager = am
      sm.assetManager should equal (am)
    }

    it("should check for initialization of ConfigManager") {
      val thrown = intercept[IllegalStateException] {
        sm.configManager
      }
      assert(thrown.getMessage === "You haven't set configManager.")
      sm.configManager = cm
      sm.configManager should equal (cm)
    }

    it("should check for initialization of InputManager") {
      val thrown = intercept[IllegalStateException] {
        sm.inputManager
      }
      assert(thrown.getMessage === "You haven't set inputManager.")
      sm.inputManager = im
      sm.inputManager should equal (im)
    }

    it("should check for initialization of LoadingScreen") {
      var thrown = intercept[IllegalStateException] {
        sm.defaultLoader
      }
      assert(thrown.getMessage === "You haven't set defaultLoader.")
      thrown = intercept[IllegalStateException] {
        sm.userLoader
      }
      assert(thrown.getMessage === "You haven't set defaultLoader.")
      sm.defaultLoader = ls1
      sm.defaultLoader should equal (ls1)
      sm.userLoader should equal (ls1)
      sm.userLoader = ls2
      sm.defaultLoader should equal (ls1)
      sm.userLoader should equal (ls2)
    }

    it("should want to quit after initialization, before states are added") {
      sm.initialize()
      sm.shouldQuit should equal (true)
    }

    it("should validate groups before adding and initialize them after") {
      var thrown = intercept[IllegalStateException] {
        gr1.assetManager
      }
      assert(thrown.getMessage === "You haven't set assetManager.")
      thrown = intercept[IllegalStateException] {
        gr1.configManager
      }
      assert(thrown.getMessage === "You haven't set configManager.")
      var thrown2 = intercept[IllegalArgumentException] {
        sm.addGroup(gr1, "")
      }
      assert(thrown2.getMessage === "Group  is already added.")
      thrown2 = intercept[IllegalArgumentException] {
        sm.addGroup(gr1, "gr1", "no-such-parent")
      }
      assert(thrown2.getMessage === "Group no-such-parent doesn't exist.")
      sm.addGroup(gr1, "gr1")
      gr1.assetManager should equal (am)
      gr1.configManager should equal (cm)
      gr1.initialized should equal (false)
      gr1.created should equal (false)
      gr1.disposed should equal (false)
      sm.shouldQuit should equal (true)
    }

    it("should validate states before adding and initialize them after") {
      sm.create()
      var thrown = intercept[IllegalStateException] {
        st1.assetManager
      }
      assert(thrown.getMessage === "You haven't set assetManager.")
      thrown = intercept[IllegalStateException] {
        st1.configManager
      }
      assert(thrown.getMessage === "You haven't set configManager.")
      thrown = intercept[IllegalStateException] {
        st1.stateManager
      }
      assert(thrown.getMessage === "You haven't set stateManager.")
      var thrown2 = intercept[IllegalArgumentException] {
        sm.addState(st1, "st1", "no-such-parent")
      }
      assert(thrown2.getMessage === "Group no-such-parent doesn't exist.")
      sm.addState(st1, "st1")
      thrown2 = intercept[IllegalArgumentException] {
        sm.addState(st2, "st1", "gr1")
      }
      assert(thrown2.getMessage === "State st1 is already added.")
      sm.addState(st2, "st2", "gr1")
      sm.addState(st3, "st3", "gr1")
      st1.stateManager should equal (sm)
      st1.configManager should equal (cm)
      st1.assetManager should equal (am)
      st1.initialized should equal (false)
      st1.created should equal (false)
      st1.disposed should equal (false)
      st1.inside should equal (false)
      st1.lastDt should equal (-1.0f)
      st1.lastAlpha should equal (-1.0f)
      st1.lastX should equal (0)
      st1.lastY should equal (0)
      sm.shouldQuit should equal (false)
    }

    it("should initialize default state when created") {
      sm.create()
      st1.initialized should equal (true)
      st2.initialized should equal (false)
      gr1.initialized should equal (false)
    }

    it("should verify push arguments and initialize correct groups after process") {
      val thrown = intercept[IllegalArgumentException] {
        sm.push("no-such-state")
      }
      assert(thrown.getMessage === "State no-such-state doesn't exist.")
      sm.push("st2")
      sm.process(1.0f)
      st1.lastDt should equal (1.0f)
      st2.lastDt should equal (-1.0f)
      st2.initialized should equal (true)
      st2.proxy should equal (Some(im))
      gr1.initialized should equal (true)
    }

    it("should want to quit after poping last state or after quit") {
      sm.pop()
      sm.pop()
      sm.process(0.0f)
      sm.shouldQuit should equal (true)
      sm.push("st1")
      sm.process(0.0f)
      sm.shouldQuit should equal (false)
      sm.quit()
      sm.process(0.0f)
      sm.shouldQuit should equal (true)
    }

    it("should issue push for change on empty stack") {
      val thrown = intercept[IllegalArgumentException] {
        sm.change("no-such-state")
      }
      assert(thrown.getMessage === "State no-such-state doesn't exist.")
      sm.change("st1")
      sm.push("st2")
      sm.process(0.6f)
      st1.lastDt should equal (0.6f)
    }

    it("should resize current state") {
      sm.push("st3")
      sm.process(0.0f)
      sm.resize(800, 600)
      st3.lastX should equal (800)
      st3.lastY should equal (600)
      st1.lastX should equal (0)
      st1.lastY should equal (0)
    }

    it("should flatten stack for push when state is already on") {
      sm.push("st1")
      sm.process(0.6f)
      st1.inside should equal (true)
      st3.inside should equal (false)
    }

    it("should not fail to process or render states") {
      sm.push("st2")
      sm.process(0.0f)
      sm.process(0.5f)
      sm.render(0.5f)
      st2.lastDt should equal (0.5f)
      st2.lastAlpha should equal (0.5f)
      sm.processFrozen(0.5f)
      sm.renderFrozen(0.5f)
      sm.push("st1")
      sm.process(0.5f)
      sm.processFrozen(0.5f)
      sm.renderFrozen(0.5f)
      sm.change("st2")
      sm.process(0.5f)
      sm.render(0.5f)
      sm.push("st1")
      sm.process(0.75f)
      sm.processFrozen(0.75f)
      sm.renderFrozen(0.75f)
      st2.lastDtF should equal (0.75f)
      st2.lastAlphaF should equal (0.75f)
      st2.lastAlphaSF should equal (0.5f)
    }

    it("should leave states when disposing") {
      sm.push("st1")
      sm.process(0.0f)
      sm.dispose()
      st1.inside should equal (false)
      st2.inside should equal (false)
      st3.inside should equal (false)
      sm.shouldQuit should equal (true)
    }

    it("should process and render during loading, updating correct screens") {
      am.isLoading = true
      sm.create()
      ls1.lastX should equal (0)
      ls1.lastY should equal (0)
      ls1.lastProcess should equal (0.0f)
      sm.resize(800, 600)
      sm.process(0.5f)
      sm.render(0.5f)
      ls1.lastX should equal (800)
      ls1.lastY should equal (600)
      ls1.lastDt should equal (0.5f)
      ls1.lastAlpha should equal (0.5f)
      ls1.lastProcess should equal (1.0f)
      ls2.lastX should equal (0)
      ls2.lastY should equal (0)
      ls2.lastProcess should equal (0.0f)
      am.isLoading = false
      sm.render(0.5f)
      am.isLoading = true
      sm.push("st2")
      sm.process(0.0f)
      sm.resize(600, 800)
      sm.process(0.5f)
      sm.render(0.5f)
      am.isLoading = false
      sm.render(0.5f)
      ls2.lastX should equal (600)
      ls2.lastY should equal (800)
      ls2.lastDt should equal (0.5f)
      ls2.lastAlpha should equal (0.5f)
      ls2.lastProcess should equal (1.0f)
    }

    it("should not fail to issue state dependent commands, when stack is empty") {
      sm.dispose()
      sm.pop()
      sm.resize(0, 0)
      sm.process(0.0f)
      sm.render(0.0f)
      sm.dispose()
    }

    it("should be possible to register profiler getters"){
      sm.getProfilingData should equal (None)
      sm.registerProfilerGetter(Unit=>Some(ProfilingData()))
      sm.getProfilingData should equal (Some(
        ProfilingData(0, 0, 0, 0, 0.0f, 0.0f, 0, 0.0f, 0, 0.0f, true, 0.0f, 0.0f, 0, 0.0f)
      ))
    }
  }

  describe("StateManagerUser") {
    it("should check for initialization of StateManager") {
      val thrown = intercept[IllegalStateException] {
        smu.stateManager
      }
      assert(thrown.getMessage === "You haven't set stateManager.")
      smu.stateManager = sm
      smu.stateManager should equal (sm)
    }

    it("should pass state requests to StateManager") {
      smu.pushState("st1")
      st1.inside should equal (false)
      sm.process(0.0f)
      st1.inside should equal (true)
      smu.changeState("st2")
      st2.inside should equal (false)
      sm.process(0.0f)
      st1.inside should equal (false)
      st2.inside should equal (true)
      smu.pushState("st3")
      sm.process(0.0f)
      st3.inside should equal (true)
      smu.processFrozenState(0.0f)
      smu.renderFrozenState(0.0f)
      st2.lastDtF should equal (0.0f)
      st2.lastAlphaF should equal (0.0f)
      smu.processFrozenState(0.5f)
      smu.renderFrozenState(0.5f)
      st2.lastDtF should equal (0.5f)
      st2.lastAlphaF should equal (0.5f)
      smu.popState()
      sm.process(0.0f)
      st3.inside should equal (false)
      smu.quit()
      sm.shouldQuit should equal (false)
      sm.process(0.0f)
      sm.shouldQuit should equal (true)
    }

    it("should check for empty state stack and common root when getting StateInfo") {
      sm.dispose()
      sm.addGroup(gr2, "gr2", "gr2")
      sm.addGroup(gr3, "gr3", "gr2")
      sm.addGroup(gr4, "gr4", "gr3")
      sm.addGroup(gr5, "gr5", "gr2")
      sm.addState(st4, "st4", "gr4")
      sm.addState(st5, "st5", "gr5")
      val thrown = intercept[IllegalStateException] {
        smu.getStateInfo("value1")
      }
      assert(thrown.getMessage === "Cannot get nor set StateInfo when stack is empty.")
      sm.push("st4")
      sm.process(0.0f)
      smu.getStateInfo("value1") should equal (None)
      val thrown2 = intercept[IllegalArgumentException] {
        smu.getStateInfo("value1", Some("gr5"))
      }
      assert(thrown2.getMessage === "Group gr5 isn't parent of currently active state.")
      smu.getStateInfo("value1", Some("gr2")) should equal (None)
    }

    it("should be able to set StateInfo") {
      smu.setStateInfo("value1", BooleanStateInfo(true))
      smu.getStateInfo("value1") should equal (Some(BooleanStateInfo(true)))
    }

    it("should provide StateInfo trough Getter and Setter interfaces") {
      smu.set[StateInfo,Boolean]("value1", true, "gr4")
      smu.getOption[StateInfo,Boolean]("value1", "gr4") should equal (Some(true))
      smu.get[StateInfo,Boolean]("value1", "gr4") should equal (true)
      smu.getOption[StateInfo,Boolean]("no-such-value", "gr4") should equal (None)
      val thrown = intercept[NoSuchElementException] {
        smu.get[StateInfo,Boolean]("no-such-value", "gr4")
      }
      assert(thrown.getMessage === "failed to get value no-such-value")
      smu.getOption[StateInfo,Boolean]("value1", "gr3") should equal (None)
      smu.setIfNotSet[StateInfo,Boolean]("value1", false, Some("gr4"))
      smu.setIfNotSet[StateInfo,Boolean]("value1", false, Some("gr3"))
      smu.get[StateInfo,Boolean]("value1", "gr3") should equal (false)
      smu.get[StateInfo,Boolean]("value1", "gr4") should equal (true)
    }

    it("should set and not confuse various types of StateInfo") {
      smu.setIn[StateInfo,Boolean]("boolean", true)
      smu.setIn[StateInfo,Float]("float", 1.0f)
      smu.setIn[StateInfo,Int]("int", 1)
      smu.setIn[StateInfo,Long]("long", 1L)
      smu.setIn[StateInfo,String]("string", "1")
      smu.getFrom[StateInfo,Boolean]("boolean") should equal (true)
      smu.getFrom[StateInfo,Float]("float") should equal (1.0f)
      smu.getFrom[StateInfo,Int]("int") should equal (1)
      smu.getFrom[StateInfo,Long]("long") should equal (1L)
      smu.getFrom[StateInfo,String]("string") should equal ("1")
      smu.getFromOption[StateInfo,Boolean]("string") should equal (None)
      smu.getFromOption[StateInfo,Float]("boolean") should equal (None)
      smu.getFromOption[StateInfo,Int]("float") should equal (None)
      smu.getFromOption[StateInfo,Long]("int") should equal (None)
      smu.getFromOption[StateInfo,String]("long") should equal (None)
    }

    it("should not change registered profiling data if profiling is not enabled"){
      smu.getProfilingData should equal (Some(
        ProfilingData(0, 0, 0, 0, 0.0f, 0.0f, 0, 0.0f, 0, 0.0f, true, 0.0f, 0.0f, 0, 0.0f)
      ))
    }
  }
}
