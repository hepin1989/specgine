package com.specdevs.specgine.test

import com.specdevs.specgine.core._

import org.scalatest.{FunSpec,Matchers}

class ConfigManagerUserSpec extends FunSpec with Matchers {
  implicit val cm = new DummyConfigManager
  val cmu = new ConfigManagerUser with GetSetUser {}

  describe("ConfigManagerUser") {
    it("should check for initialization of ConfigManager") {
      val thrown = intercept[IllegalStateException] {
        cmu.configManager
      }
      assert(thrown.getMessage === "You haven't set configManager.")
      cmu.configManager = cm
      cmu.configManager should equal (cm)
    }

    it("should correctly call store on ConfigManager") {
      cm.stored should equal (false)
      cmu.storeConfig()
      cm.stored should equal (true)
    }

    it("should be able to set and get config") {
      cmu.getConfig("test") should equal (None)
      val thrown = intercept[NoSuchElementException] {
        cmu.get[Config,Boolean]("test")
      }
      assert(thrown.getMessage === "failed to get value test")
      cmu.setConfig("test", BooleanConfig(true))
      cmu.getConfig("test") should equal (Some(BooleanConfig(true)))
      cmu.setIfNotSet[Config,Boolean]("test", false)
      cmu.setIfNotSet[Config,Boolean]("next", false)
      cmu.get[Config,Boolean]("test") should equal (true)
      cmu.get[Config,Boolean]("next") should equal (false)
    }

    it("should set and not confuse various types of Config") {
      cmu.set[Config,Boolean]("boolean", true)
      cmu.set[Config,Float]("float", 1.0f)
      cmu.set[Config,Int]("int", 1)
      cmu.set[Config,Long]("long", 1L)
      cmu.set[Config,String]("string", "1")
      cmu.get[Config,Boolean]("boolean") should equal (true)
      cmu.get[Config,Float]("float") should equal (1.0f)
      cmu.get[Config,Int]("int") should equal (1)
      cmu.get[Config,Long]("long") should equal (1L)
      cmu.get[Config,String]("string") should equal ("1")
      cmu.getOption[Config,Boolean]("string") should equal (None)
      cmu.getOption[Config,Float]("boolean") should equal (None)
      cmu.getOption[Config,Int]("float") should equal (None)
      cmu.getOption[Config,Long]("int") should equal (None)
      cmu.getOption[Config,String]("long") should equal (None)
    }
  }
}
