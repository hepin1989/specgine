package com.specdevs.specgine.test

import com.specdevs.specgine.core.{selectImplementation,EagerSelector,LazySelector}

import org.scalatest.{FunSpec,Matchers}

object ImplementationState {
  var version = 1
  var calledBasic = false
  var calledLazy = false
}

class Base(val name: String)

class BasicSelector extends Base("basic") with EagerSelector[Base] {
  ImplementationState.calledBasic = true
  def isValid: Boolean = (ImplementationState.version == 1)
}

class OtherBase extends Base("lazy") {
  ImplementationState.calledLazy = true
}

class AlternativeSelector extends LazySelector[Base](new OtherBase) {
  def isValid: Boolean = (ImplementationState.version == 2)
}

class ImplementationSelectorSpec extends FunSpec with Matchers {
  describe("ImplementationSelector") {
    it("should be possible to select one of implementations") {
      val base = selectImplementation(new BasicSelector, new AlternativeSelector)
      base.get.name should equal ("basic")
    }

    it("should not initialize lazy selectors when not created") {
      ImplementationState.calledLazy should equal (false)
      ImplementationState.calledBasic should equal (true)
    }

    it("should pick alternative implementation when conditions for first one are not met") {
      ImplementationState.version = 2
      val base = selectImplementation(new BasicSelector, new AlternativeSelector)
      base.get.name should equal ("lazy")
      ImplementationState.calledLazy should equal (true)
    }

    it("should return None when no implementations met requirements") {
      ImplementationState.version = 3
      selectImplementation(new BasicSelector, new AlternativeSelector) should equal (None)
    }
  }
}
