package com.specdevs.specgine.test

import com.specdevs.specgine.assets.{Asset,AssetManagerUser}
import com.specdevs.specgine.core.{GetSetUser,Provider}

import org.scalatest.{FunSpec,Matchers}
import org.scalatest.concurrent.AsyncAssertions

import scala.util.{Failure,Success}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class AssetManagerUserSpec extends FunSpec with Matchers with AsyncAssertions {
  val amu = new AssetManagerUser with GetSetUser {}
  implicit val am = new DummyAssetManager

  implicit object DummyAssetProvider extends Provider[Asset,String] {
    def unpack(value: Option[Asset]): Option[String] = {
      value match {
        case Some(DummyAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: String): Asset = DummyAsset(value)
  }

  implicit object OtherDummyAssetProvider extends Provider[Asset,Int] {
    def unpack(value: Option[Asset]): Option[Int] = {
      value match {
        case Some(OtherDummyAsset(value)) => Some(value)
        case _ => None
      }
    }
    def pack(value: Int): Asset = OtherDummyAsset(value)
  }

  describe("AssetManagerUser") {
    it("should check for initialization of AssetManager") {
      val thrown = intercept[IllegalStateException] {
        amu.assetManager
      }
      assert(thrown.getMessage === "You haven't set assetManager.")
      amu.assetManager = am
      amu.assetManager should equal (am)
    }

    it("should not report assets when nothing is loaded") {
      amu.isAssetLoaded("1") should equal (false)
      amu.getAsset("1") should equal (None)
      val future = amu.getFutureAsset("1")
      val waiter = new Waiter
      future onComplete {
        case Failure(e) => waiter(throw e); waiter.dismiss()
        case Success(_) => waiter.dismiss()
      }
      intercept[IllegalStateException] {
        waiter.await
      }
      ()
    }

    it("should be possible to load and set local assets") {
      amu.loadAsset("1")
      amu.loadAsset("2")
      amu.setAsset("2", DummyAsset("local"))
      amu.setAsset("3", DummyAsset("local"))
      amu.setFutureAsset("4", Future.successful[Asset](DummyAsset("1")))
      amu.isAssetLoaded("1") should equal (true)
      amu.isAssetLoaded("2") should equal (true)
      amu.isAssetLoaded("3") should equal (true)
      amu.isAssetLoaded("4") should equal (true)
    }

    it("should be possible to get loaded asset") {
      amu.getAsset("1") match {
        case Some(DummyAsset(x)) => assert(x === "1")
        case _ => fail("failed to get asset 1")
      }
      amu.getAsset("2") match {
        case Some(DummyAsset(x)) => assert(x === "local")
        case _ => fail("failed to get asset 2")
      }
      amu.getAsset("3") match {
        case Some(DummyAsset(x)) => assert(x === "local")
        case _ => fail("failed to get asset 3")
      }
      val future = amu.getFutureAsset("1")
      val waiter = new Waiter
      future onComplete {
        case Failure(_) => waiter.dismiss()
        case Success(v) => waiter {
          v match {
            case DummyAsset(x) => assert(x === "1")
            case _ => fail("failed to get asset 1")
          }
        }; waiter.dismiss()
      }
      waiter.await()
    }

    it("should issue tasks") {
      val asset = DummyAsset("dummy")
      amu.setAsset("dummy", asset)
      amu.addTask { asset.value = "changed" }
      asset.value should equal ("changed")
      amu.addTask(Future {
        Thread.sleep(10)
        asset.value = "again"
      })
      asset.value should equal ("again")
    }

    it("should dispose locally added assets, but not globally loaded trough asset manager") {
      val asset1 = amu.getAsset("1") match {
        case Some(x @ DummyAsset(_)) => x
        case _ => fail("failed to get asset 1")
      }
      val asset2 = amu.getAsset("2") match {
        case Some(x @ DummyAsset(_)) => x
        case _ => fail("failed to get asset 2")
      }
      amu.disposeAssets()
      asset1.value should equal ("1")
      asset2.value should equal ("")
    }

    it("should expose AssetManager interface of Getters and Setters") {
      amu.setAsset("1", DummyAsset("1"))
      var future = amu.getFuture[Asset,String]("1")
      var waiter = new Waiter
      future onComplete {
        case Failure(_) => waiter.dismiss()
        case Success(v) => waiter {
          v should equal ("1")
        }; waiter.dismiss()
      }
      waiter.await()
      amu.setFuture[Asset,String]("one", Future.successful[String]("1"))
      future = amu.getFuture[Asset,String]("one")
      waiter = new Waiter
      future onComplete {
        case Failure(_) => waiter.dismiss()
        case Success(v) => waiter {
          v should equal ("1")
        }; waiter.dismiss()
      }
      waiter.await()
      var future2 = amu.getFuture[Asset,Int]("no-such-resource")
      waiter = new Waiter
      future2 onComplete {
        case Failure(e) => waiter { throw e }; waiter.dismiss()
        case Success(v) => waiter.dismiss()
      }
      var thrown = intercept[IllegalStateException] {
        waiter.await()
      }
      future2 = amu.getFuture[Asset,Int]("1")
      waiter = new Waiter
      future2 onComplete {
        case Failure(e) => waiter { throw e }; waiter.dismiss()
        case Success(v) => waiter.dismiss()
      }
      thrown = intercept[IllegalStateException] {
        waiter.await()
      }
      future2 = amu.getFuture[Asset,Int]("no-such-resource")
      amu.setFuture[Asset,Int]("bad", Future.failed[Int](new IllegalStateException))
      amu.getOption[Asset,Int]("bad") should equal (None)
    }
  }
}
