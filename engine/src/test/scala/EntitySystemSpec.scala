package com.specdevs.specgine.test

import com.specdevs.specgine.states._

import org.scalatest.{FunSpec,Matchers}

class EntitySystemSpec extends FunSpec with Matchers {
  class DummyVoidEntitySystem extends VoidEntitySystem with DummyCallbacksImplementation
  class DummyComponentsSpec extends ComponentsSpec
  class DummyEntitySystem extends EntitySystem with DummyCallbacksImplementation {
    var added = false
    def addEntityToSystem(e: Entity, cs: Set[Component]): Unit = {
      added = true
    }
    def removeEntityFromSystem(e: Entity): Unit = {
      added = false
    }
    def systemHasEntity(e: Entity): Boolean = added
    def systemWantsEntity(cs: Set[Component]): Boolean = false
    val inputPriority = 0
    val processPriority = 0
    val renderPriority = 0
  }
  class DummyProcessorA extends Processor0
  class DummyProcessorB extends Processor1 with DummyEntityLoop1
  class DummyProcessorC extends Processor2 with DummyEntityLoop2
  class DummyRendererA extends Renderer0
  class DummyRendererB extends Renderer1 with DummyEntityLoop1
  class DummyRendererC extends Renderer2 with DummyEntityLoop2

  val dummyComponent = new TeamA
  val dummyEntity = Entity(42)
  val dummyVoidEntitySystem = new DummyVoidEntitySystem
  val dummyEntitySystem = new DummyEntitySystem
  val dummyAssetManager = new DummyAssetManager
  val dummyTweenManager = new DummyTweenManager
  val dummyProcessor0 = new DummyProcessorA
  val dummyProcessor1 = new DummyProcessorB
  val dummyProcessor2 = new DummyProcessorC
  val dummyRenderer0 = new DummyRendererA
  val dummyRenderer1 = new DummyRendererB
  val dummyRenderer2 = new DummyRendererC

  describe("VoidEntitySystem") {
    it("should not request any components") {
      dummyVoidEntitySystem.systemWantsEntity(Set(dummyComponent)) should equal (false)
    }

    it("should not contain any entities") {
      dummyVoidEntitySystem.addEntityToSystem(dummyEntity, Set(dummyComponent))
      dummyVoidEntitySystem.systemHasEntity(dummyEntity) should equal (false)
    }

    it("should not remove any entity") {
      dummyVoidEntitySystem.removeEntityFromSystem(dummyEntity)
      ()
    }
  }

  describe("ComponentsSpec") {
    it("should be created and its group should return None when group method is not overwritten") {
      val dummyComponentsSpec = new DummyComponentsSpec
      dummyComponentsSpec.group(Entity(1)) should equal (None)
    }
  }

  describe("EntitySystem") {
    it("should check calling of basic methods") {
      dummyEntitySystem.doCreate()
      dummyEntitySystem.created should equal (true)
      dummyEntitySystem.doEnter()
      dummyEntitySystem.inside should equal (true)
      dummyEntitySystem.doInitialize()
      dummyEntitySystem.initialized should equal (true)
      dummyEntitySystem.doLeave()
      dummyEntitySystem.inside should equal (false)
      val thrownAsset = intercept[IllegalStateException] {
        dummyEntitySystem.doDispose()
      }
      assert(thrownAsset.getMessage === "You haven't set assetManager.")
      dummyEntitySystem.assetManager = dummyAssetManager
      dummyEntitySystem.doDispose()
      dummyEntitySystem.resizeFrozen(4, 2)
      dummyEntitySystem.resize(4, 2)
      dummyEntitySystem.disposed should equal (true)
    }

    it("should add and remove an entity") {
      dummyEntitySystem.added should equal (false)
      dummyEntitySystem.addEntityToSystem(dummyEntity, Set(dummyComponent))
      dummyEntitySystem.added should equal (true)
      dummyEntitySystem.removeEntityFromSystem(dummyEntity)
      dummyEntitySystem.added should equal (false)
    }
  }

  describe("Processor0") {
    it("should check processing") {
      dummyProcessor0.processAll(0.1f)
      dummyProcessor0.process(0.1f)
      ()
    }
  }

  describe("Processor1") {
    it("should check processing") {
      dummyProcessor1.processAll(0.1f)
      dummyProcessor1.process(0.1f, dummyEntity)
      dummyProcessor1.looped should equal (true)
    }
  }

  describe("Processor2") {
    it("should check processing") {
      dummyProcessor2.processAll(0.1f)
      dummyProcessor2.process(0.1f, dummyEntity, dummyEntity)
      dummyProcessor2.looped should equal (true)
    }
  }

  describe("Renderer0") {
    it("should check rendering") {
      dummyRenderer0.renderAll(0.1f)
      dummyRenderer0.render(0.1f)
      ()
    }
  }

  describe("Renderer1") {
    it("should check rendering") {
      dummyRenderer1.renderAll(0.1f)
      dummyRenderer1.render(0.1f, dummyEntity)
      dummyRenderer1.beginRender(0.1f)
      dummyRenderer1.endRender(0.1f)
      dummyRenderer1.looped should equal (true)
    }
  }

  describe("Renderer2") {
    it("should check rendering") {
      dummyRenderer2.renderAll(0.1f)
      dummyRenderer2.render(0.1f, dummyEntity, dummyEntity)
      dummyRenderer2.beginRender(0.1f)
      dummyRenderer2.endRender(0.1f)
      dummyRenderer2.looped should equal (true)
    }
  }
}
