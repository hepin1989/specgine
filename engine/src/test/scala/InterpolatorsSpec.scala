package com.specdevs.specgine.test

import com.specdevs.specgine.animation.Interpolator
import com.specdevs.specgine.animation.interpolators._

import org.scalatest.{FunSpec,Matchers}

import math.{sqrt,sin,Pi}

class InterpolatorsSpec extends FunSpec with Matchers {
  describe("Linear") {
    it("should equal to argument passed") {
      val linear = Linear
      linear.apply(0.5f) should equal (0.5f)
      linear.apply(0.5f + 0.1f) should equal (0.6f)
    }
  }

  describe("Quad") {
    it("should equal to square of argument passed") {
      val quad = Quad
      quad.apply(0.5f) should equal (0.25f)
      quad.apply(0.5f + 0.1f) should equal (0.36f)
    }
  }

  describe("Power") {
    it("should equal to Linear interpolator") {
      val linear = Linear
      val power = Power(1f)
      power.apply(0.5f) should equal (linear.apply(0.5f))
      power.apply(0.5f + 0.1f) should equal (linear.apply(0.5f + 0.1f))
    }

    it("should equal to quadratic interpolator") {
      val quad = Quad
      val power = Power(2f)
      power.apply(0.5f) should equal (quad.apply(0.5f))
    }

    it("should equal to square root interpolator") {
      val power = Power(0.5f)
      power.apply(0.25f) should equal (0.5f)
    }
  }

  describe("Generic interpolator") {
    it("should equal to linear interpolator") {
      val genericLinearInterpolator = functionToInterpolator[Float]((x: Float) => x)
      val linear = Linear
      genericLinearInterpolator.apply(0.5f) should equal (linear.apply(0.5f))
      genericLinearInterpolator.apply(0.5f + 0.1f) should equal (linear.apply(0.5f + 0.1f))
    }

    it("should equal to quadratic interpolator") {
      case object GenericLinearInterpolator extends Interpolator[Float] { def apply(x: Float): Float = x*x}
      val genericLinearInterpolator = GenericLinearInterpolator
      val quad = Quad
      genericLinearInterpolator.apply(0.5f) should equal (quad.apply(0.5f))
      genericLinearInterpolator.apply(0.5f + 0.1f) should equal (quad.apply(0.5f + 0.1f))
    }

    it("should equal to sin(0.5*Pi*x) interpolator") {
      val genericLinearInterpolator = functionToInterpolator[Double]((x: Float) => sin(Pi * 0.5f * x))
      genericLinearInterpolator.apply(0.5f) should be (sin(Pi * 0.5f *0.5f) +- 0.000001f)
      genericLinearInterpolator.apply(0.5f + 0.1f) should be ((sin(Pi * 0.5f * (0.5f + 0.1f)) +- 0.000001f))
    }
  }
}
