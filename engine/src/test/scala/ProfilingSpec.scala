package com.specdevs.specgine.test

import org.scalatest.{FunSpec,Matchers}

import com.specdevs.specgine.core.ProfilingData

class ProfilingDataSpec extends FunSpec with Matchers {
  describe("ProfilingData"){
    it("should be stable by default"){
      val pd = ProfilingData()
      pd.stable should equal (true)
    }

    it("should calculate time difference") {
      val pd = ProfilingData()
      pd.animFrameCount = 5
      pd.avgAnimFrameTime = 0.2f
      pd.deltaTime = 2
      pd.timeDiff should equal (1)
    }
  }
}
