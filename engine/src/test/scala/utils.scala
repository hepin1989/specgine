package com.specdevs.specgine.test

import com.specdevs.specgine.assets.{AbstractAssetManager,Asset,AssetDescriptor}
import com.specdevs.specgine.core.{AbstractConfigManager,Config,StateManager,State}
import com.specdevs.specgine.states._
import com.specdevs.specgine.animation.TweenManager
import com.specdevs.specgine.input.{AbstractInputManager,InputReceiver}

import scala.concurrent.{Future,Promise,Await}
import scala.util.{Failure,Success}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.collection.mutable.HashMap

case class Position(var x: Float, var y: Float) extends Component

case class Velocity(var x: Float, var y: Float) extends Component

case class Acceleration(var x: Float, var y: Float) extends Component

case class TeamA() extends Component

case class TeamB() extends Component

case class TeamC() extends Component

case class DummyAsset(var value: String) extends Asset {
  def dispose(): Unit = {
    value = ""
  }
}

case class OtherDummyAsset(var value: Int) extends Asset {
  def dispose(): Unit = {
    value = -1
  }
}

class DummyAssetManager extends AbstractAssetManager {
  private val assets = new HashMap[String, Asset]
  private val assetsLocal = new HashMap[String, Asset]

  var isLoading = false

  def isLoaded(name: String): Boolean = assets.isDefinedAt(name) || assetsLocal.isDefinedAt(name)

  def unload(name: String): Unit = {
    assets -= name
    ()
  }

  def removeLocal(name: String): Unit = {
    assetsLocal -= name
    ()
  }

  def needsLoading: Boolean = isLoading

  def update(): Boolean = true

  def getPercentage: Float = 1.0f

  def registerDescriptor(added: AssetDescriptor*): Unit = {}

  def dispose(): Unit = {
    assets.clear()
    assetsLocal.clear()
  }

  def get(key: String): Option[Asset] = {
    if (assetsLocal.isDefinedAt(key)) {
      Some(assetsLocal(key))
    } else if (assets.isDefinedAt(key)) {
      Some(assets(key))
    } else {
      None
    }
  }

  def set(key: String, asset: Asset): Unit = {
    assetsLocal(key) = asset
  }

  def addTask(task: => Unit): Unit = {
    task
  }

  def addTask[T](task: Future[T]): Unit = {
    task onComplete { _ => () }
    Await.ready(task, Duration(1000, "millis"))
    ()
  }

  def setFuture(key: String, value: Future[Asset]): Unit = {
    value onComplete {
      case Success(v) => assetsLocal(key) = v
      case _ => ()
    }
    Await.ready(value, Duration(1000, "millis"))
    ()
  }

  def load(name: String, descriptor: AssetDescriptor): Future[Asset] = {
    assets(name) = DummyAsset(name)
    Future.successful[Asset](assets(name))
  }

  def getFuture(key: String): Future[Asset] = get(key) match {
    case Some(asset) => Future.successful[Asset](asset)
    case None => Future.failed[Asset](new IllegalStateException)
  }
}

class DummyConfigManager extends AbstractConfigManager {
  val storage = new HashMap[String,Config]

  var stored = false

  def store(): Unit = {
    stored = true
  }

  def get(key: String): Option[Config] = storage.get(key)

  def set(key: String, config: Config): Unit = {
    storage(key) = config
  }
}

class DummyInputManager extends AbstractInputManager {
  def reset(): Unit = {}

  def addReceiver(what: InputReceiver, priority: Int): Unit = {}
}

class DummyTweenManager extends TweenManager {
  var updated = false
  override def update(dt: Float): Unit = {
    super.update(dt)
    updated = true
  }
}

trait DummyCallbacksImplementation {
  var created = false
  var disposed = false
  var initialized = false
  var inside = false
  def create(): Unit = {
    created = true
  }
  def dispose(): Unit = {
    disposed = true
  }
  def initialize(): Unit = {
    initialized = true
  }
  def enter(): Unit = {
    inside = true
  }

  def leave(): Unit = {
    inside = false
  }
}

trait DummyEntityLoop1 extends EntityLoop1 {
  var looped = false
  def foreachEntity1(f: Entity => Unit): Unit = {
    looped = true
  }
}

trait DummyEntityLoop2 extends EntityLoop2 {
  var looped = false
  def foreachEntity2(f: (Entity, Entity) => Unit): Unit ={
    looped = true
  }
}

class DummyStateManager extends StateManager

class DummyMessage extends Message

class DummyReply extends Reply

class DummyWorldUser extends WorldUser

class DummyAbstractMenuScreen extends AbstractMenuScreen with DummyCallbacksImplementation {
  var processed = false
  var processedBackground = false
  var rendered = false
  var resized = false
  var renderedBackground = false
  def process(dt: Float): Unit = {
    processed = true
  }
  def render(alpha: Float): Unit = {
    rendered = true
  }
  def resize(x: Int, y: Int): Unit = {
    resized = true
  }
  def processBackground(dt: Float): Unit = {
    processedBackground = true
  }
  def renderBackground(alpha: Float): Unit = {
    renderedBackground = true
  }
}

class DummyMenuSwitcher extends MenuSwitcher {
  def changeMenu(name: String): Unit = {}
}
