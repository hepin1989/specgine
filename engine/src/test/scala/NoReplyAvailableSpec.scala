package com.specdevs.specgine.test

import com.specdevs.specgine.states._

import org.scalatest.{FunSpec,Matchers}

class NoReplyAvailableSpec extends FunSpec with Matchers {
  val dummyNoReplyAvailable = new NoReplyAvailable
  describe("NoReplyAvailable") {
    it("should return predefined string") {
      dummyNoReplyAvailable.toString should equal ("No reply available, looks like yr code isn't unassailable")
    }
  }
}
