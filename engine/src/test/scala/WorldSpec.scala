package com.specdevs.specgine.test

import com.specdevs.specgine.states._
import com.specdevs.specgine.input.{InputReceiver,InputProxy,AbstractInputManager}

import org.scalatest.{FunSpec,Matchers}

import org.scalatest.concurrent.AsyncAssertions.Waiter

import scala.util.{Failure,Success}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import scala.collection.mutable.HashMap

// @The[TeamA]
// @No[TeamB]
class PositionSpec extends ComponentsSpec with ComponentsManager {
  val pos = new HashMap[Entity,Position] // Needed[Position]
  val vel = new HashMap[Entity,Velocity] // Needed[Velocity]
  val acc = new HashMap[Entity,Option[Acceleration]] // Optional[Acceleration]

  override def group(e: Entity): Option[Int] = {
    if (pos(e).x > 0.5f) {
      Some(1)
    } else if (pos(e).x >= 0) {
      Some(2)
    } else {
      None
    }
  }

  def has(e: Entity): Boolean = pos.isDefinedAt(e) && vel.isDefinedAt(e)

  def remove(e: Entity): Unit = {
    pos.remove(e)
    vel.remove(e)
    acc.remove(e)
    ()
  }

  private def isPosition(c: Component): Boolean = c match {
    case (_: Position) => true
    case _             => false
  }

  private def isVelocity(c: Component): Boolean = c match {
    case (_: Velocity) => true
    case _             => false
  }

  private def isTeamA(c: Component): Boolean = c match {
    case (_: TeamA) => true
    case _          => false
  }

  private def isTeamB(c: Component): Boolean = c match {
    case (_: TeamB) => true
    case _          => false
  }

  def wants(cs: Set[Component]): Boolean = {
    cs.exists(isPosition) && cs.exists(isVelocity) && cs.exists(isTeamA) && !cs.exists(isTeamB)
  }

  def initOptional(e: Entity): Unit = acc.update(e, None)

  def addComponent(e: Entity, c: Component): Unit = c match {
    case (v @ (_: Position))     => pos.update(e,v)
    case (v @ (_: Velocity))     => vel.update(e,v)
    case (v @ (_: Acceleration)) => acc.update(e, Some(v))
    case _                       => ()
  }

  def add(e: Entity, cs: Set[Component]): Unit = {
    initOptional(e)
    cs.foreach(c => addComponent(e,c))
  }
}

class PositionNonProcessableSystem extends SingleEntitySystem with DummyCallbacksImplementation {
  val components = new PositionSpec

  override def process(dt: Float, e: Entity): Unit = {
    components.pos(e).x += components.vel(e).x*dt
    components.pos(e).y += components.vel(e).y*dt
  }
}

class PositionSystem extends PositionNonProcessableSystem with Processor

class DummySystem extends PositionSystem with Renderer with Receiver with Responder {
  def respond(message: Message): Option[Reply] = None
  def receive(message: Message): Unit = {}

  override def render(alpha: Float, e: Entity): Unit = {}
}

class WorldSpec extends FunSpec with Matchers {
  val world = new World
  val dummyMessage = new DummyMessage
  val dummyWorldUser = new DummyWorldUser
  val ps = new PositionSystem
  val psNonProcessable = new PositionNonProcessableSystem
  val dummyStateManager = new DummyStateManager
  val dummyConfigManager = new DummyConfigManager
  val dummyAssetManager = new DummyAssetManager
  val dummyTweenManager = new DummyTweenManager
  val dummyInputManager = new DummyInputManager

  world.stateManager = dummyStateManager
  world.configManager = dummyConfigManager
  world.assetManager = dummyAssetManager
  world.tweenManager = dummyTweenManager
  world.addSystem(psNonProcessable)

  val player = world.createEntity(Position(0, 0), Velocity(1, 1), TeamA())
  val player2 = world.createEntity(Position(1, 1), Velocity(2, 2), Acceleration(1, 0), TeamA())
  val player3 = world.createEntity(Position(1, 1), Velocity(2, 2), Acceleration(1, 0), TeamB())
  val player4 = world.createEntity(Position(0, 0), Velocity(1, 1))
  val player5 = world.createEntity(Position(-5, 0), Velocity(1, 1), TeamA())

  describe("World") {
    it("should process 0.1f forward but not affect components") {
      world.process(0.1f)
      psNonProcessable.components.pos(player) should equal (Position(0, 0))
      psNonProcessable.components.vel(player) should equal (Velocity(1, 1))
      psNonProcessable.components.pos(player2) should equal (Position(1, 1))
      psNonProcessable.components.vel(player2) should equal (Velocity(2, 2))
    }

    it("should process 0.1f forward and change components") {
      world.addSystem(ps)
      world.process(0.1f)
      ps.components.pos(player) should equal (Position(0.1f, 0.1f))
      ps.components.vel(player) should equal (Velocity(1, 1))
      ps.components.pos(player2) should equal (Position(1.2f, 1.2f))
      ps.components.vel(player2) should equal (Velocity(2, 2))
    }

    it("should process 0.1f forward and again change components") {
      world.process(0.1f)
      ps.components.pos(player) should equal (Position(0.2f, 0.2f))
      ps.components.vel(player) should equal (Velocity(1, 1))
      ps.components.pos(player2).x should be (1.4f +- 0.00001f)
      ps.components.pos(player2).y should be (1.4f +- 0.00001f)
      ps.components.vel(player2) should equal (Velocity(2, 2))
    }
  }

  describe("Both Position Systems") {
    it("should have player and player2") {
      ps systemHasEntity player should equal (true)
      ps systemHasEntity player2 should equal (true)
      psNonProcessable systemHasEntity player should equal (true)
      psNonProcessable systemHasEntity player2 should equal (true)
    }

    it("should not have player3") {
      ps systemHasEntity player3 should equal (false)
      psNonProcessable systemHasEntity player3 should equal (false)
    }

    it("should not have tempEntity and after removing it should not have it as well") {
      val tempEntity = world.createEntity(Position(1, 1), Velocity(2, 2), Acceleration(1, 0), TeamB())
      ps systemHasEntity tempEntity should equal (false)
      ps removeEntityFromSystem tempEntity
      ps systemHasEntity tempEntity should equal (false)
    }

    it("should not have tempEntity initially, but should have when removing TeamB") {
      val tempEntity = world.createEntity(Position(1, 1), Velocity(2, 2), Acceleration(1, 0), TeamA(), TeamB())
      ps systemHasEntity tempEntity should equal (false)
      psNonProcessable systemHasEntity tempEntity should equal (false)
      def removeTeamB(c: Component): Boolean = {
        val rule = c match {
          case (_: TeamB) => false
          case _          => true
        }
        rule
      }
      world.filterComponents(tempEntity, removeTeamB)
      ps systemHasEntity tempEntity should equal (true)
      psNonProcessable systemHasEntity tempEntity should equal (true)
    }

    it("should add tempEntity and then should remove it after removing TeamA") {
      val tempEntity = world.createEntity(Position(0, 0), Velocity(1, 1), TeamA())
      ps systemHasEntity tempEntity should equal (true)
      psNonProcessable systemHasEntity tempEntity should equal (true)
      def removeTeamA(c: Component): Boolean = {
        val rule = c match {
          case (_: TeamA) => false
          case _          => true
        }
        rule
      }
      world.filterComponents(tempEntity, removeTeamA)
      ps systemHasEntity tempEntity should equal (false)
      psNonProcessable systemHasEntity tempEntity should equal (false)
    }

    it("should not add tempEntity and should not have it after removing Acceleration") {
      val tempEntity = world.createEntity(Position(0, 0), Velocity(1, 1), TeamB(), Acceleration(1, 0))
      ps systemHasEntity tempEntity should equal (false)
      psNonProcessable systemHasEntity tempEntity should equal (false)
      def removeAcceleration(c: Component): Boolean = {
        val rule = c match {
          case (_: Acceleration) => false
          case _          => true
        }
        rule
      }
      world.filterComponents(tempEntity, removeAcceleration)
      ps systemHasEntity tempEntity should equal (false)
      psNonProcessable systemHasEntity tempEntity should equal (false)
    }

    it("should not have player4 initially, but should have it after adding TeamA") {
      ps systemHasEntity player4 should equal (false)
      psNonProcessable systemHasEntity player4 should equal (false)
      world.addComponents(player4, TeamA())
      ps systemHasEntity player4 should equal (true)
      psNonProcessable systemHasEntity player4 should equal (true)
    }

    it("should add entity and then remove it") {
      val tempPlayer = world.createEntity(Position(0, 0), Velocity(1, 1), TeamA())
      ps systemHasEntity tempPlayer should equal (true)
      psNonProcessable systemHasEntity tempPlayer should equal (true)
      world.removeEntity(tempPlayer)
      ps systemHasEntity tempPlayer should equal (false)
      psNonProcessable systemHasEntity tempPlayer should equal (false)
    }

    it("should want entity with set of TeamA, Position, Velocity components plus optionally Acceleration") {
      ps systemWantsEntity Set(Position(0, 0), Velocity(1, 1), TeamA(), Acceleration(1, 0)) should equal (true)
      ps systemWantsEntity Set(Position(0, 0), Velocity(1, 1), TeamA()) should equal (true)
      psNonProcessable systemWantsEntity
        Set(Position(0, 0), Velocity(1, 1), TeamA(), Acceleration(1, 0)) should equal (true)
      psNonProcessable systemWantsEntity Set(Position(0, 0), Velocity(1, 1), TeamA()) should equal (true)
    }

    it("should not want entity without TeamA") {
      ps systemWantsEntity Set(Position(0, 0), Velocity(1, 1), Acceleration(1, 0)) should equal (false)
      ps systemWantsEntity Set(Position(0, 0), Velocity(1, 1)) should equal (false)
      psNonProcessable systemWantsEntity Set(Position(0, 0), Velocity(1, 1), Acceleration(1, 0)) should equal (false)
      psNonProcessable systemWantsEntity Set(Position(0, 0), Velocity(1, 1)) should equal (false)
    }

    it("should not want entity without Position") {
      ps systemWantsEntity Set(Velocity(1, 1), TeamA(), Acceleration(1, 0)) should equal (false)
      ps systemWantsEntity Set(Velocity(1, 1), TeamA()) should equal (false)
      psNonProcessable systemWantsEntity Set(Velocity(1, 1), TeamA(), Acceleration(1, 0)) should equal (false)
      psNonProcessable systemWantsEntity Set(Velocity(1, 1), TeamA()) should equal (false)
    }

    it("should not want entity without Velocity") {
      ps systemWantsEntity Set(Position(0, 0), TeamA(), Acceleration(1, 0)) should equal (false)
      ps systemWantsEntity Set(Position(0, 0), TeamA()) should equal (false)
      psNonProcessable systemWantsEntity Set(Position(0, 0), TeamA(), Acceleration(1, 0)) should equal (false)
      psNonProcessable systemWantsEntity Set(Position(0, 0), TeamA()) should equal (false)
    }

    it("should not want entity with TeamB") {
      ps systemWantsEntity Set(Position(0, 0), TeamB(), Acceleration(1, 0)) should equal (false)
      ps systemWantsEntity Set(Position(0, 0), TeamB()) should equal (false)
      psNonProcessable systemWantsEntity Set(Position(0, 0), TeamB(), Acceleration(1, 0)) should equal (false)
      psNonProcessable systemWantsEntity Set(Position(0, 0), TeamB()) should equal (false)
    }

    it("should be able to remove entities") {
      val tempPlayer = world.createEntity(Position(0, 0), Velocity(1, 1), TeamA())
      ps systemHasEntity tempPlayer should equal (true)
      psNonProcessable systemHasEntity tempPlayer should equal (true)
      ps removeEntityFromSystem tempPlayer
      psNonProcessable removeEntityFromSystem tempPlayer
      ps systemHasEntity tempPlayer should equal (false)
      psNonProcessable systemHasEntity tempPlayer should equal (false)
    }

    it("should remove entity if TeamB component is added") {
      val tempPlayer = world.createEntity(Position(0, 0), Velocity(1, 1), TeamA())
      ps systemHasEntity tempPlayer should equal (true)
      psNonProcessable systemHasEntity tempPlayer should equal (true)
      world.addComponents(tempPlayer, TeamB())
      world.addComponents(tempPlayer, TeamB())
      ps systemHasEntity tempPlayer should equal (false)
      psNonProcessable systemHasEntity tempPlayer should equal (false)
    }
  }

  describe("WorldUserSpec") {
    it("should error when extracting world if it was not specified") {
      val thrownAsset = intercept[IllegalStateException] {
        dummyWorldUser.world
      }
      assert(thrownAsset.getMessage === "You haven't set world.")
    }
  }

  describe("WorldSpec") {
    it("should check whether AssetManager is set") {
      val dummyWorld = new World
      val thrownAsset = intercept[IllegalStateException] {
        dummyWorld.doDispose()
      }
      assert(thrownAsset.getMessage === "You haven't set assetManager.")
      dummyWorld.assetManager = dummyAssetManager
    }

    it("should error when adding component to entity which was not added to world") {
      val dummyWorld = new World
      val thrownAsset = intercept[IllegalArgumentException] {
        dummyWorld.addComponents(Entity(42), TeamA())
      }
      assert(thrownAsset.getMessage === "Entity Entity(42) doesn't exist.")
    }

    it("should error when removing entity when it was not added to world") {
      val dummyWorld = new World
      val thrownAsset = intercept[IllegalArgumentException] {
        dummyWorld.removeEntity(Entity(42))
      }
      assert(thrownAsset.getMessage === "Entity Entity(42) doesn't exist.")
    }

    it("should error when filtering components from entity when it was not added to world") {
      val dummyWorld = new World
      val thrownAsset = intercept[IllegalArgumentException] {
        dummyWorld.filterComponents(Entity(42), ((cs: Component) => false))
      }
      assert(thrownAsset.getMessage === "Entity Entity(42) doesn't exist.")
    }

    it("should add dummy system to world, which implements Renderer, Receiver and Responder interfaces") {
      val dummyWorld = new World
      val dummySystem = new DummySystem
      dummyWorld.stateManager = dummyStateManager
      dummyWorld.configManager = dummyConfigManager
      dummyWorld.assetManager = dummyAssetManager
      dummyWorld.tweenManager = dummyTweenManager
      dummyWorld.addSystem(dummySystem)
      dummyWorld.createEntity(Position(0, 0), Velocity(1, 1), TeamA())
      dummyWorld.render(0.1f)
      dummyWorld.resize(4, 2)
      dummyWorld.resizeFrozen(4, 2)
      dummyWorld.send(dummyMessage)
      dummyWorld.query(dummyMessage) should equal (Stream.Empty)
    }

    it("should add input receiver") {
      val dummyWorld = new World
      class DummyInputReceiverSystem extends DummySystem with InputReceiver
      val dummySystem = new DummyInputReceiverSystem
      dummyWorld.stateManager = dummyStateManager
      dummyWorld.configManager = dummyConfigManager
      dummyWorld.assetManager = dummyAssetManager
      dummyWorld.tweenManager = dummyTweenManager
      dummyWorld.addSystem(dummySystem)
      dummyWorld.addReceivers(dummyInputManager)
      ()
    }

    it("should add input proxy") {
      val dummyWorld = new World
      class DummyInputReceiverSystem extends DummySystem with InputProxy {
        def addReceivers(manager: AbstractInputManager): Unit = {}
      }
      val dummySystem = new DummyInputReceiverSystem
      dummyWorld.stateManager = dummyStateManager
      dummyWorld.configManager = dummyConfigManager
      dummyWorld.assetManager = dummyAssetManager
      dummyWorld.tweenManager = dummyTweenManager
      dummyWorld.addSystem(dummySystem)
      dummyWorld.addReceivers(dummyInputManager)
      ()
    }

    it("should query and return no response") {
      val dummyWorld = new World
      val future = dummyWorld.queryFirstAsync(dummyMessage)
      val waiter = new Waiter
      future onComplete {
        case Failure(f) => waiter {
          throw(f)
        }; waiter.dismiss()
        case Success(_) => waiter.dismiss()
      }
      intercept[NoReplyAvailable] {
        waiter.await()
      }
      dummyWorld.queryFirst(dummyMessage) should equal (None)
      dummyWorld.query(dummyMessage) should equal (Stream.Empty)
      dummyWorld.queryAsync(dummyMessage) should equal (Stream.Empty)
    }

    it("should query and return response") {
      val dummyWorld = new World
      dummyWorld.stateManager = dummyStateManager
      dummyWorld.configManager = dummyConfigManager
      dummyWorld.assetManager = dummyAssetManager
      dummyWorld.tweenManager = dummyTweenManager
      val dummyReply = new DummyReply
      class DummyRespondSystem extends DummySystem with Responder {
        override def respond(message: Message): Option[Reply] = Some(dummyReply)
      }
      dummyWorld.addSystem(new DummyRespondSystem)
      dummyWorld.queryFirst(dummyMessage).get should equal (dummyReply)
      dummyWorld.query(dummyMessage).head should equal (dummyReply)
      val future = dummyWorld.queryAsync(dummyMessage).head
      val waiter = new Waiter
      future onComplete {
        case Failure(_) => waiter.dismiss()
        case Success(v) => waiter {
          v should equal (dummyReply)
        }; waiter.dismiss()
      }
      waiter.await()

      val future2 = dummyWorld.queryFirstAsync(dummyMessage)
      val waiter2 = new Waiter
      future2 onComplete {
        case Failure(_) => waiter2.dismiss()
        case Success(v) => waiter2 {
          v should equal (dummyReply)
        }; waiter2.dismiss()
      }
      waiter2.await()
    }
  }
}
