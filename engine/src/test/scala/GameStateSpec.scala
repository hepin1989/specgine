package com.specdevs.specgine.test

import com.specdevs.specgine.states._
import com.specdevs.specgine.input.AbstractInputManager

import org.scalatest.{FunSpec,Matchers}

import scala.concurrent.Future
import scala.collection.mutable.HashMap

class GameStateSpec extends FunSpec with Matchers {
  val dummySingleEntitySystem = new PositionNonProcessableSystem

  class DummyGameState extends GameState {
    var removedEntity = false
    var sent = false
    var queried = false
    var queriedAsync = false
    var queriedFirst = false
    var queriedFirstAsync = false
    var filteredComponents = false
    var addedComponents = false
    var entityCreated = false
    var initialized = false
    var created = false
    var inside = false
    var disposed = false
    var processed = false
    var rendered = false
    var resizedFrozen = false
    var resized = false
    var addedReceivers = false

    def initialize(): Unit = {
      initialized = true
      addSystem(dummySingleEntitySystem)
    }

    override def create(): Unit = {
      super.create()
      created = true
    }

    override def enter(): Unit = {
      super.enter()
      inside = true
    }

    override def leave(): Unit = {
      super.leave()
      inside = false
    }

    override def dispose(): Unit = {
      super.dispose()
      disposed = true
    }

    override def process(dt: Float): Unit = {
      super.process(dt)
      processed = true
    }

    override def render(alpha: Float): Unit = {
      super.render(alpha)
      rendered = true
    }

    override def resize(x: Int, y: Int): Unit = {
      super.resize(x, y)
      resized = true
    }

    override def resizeFrozen(x: Int, y: Int): Unit = {
      super.resizeFrozen(x, y)
      resizedFrozen = true
    }

    override def addReceivers(manager: AbstractInputManager): Unit = {
      super.addReceivers(manager)
      addedReceivers = true
    }

    override def createEntity(components: Component*): Entity = {
      entityCreated = true
      super.createEntity(components: _*)
    }

    override def addComponents(e: Entity, components: Component*): Unit = {
      addedComponents = true
      super.addComponents(e, components: _*)
    }

    override def filterComponents(e: Entity, rule: Component => Boolean): Unit = {
      filteredComponents = true
      super.filterComponents(e, rule)
    }

    override def removeEntity(e: Entity): Unit = {
      removedEntity = true
      super.removeEntity(e)
    }

    override def send(message: Message): Unit = {
      sent = true
      super.send(message)
    }

    override def query(message: Message): Stream[Reply] = {
      queried = true
      super.query(message)
    }

    override def queryAsync(message: Message): Stream[Future[Reply]] = {
      queriedAsync = true
      super.queryAsync(message)
    }

    override def queryFirst(message: Message): Option[Reply] = {
      queriedFirst = true
      super.queryFirst(message)
    }

    override def queryFirstAsync(message: Message): Future[Reply] = {
      queriedFirstAsync = true
      super.queryFirstAsync(message)
    }
  }

  class DummySpecOne extends ComponentsSpec with ComponentsManager {
    val team1 = new HashMap[Entity,TeamA]
    var removed = false
    def isTeamA(c: Component): Boolean = c match {
      case (_: TeamA) => true
      case _          => false
    }
    def wants(cs: Set[Component]): Boolean = cs.exists(isTeamA)
    def addComponent(e: Entity, c: Component): Unit = c match {
      case (t @ (_: TeamA)) => team1.update(e, t)
      case _                => ()
    }

    def add(e: Entity, cs: Set[Component]): Unit = {
      cs.foreach(c => addComponent(e,c))
    }
    def has(e: Entity): Boolean = team1.isDefinedAt(e)
    def remove(e: Entity): Unit = {
      team1.remove(e)
      removed = true
    }
    override def group(e: Entity): Option[Int] = {
      if (e.id > 42) Some(1) else None
    }
  }

  class DummySpecTwo extends ComponentsSpec with ComponentsManager {
    val team2 = new HashMap[Entity,TeamB]
    var removed = false
    def isTeamB(c: Component): Boolean = c match {
      case (_: TeamB) => true
      case _          => false
    }
    def wants(cs: Set[Component]): Boolean = cs.exists(isTeamB)
    def addComponent(e: Entity, c: Component): Unit = c match {
      case (t @ (_: TeamB)) => team2.update(e, t)
      case _                => ()
    }

    def add(e: Entity, cs: Set[Component]): Unit = {
      cs.foreach(c => addComponent(e,c))
    }
    def has(e: Entity): Boolean = team2.isDefinedAt(e)
    def remove(e: Entity): Unit = {
      team2.remove(e)
      removed = true
    }
    override def group(e: Entity): Option[Int] = {
      if (e.id < 42) Some(1) else None
    }
  }

  class DummyPairEntitySystem extends
    PairEntitySystem
      with DummyCallbacksImplementation
      with Processor
      with Renderer {
    val components1 = new DummySpecOne
    val components2 = new DummySpecTwo

    override def process(dt: Float, e1: Entity, e2: Entity): Unit = {}
    override def render(alpha: Float, e1: Entity, e2: Entity): Unit = {}
  }

  val dummyGameState = new DummyGameState
  dummyGameState.assetManager = new DummyAssetManager
  dummyGameState.stateManager = new DummyStateManager
  dummyGameState.configManager = new DummyConfigManager
  dummyGameState.tweenManager =  new DummyTweenManager
  val dummyInputManager = new DummyInputManager
  val dummyPairEntitySystem = new DummyPairEntitySystem
  dummyGameState.world.addSystem(dummyPairEntitySystem)

  describe("GameState") {
    it("should initialize"){
      dummyGameState.initialized should equal (false)
      dummyGameState.doInitialize()
      dummyGameState.initialized should equal (true)
    }

    it("should create") {
      dummyGameState.created should equal (false)
      dummyGameState.create()
      dummyGameState.created should equal (true)
    }

    it("should enter and leave") {
      dummyGameState.inside should equal (false)
      dummyGameState.enter()
      dummyGameState.inside should equal (true)
      dummyGameState.leave()
      dummyGameState.inside should equal (false)
    }

    it("should dispose") {
      dummyGameState.disposed should equal (false)
      dummyGameState.dispose()
      dummyGameState.disposed should equal (true)
    }

    it("should process") {
      dummyGameState.processed should equal (false)
      dummyGameState.process(0.1f)
      dummyGameState.processed should equal (true)
    }

    it("should render") {
      dummyGameState.rendered should equal (false)
      dummyGameState.render(0.1f)
      dummyGameState.rendered should equal (true)
    }

    it("should resize") {
      dummyGameState.resized should equal (false)
      dummyGameState.resize(4, 2)
      dummyGameState.resized should equal (true)
    }

    it("should resizeFrozen") {
      dummyGameState.resizedFrozen should equal (false)
      dummyGameState.resizeFrozen(4, 2)
      dummyGameState.resizedFrozen should equal (true)
    }

    it("should addReceivers") {
      dummyGameState.addedReceivers should equal (false)
      dummyGameState.addReceivers(dummyInputManager)
      dummyGameState.addedReceivers should equal (true)
    }

    it("should create entity with TeamA and have it in system") {
      dummyPairEntitySystem.systemWantsEntity(Set(TeamA())) should equal (true)
      val entity = Entity(42)
      val entity2 = Entity(43)
      dummyPairEntitySystem.addEntityToSystem(entity,Set(TeamA()))
      dummyPairEntitySystem.addEntityToSystem(entity2,Set(TeamA()))
      dummyPairEntitySystem.systemHasEntity(entity) should equal (true)
      dummyPairEntitySystem.systemHasEntity(entity2) should equal (true)
      dummyPairEntitySystem.removeEntityFromSystem(entity)
      dummyPairEntitySystem.removeEntityFromSystem(entity2)
      dummyPairEntitySystem.components1.removed should equal (true)
    }

    it("should create entity with TeamB and have it in system") {
      val tempSystem = new DummyPairEntitySystem
      tempSystem.systemWantsEntity(Set(TeamB())) should equal (true)
      val entity = Entity(42)
      val entity2 = Entity(41)
      dummyPairEntitySystem.addEntityToSystem(entity,Set(TeamB()))
      dummyPairEntitySystem.addEntityToSystem(entity2,Set(TeamB()))
      dummyPairEntitySystem.systemHasEntity(entity) should equal (true)
      dummyPairEntitySystem.systemHasEntity(entity2) should equal (true)
      dummyPairEntitySystem.removeEntityFromSystem(entity)
      dummyPairEntitySystem.removeEntityFromSystem(entity2)
      dummyPairEntitySystem.components1.removed should equal (true)
    }

    it("should process and render") {
      val tempSystem = new DummyPairEntitySystem
      dummyGameState.world.addSystem(tempSystem)
      tempSystem.addEntityToSystem(Entity(41),Set(TeamB()))
      tempSystem.addEntityToSystem(Entity(43),Set(TeamB()))
      tempSystem.addEntityToSystem(Entity(44),Set(TeamA()))
      tempSystem.addEntityToSystem(Entity(40),Set(TeamA()))
      dummyGameState.world.process(0.1f)
      dummyGameState.world.render(0.1f)
    }

    it("should not remove non existing entity") {
      val tempSystem = new DummyPairEntitySystem
      dummyGameState.world.addSystem(tempSystem)
      val tempEntity = dummyGameState.world.createEntity(TeamC())
      tempSystem.removeEntityFromSystem(tempEntity)
      tempSystem.components1.removed should equal (false)
      tempSystem.components2.removed should equal (false)
    }

    it("should create entity and remove it") {
      val entity = dummyGameState.createEntity(TeamA())
      dummyGameState.entityCreated should equal (true)
      dummyGameState.removeEntity(entity)
      dummyGameState.removedEntity should equal (true)
    }

    it("should create entity and add components to it") {
      val entity = dummyGameState.createEntity(TeamA())
      dummyGameState.entityCreated should equal (true)
      dummyGameState.addComponents(entity, TeamB())
      dummyGameState.addedComponents should equal (true)
    }

    it("should create entity and filter components to it") {
      val entity = dummyGameState.createEntity(TeamA())
      dummyGameState.entityCreated should equal (true)
      dummyGameState.filterComponents(entity, (cs: Component) => false)
      dummyGameState.filteredComponents should equal (true)
    }

    it("should send message") {
      dummyGameState.send(new DummyMessage)
      dummyGameState.sent should equal (true)
    }

    it("should query message") {
      dummyGameState.query(new DummyMessage)
      dummyGameState.queried should equal (true)
    }

    it("should queryAsync message") {
      dummyGameState.queryAsync(new DummyMessage)
      dummyGameState.queriedAsync should equal (true)
    }

    it("should queryFirst message") {
      dummyGameState.queryFirst(new DummyMessage)
      dummyGameState.queriedFirst should equal (true)
    }

    it("should queryFirstAsync message") {
      dummyGameState.queryFirstAsync(new DummyMessage)
      dummyGameState.queriedFirstAsync should equal (true)
    }
  }
}
