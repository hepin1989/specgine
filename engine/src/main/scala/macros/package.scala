package com.specdevs.specgine

/** Package providing macros simplifying usage of SpecGine. */
package object macros
