package com.specdevs.specgine.macros.states

import com.specdevs.specgine.states.{Component,Entity}

import scala.collection.mutable.LongMap

/** Root of all possible field descriptors used by `withManager` macro. */
sealed trait FieldDescription

private[specgine] trait EntityMap[A] {
  private val implementation = new LongMap[A]

  /** Method to remove [[com.specdevs.specgine.states.Entity `Entity`]] from map.
   *
   * Should not be used directly, use `removeEntity` method from
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @param e entity to remove from map.
   * @return `Some` data removed from map, if found.
   */
  def remove(e: Entity): Option[A] = implementation.remove(e.id)

  /** Method to update [[com.specdevs.specgine.states.Entity `Entity`]] in map.
   *
   * Should not be used directly, use `addComponents` and `filterComponents` methods
   * from [[com.specdevs.specgine.states.World `World`]].
   *
   * @param e entity to look for in map.
   */
  def update(e: Entity, value: A): Unit = implementation.update(e.id, value)

  /** Method to access data related to given [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * Can raise exception if entity is not defined.
   *
   * @param e entity to look for in map.
   * @return data stored in map.
   */
  def apply(e: Entity): A = implementation.apply(e.id)

  /** Method checking if given [[com.specdevs.specgine.states.Entity `Entity`]] is stored in map.
   *
   * @param e entity to look for in map.
   * @return `true` if stored, `false` otherwise.
   */
  def isDefinedAt(e: Entity): Boolean = implementation.isDefinedAt(e.id)

  /** Method to securely access data related to given [[com.specdevs.specgine.states.Entity `Entity`]].
   *
   * @param e entity to look for in map.
   * @return `Some` data stored in map, `None` if entity `e` is not stored in map.
   */
  def get(e: Entity): Option[A] = implementation.get(e.id)

  /** Iterate over [[com.specdevs.specgine.states.Entity `Entity`]] map.
   *
   * @param f Some function to call for every entity.
   */
  def foreach(f: (Entity, A) => Unit): Unit = implementation.foreach { arg => f(Entity(arg._1), arg._2) }
}

/** Specifies that this type of [[com.specdevs.specgine.states.Component `Component`]] is needed.
 *
 * All instances of [[com.specdevs.specgine.states.Entity `Entity`]] must have this
 * [[com.specdevs.specgine.states.Component `Component`]] associated with
 * them, to be accepted by the generated
 * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]] and
 * thus also by [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it. Value of component can be accessed trough this field.
 *
 * Because all entities have this component, `Needed` maps
 * [[com.specdevs.specgine.states.Entity `Entity`]] directly to
 * [[com.specdevs.specgine.states.Component `Component`]].
 *
 * @constructor Creates a map from [[com.specdevs.specgine.states.Entity `Entity`]]
 *              to [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class Needed[C <: Component] extends EntityMap[C] with FieldDescription

/** Specifies that this type of [[com.specdevs.specgine.states.Component `Component`]] is optional.
 *
 * Generated [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * and thus also [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * using it will accept [[com.specdevs.specgine.states.Entity `Entity`]] even
 * if is not associated with given [[com.specdevs.specgine.states.Component `Component`]],
 * but it will be accessible trough this field.
 *
 * Because entities may not have this component, `Optional` maps
 * [[com.specdevs.specgine.states.Entity `Entity`]] to `Option` of
 * [[com.specdevs.specgine.states.Component `Component`]], which is set to
 * `None` if entity does not have it, and to some component if it does.
 *
 * @constructor Creates a map from [[com.specdevs.specgine.states.Entity `Entity`]]
 *              to `Option` of [[com.specdevs.specgine.states.Component `Component`]].
 * @tparam C Subtype of [[com.specdevs.specgine.states.Component `Component`]].
 */
class Optional[C <: Component] extends EntityMap[Option[C]] with FieldDescription

/** Specifies that [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
 * should be able to access all [[com.specdevs.specgine.states.Component `Component`]]s.
 *
 * This field does not influence in any way if
 * [[com.specdevs.specgine.states.Entity `Entity`]] will be accepted by generated
 * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]] and
 * thus also by [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]],
 * but if it will be accepted due to other fields or rules, all its
 * [[com.specdevs.specgine.states.Component `Component`]]s will be stored in
 * it.
 *
 * Because entities can have any number of different components, `All` maps
 * [[com.specdevs.specgine.states.Entity `Entity`]] to `Set` of
 * [[com.specdevs.specgine.states.Component `Component`]]s.
 * @constructor Creates a map from [[com.specdevs.specgine.states.Entity `Entity`]]
 *              to `Set` of [[com.specdevs.specgine.states.Component `Component`]]s.
 */
class All extends EntityMap[Set[Component]] with FieldDescription

