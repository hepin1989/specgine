package com.specdevs.specgine.input

/** Package implementing input classes using LibGDX framework. */
package object gdx
