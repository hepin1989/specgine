package com.specdevs.specgine.input.gdx

import com.specdevs.specgine.input._

import com.badlogic.gdx.input.{GestureDetector => GdxGestureDetector}
import com.badlogic.gdx.Input.{Keys => GdxKeys}
import com.badlogic.gdx.input.GestureDetector.{GestureListener => GdxGestureListener}
import com.badlogic.gdx.{InputProcessor => GdxInputProcessor}
import com.badlogic.gdx.{InputMultiplexer => GdxInputMultiplexer}
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.{Vector2 => GdxVector2}

/** Input manager class based on LibGDX.
 *
 * Currently there is no way to configure gesture options, and default LibGDX
 * `GestureDetector` options are used:
 *   - Size of tap area is square 40 pixels wide.
 *   - Time between taps needs to be 0.4 second to count as separate taps.
 *   - Time to hold finger before it is qualified as long-press is 1.1 second.
 *   - Time to fire fling event cannot be higher than 0.15 second.
 *
 * @constructor Creates instance of
 *              [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]]
 *              thas also implements LibGDX `InputProcessor` and `GestureListener`.
 */
class InputManager extends AbstractInputManager with GdxInputProcessor with GdxGestureListener {
  private val multiplexer = new GdxInputMultiplexer

  private val keys = new InputReceiverQueue[KeyReceiver]

  private val backs = new InputReceiverQueue[BackReceiver]

  private val pointers = new InputReceiverQueue[PointerReceiver]

  private val gestures = new InputReceiverQueue[GestureReceiver]

  multiplexer.addProcessor(this)

  multiplexer.addProcessor(new GdxGestureDetector(this))

  Gdx.input.setInputProcessor(multiplexer)

  def addReceiver(what: InputReceiver, priority: Int = 0): Unit = {
    what match {
      case x: KeyReceiver => keys.enqueue(x, priority)
      case _ => ()
    }
    what match {
      case x: BackReceiver => {
        backs.enqueue(x, priority)
        Gdx.input.setCatchBackKey(true)
      }
      case _ => ()
    }
    what match {
      case x: PointerReceiver => pointers.enqueue(x, priority)
      case _ => ()
    }
    what match {
      case x: GestureReceiver => gestures.enqueue(x, priority)
      case _ => ()
    }
  }

  def reset(): Unit = {
    keys.clear()
    backs.clear()
    pointers.clear()
    gestures.clear()
    Gdx.input.setCatchBackKey(false)
  }

  /** Implementation of LibGDX back button and key press callback.
   *
   * @param keycode Key-code of button.
   * @return `true` if event is consumed.
   */
  def keyDown(keycode: Int): Boolean = {
    val back = (keycode == GdxKeys.BACK) && (backs exists (_.backDown()))
    back || (keys exists (_.keyDown(keycode)))
  }

  /** Implementation of LibGDX back button and key release callback.
   *
   * @param keycode Key-code of button.
   * @return `true` if event is consumed.
   */
  def keyUp(keycode: Int): Boolean = {
    val back = (keycode == GdxKeys.BACK) && (backs exists (_.backUp()))
    back || (keys exists (_.keyUp(keycode)))
  }

  /** Implementation of LibGDX key typed callback.
   *
   * @param char Typed character.
   * @return `true` if event is consumed.
   */
  def keyTyped(char: Char): Boolean = {
    keys exists (_.keyTyped(char))
  }

  /** Implementation of LibGDX pointer press callback.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
    pointers exists (_.touchDown(x, y, pointer, button))
  }

  /** Implementation of LibGDX pointer release callback.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def touchUp(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
    pointers exists (_.touchUp(x, y, pointer, button))
  }

  /** Implementation of LibGDX pointer drag callback.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @return `true` if event is consumed.
   */
  def touchDragged(x: Int, y: Int, pointer: Int): Boolean = {
    pointers exists (_.touchDragged(x, y, pointer))
  }

  /** Implementation of LibGDX pointer move callback.
   *
   * Never called for touch-screen.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @return `true` if event is consumed.
   */
  def mouseMoved(x: Int, y: Int): Boolean = {
    pointers exists (_.mouseMoved(x, y))
  }

  /** Implementation of LibGDX mouse scroll callback.
   *
   * Never called for touch-screen.
   *
   * @param amount Amount of scroll.
   * @return `true` if event is consumed.
   */
  def scrolled(amount: Int): Boolean = {
    pointers exists (_.scrolled(amount))
  }

  /** Implementation of LibGDX fling callback.
   *
   * Fling is sometimes called "swipe". It is quick gesture used to turn pages
   * or move content quickly.
   *
   * @param velX X velocity in pixels per second of finger.
   * @param velY Y velocity in pixels per second of finger.
   * @param button Button used for fling, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def fling(velX: Float, velY: Float, button: Int): Boolean = {
    gestures exists (_.fling(velX, velY, button))
  }

  /** Implementation of LibGDX pointer press callback.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean = {
    gestures exists (_.touchDown(x, y, pointer, button))
  }

  /** Implementation of LibGDX pointer long-press callback.
   *
   * Exact time depends on implementation of
   * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @return `true` if event is consumed.
   */
  def longPress(x: Float, y: Float): Boolean = {
    gestures exists (_.longPress(x, y))
  }

  /** Implementation of LibGDX pointer tap callback.
   *
   * It can be also used to register double-clicks or double-taps.
   *
   * Exact time and area depends on implementation of
   * [[com.specdevs.specgine.input.AbstractInputManager `AbstractInputManager`]].
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param count Count of taps.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def tap(x: Float, y: Float, count: Int, button: Int): Boolean = {
    gestures exists (_.tap(x, y, count, button))
  }

  /** Implementation of LibGDX pointer pan callback.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param dx Amount of pan in x direction.
   * @param dy Amount of pan in y direction.
   * @return `true` if event is consumed.
   */
  def pan(x: Float, y: Float, dx: Float, dy: Float): Boolean = {
    gestures exists (_.pan(x, y, dx, dy))
  }

  /** Implementation of LibGDX pan-stop callback.
   *
   * @param x X coordinate of event.
   * @param y Y coordinate of event.
   * @param pointer Number of finger, on mouse always 1.
   * @param button Number of button, on touchscreen always 0.
   * @return `true` if event is consumed.
   */
  def panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean = {
    gestures exists (_.panStop(x, y, pointer, button))
  }

  /** Implementation of LibGDX zoom callback.
   *
   * @param initialDistance Distance between fingers when second finger
   *        touched the screen.
   * @param distance Current distance between fingers.
   * @return `true` if event is consumed.
   */
  def zoom(initialDistance: Float, distance: Float): Boolean = {
    gestures exists (_.zoom(initialDistance, distance))
  }

  /** Implementation of LibGDX pinch callback.
   *
   * It is universal two-finger gesture, that can be used to implement
   * rotations or mixed zoom/rotate/pan events.
   *
   * @param initialP1 Coordinates of first finger when it was placed.
   * @param initialP2 Coordinates of second finger when it was placed.
   * @param currentP1 Current coordinates of first finger.
   * @param currentP2 Current coordinates of second finger.
   * @return `true` if event is consumed.
   */
  def pinch(initialP1: GdxVector2, initialP2: GdxVector2, currentP1: GdxVector2, currentP2: GdxVector2): Boolean = {
    gestures exists (_.pinch(
      (initialP1.x, initialP1.y),
      (initialP2.x, initialP2.y),
      (currentP1.x, currentP1.y),
      (currentP2.x, currentP2.y)
    ))
  }
}
