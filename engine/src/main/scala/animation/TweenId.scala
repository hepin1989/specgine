package com.specdevs.specgine.animation

/** Class for tweens ids in game.
 *
 * `TweenId` is represented by its `id`. It can be seen as label.
 *
 * @constructor Creates new `TweenId` object with provided id.
 * @param id Id of `TweenId`.
 */
case class TweenId(val id: Int) extends AnyVal {
  /** Utility method used to create `TweenId` successor.
   *
   * When method is used on `TweenId` that was lastly created, it
   * guarantees that the id will be uniqe, unless `Int` overflows
   * and comes back to `1`.
   *
   * @return New `TweenId` instance.
   */
  def next: TweenId = new TweenId(id+1)
}
