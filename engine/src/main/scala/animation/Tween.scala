package com.specdevs.specgine.animation

import com.specdevs.specgine.core.Accessor

import scala.math.{min,max}

/** Class for interpolation between parameter's current value and desired target value.
 *
 * @constructor Creates new [[com.specdevs.specgine.animation.Tween `Tween`]].
 * @param acc Accessor for chosen parameter.
 * @param target Desired target.
 * @param duration Amount of time the parameter is changing to a target value.
 * @param easingFunction A way of changing, described by a continuous function whose domain is [0, 1].
 *   For T=Double and T=Float its value at 0 is supposed to be equal to 0 and
 *   its value at 1 is supposed to be equal to 1.
 * @param stopAfter Number of repetitions after which the animation is supposed to stop. Loop infinitely if None.
 * @param yoyo Whether, after reaching the target,
 *   animation should go back in the opposite direction or start again from the beginning.
 * @note Default value for easingFunction is defined for T=Float.
 */
class Tween[T] (
  val acc: Accessor[T],
  val target: T,
  val duration: Float,
  val easingFunction: Interpolator[T] = interpolators.Linear,
  val stopAfter: Option[Int] = Some(1),
  val yoyo: Boolean = false)(implicit num: Numeric[T]) extends TweenInterface {

  private var initialValue: Option[T] = None

  private var diff: Option[T] = None

  private var currentIter = stopAfter getOrElse -1

  private var direction = 1f

  private var t = 0f

  def update(dt: Float): Boolean = {

    if (initialValue.isEmpty) {
      initialValue = Some(acc.value)
      diff = Some(num.minus(target, initialValue.get))
    }

    t += dt*direction

    if (stopAfter.isEmpty || currentIter>0) {
      if (t < 0 || duration < t) {
        currentIter -= 1
        if (yoyo) direction = -direction else t = 0
      }
      t = max(min(t, duration), 0)
      acc.value = num.plus(num.times(diff.get, easingFunction(t/duration)), initialValue.get)
      false
    } else {
      true
    }
  }
}
