package com.specdevs.specgine.core

/** Trait mixed-in into classes that use
 * [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
 */
trait ConfigManagerUser {
  private var theConfigManager: Option[AbstractConfigManager] = None

  /** Getter for instance of
   * [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
   *
   * @return Configuration manager instance.
   */
  implicit def configManager: AbstractConfigManager = {
    if (theConfigManager.isEmpty) {
      throw new IllegalStateException("You haven't set configManager.")
    } else {
      theConfigManager.get
    }
  }

  /** Setter for instance of
   * [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
   *
   * @param manager Configuration manager instance.
   */
  def configManager_=(manager: AbstractConfigManager): Unit = {
    theConfigManager = Some(manager)
  }

  /** Method used to get configuration from manager.
   *
   * @param key Key poinint to value.
   * @return Value associated with key, or `None` if not found.
   */
  def getConfig(key: String): Option[Config] = {
    configManager.get(key)
  }

  /** Method used to set configuration in manager.
   *
   * @param key Key poinint to value.
   * @param value Value to store.
   */
  def setConfig(key: String, value: Config): Unit = {
    configManager.set(key, value)
  }

  /** Method to ensure configuration persistence. */
  def storeConfig(): Unit = {
    configManager.store()
  }
}
