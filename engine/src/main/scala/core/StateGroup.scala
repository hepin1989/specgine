package com.specdevs.specgine.core

import com.specdevs.specgine.assets.AssetManagerUser

import scala.collection.mutable.HashMap

/** Trait that is parent of all groups in SpecGine.
 *
 * User of SpecGine should implement abstract methods:
 *   - `initialize`,
 *   - `create`,
 *   - `dispose`.
 *
 * Groups are very important for resource management, because when something
 * is initialized or created, we already have guarantee that all groups which
 * are direct or indirect parents of state or group are already initialized or
 * created before. Disposing groups or states works analogusly.
 *
 * They are perfect place to declare shared resources or configuration, or to
 * exchange information between states.
 */
trait StateGroup extends AssetManagerUser with ConfigManagerUser with GetSetUser {
  private val localInfo = new HashMap[String, StateInfo]

  /** Method called, when group is initialized for the first time.
   *
   * This is perfect place to load your assets or set some configuration.
   */
  def initialize(): Unit

  /** Helper method correctly calling `initialize`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doInitialize(): Unit = {
    initialize()
  }

  /** Method called, when group is loaded.
   *
   * This is perfect place store loaded assets in local state or allocate
   * local data structures.
   */
  def create(): Unit

  /** Helper method correctly calling `create`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doCreate(): Unit = {
    create()
  }

  /** Method called, when last state depending on group is permamently removed from stack.
   *
   * Remember to deallocate all resources you allocated yourself inside
   * `create` or `initialize`.
   */
  def dispose(): Unit

  /** Helper method correctly calling `dispose`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doDispose(): Unit = {
    dispose()
    localInfo.clear()
    disposeAssets()
  }

  /** Method used to get state info from group.
   *
   * @param key Key poinint to value.
   * @return Value associated with key, or `None` if not found.
   */
  def getLocalInfo(key: String): Option[StateInfo] = localInfo.get(key)

  /** Method used to set sate info in group.
   *
   * @param key Key poinint to value.
   * @param value Value to store.
   */
  def setLocalInfo(key: String, value: StateInfo): Unit = {
    localInfo += key -> value
    ()
  }
}
