package com.specdevs.specgine.core

private[core] sealed trait Node

private[core] class StateNode(val state: State, val parent: String) extends Node {
  private var onStack = false

  private var theAlpha = 0f

  def alpha: Float = theAlpha

  def alpha_=(a: Float): Unit = {
    theAlpha = a
  }

  def isOnStack: Boolean = onStack

  def initialize(): Unit = {
    state.doInitialize()
    onStack = true
  }

  def dispose(): Unit = {
    onStack = false
    state.doDispose()
  }
}

private[core] class GroupNode(val group: StateGroup, val level: Int, val parent: String) extends Node {
  private var initializeCount = 0

  private var notCreated = true

  def initialize(): Unit = {
    if (initializeCount==0) group.doInitialize()
    initializeCount += 1
  }

  def create(): Unit = {
    if (notCreated) {
      group.doCreate()
      notCreated = false
    }
  }

  def dispose(): Unit = {
    initializeCount -= 1
    if (initializeCount==0) {
      group.doDispose()
      notCreated = true
    }
  }
}
