package com.specdevs.specgine.core

/** Trait representing all implementations of main game classes.
 *
 * This trait represents framework-independent trait for main game classes.
 * Each class implementing it, is responsible for following tasks:
 *   - Creating instances of framework-specific managers.
 *   - Creating [[com.specdevs.specgine.core.StateManager `StateManager`]] and
 *     setting in it just created managers.
 *   - Call `initialize` for [[com.specdevs.specgine.core.StateManager `StateManager`]].
 *   - Set `defaultLoader` in [[com.specdevs.specgine.core.StateManager `StateManager`]].
 *   - Call `initialize` on `this`.
 *   - Proxy methods `addState` and `addGroup` to
 *     [[com.specdevs.specgine.core.StateManager `StateManager`]], remember
 *     value of eventual call to `setLoadingScreen`.
 *   - Set `userLoader` in [[com.specdevs.specgine.core.StateManager `StateManager`]]
 *     using stored value of `setLoadingScreen` (if set).
 *   - Call `create` on [[com.specdevs.specgine.core.StateManager `StateManager`]].
 *   - Issue `process`, `render` and `resize` methods on
 *     [[com.specdevs.specgine.core.StateManager `StateManager`]] when needed.
 *   - Close game, if `shouldQuit` in
 *     [[com.specdevs.specgine.core.StateManager `StateManager`]] returns `true`.
 *   - Call `dispose` method on all managers as needed.
 *   - Call `getProfilingData` method when interested in profiling statistics.
 *
 * `process(dt: Float)` method of [[com.specdevs.specgine.core.StateManager `StateManager`]]
 * should be called with equal `dt` steps for deterministic physics
 * simulation. Fraction of `dt` step not fitting into time used to process and
 * render last step should be passed to `render(alpha: Float)`, so rendering
 * can interpolate between two simulation states.
 *
 * Example implementation based on LibGDX framework is present in
 * [[com.specdevs.specgine.core.gdx]] package.
 *
 * @see [[http://gafferongames.com/game-physics/fix-your-timestep/ ''gafferongames.com web page'']],
 *      for ideas behind separate `process` and `render`.
 */
trait AbstractGame {
  /** Register state with given name.
   *
   * @param state [[com.specdevs.specgine.core.State `State`]] to add.
   * @param name Name of [[com.specdevs.specgine.core.State `State`]].
   * @param parentName Name of [[com.specdevs.specgine.core.StateGroup `StateGroup`]].
   * @param default Set to `true` if you want to start game from this state.
   * @throws IllegalArgumentException If parent group is missing or name already used.
   */
  protected def addState(state: State, name: String, parentName: String = "", default: Boolean = false): Unit

  /** Register group with given name.
   *
   * @param group [[com.specdevs.specgine.core.StateGroup `StateGroup`]] to add.
   * @param name Name of [[com.specdevs.specgine.core.StateGroup `StateGroup`]].
   * @param parentName Name of [[com.specdevs.specgine.core.StateGroup `StateGroup`]].
   * @throws IllegalArgumentException If parent group is missing or name already used.
   */
  protected def addGroup(group: StateGroup, name: String, parentName: String = ""): Unit

  /** Set loading screen to use for states without specified loading screen.
   *
   * @param loader Implementation of [[com.specdevs.specgine.core.LoadingScreen `LoadingScreen`]].
   */
  protected def setLoadingScreen(loader: LoadingScreen): Unit

  /** Initialization code of game.
   *
   * Inside this method, user of SpecGine should create hierarchy of states
   * and groups.
   */
  def initialize(): Unit

  /** Returns profiling data.
   *
   * @return `SpecGine` `ProfilingData` or `None` if profiling is not enabled.
   */
  def getProfilingData(): Option[ProfilingData]
}
