package com.specdevs.specgine.core

import com.specdevs.specgine.assets.AssetManagerUser
import com.specdevs.specgine.animation.{TweenManager,TweenManagerUser}

/** Trait that is parent of all states in SpecGine.
 *
 * States are one of key concepts of SpecGine. They are managed by
 * [[com.specdevs.specgine.core.StateManager `StateManager`]] and its
 * internal stack. To create state, user should implement abstract methods:
 *   - `initialize` - Called when state enters stack, before loading of
 *     screen.
 *   - `create` - Called after loading of screen.
 *   - `enter` - Called when state becomes active (is created and on top of
 *     stack).
 *   - `leave` - Called when state becomes frozen (some element of stack is
 *     above it).
 *   - `dispose` - Called when state leaves stack.
 *
 * And:
 *   - `resize` - Called when screen size changes.
 *   - `process` - Called when state should process simulation.
 *   - `render` - Called when state should render itself.
 *
 * To add support frozen rendering, overload methods:
 *   - `resizeFrozen`,
 *   - `processFrozen`,
 *   - `renderFrozen`.
 *
 * To set custom loading screen, overload methods:
 *   - `resizeLoading`,
 *   - `processLoading`,
 *   - `renderLoading`.
 *
 * Those custom methods for loading and frozen processing work in same manner
 * as basic methods for states. It is important to note, that frozen
 * processing will occur only when requested by state directly above when
 * it is active state (not from other frozen state) and loading will be
 * displayed when during state transition new resources needs to be loaded.
 * It allows to for example render frozen game under pause menu (which will
 * be just new state on top of game state) or change loading screen to
 * mission-dependent one, by just overloading three methods in "mission state".
 *
 * `State` is equipped with tween mechanism so it enables certain parameters' animations.
 * See [[com.specdevs.specgine.animation `com.specdevs.specgine.animation`]] for details.
 *
 * It should be rare to need to create `State` directly, because
 * [[com.specdevs.specgine.states]] provides some universal states more suited
 * for further extension.
 */
trait State extends AssetManagerUser with ConfigManagerUser with GetSetUser with StateManagerUser
  with TweenManagerUser {

  /** Method called, when state is initialized for the first time.
   *
   * This is perfect place to load your assets or set some configuration.
   */
  def initialize(): Unit

  /** Helper method correctly calling `initialize`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doInitialize(): Unit = {
    tweenManager = new TweenManager
    initialize()
  }

  /** Method called, when state is loaded.
   *
   * This is perfect place to store loaded assets in local state or allocate
   * local data structures. You do not have guarantee, that `resize` was
   * called.
   */
  def create(): Unit

  /** Helper method correctly calling `create`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doCreate(): Unit = {
    create()
  }

  /** Method called, when state becomes active.
   *
   * This is last method called before `process` or `render`, you already can
   * be sure that `resize` was called. It is perfect place to prepare
   * rendering. This can be called multiple times if state becomes frozen and
   * then back active.
   */
  def enter(): Unit

  /** Helper method correctly calling `enter`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doEnter(): Unit = {
    enter()
  }

  /** Method called, when state becomes frozen.
   *
   * This is perfect place to deallocate all resources allocated inside
   * `enter` method. It will be called when state becomes frozen, but also
   * just before it is going to be disposed.
   */
  def leave(): Unit

  /** Helper method correctly calling `leave`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doLeave(): Unit = {
    leave()
  }

  /** Method called, when state is permamently removed from stack.
   *
   * Remember to deallocate all resources you allocated yourself inside
   * `create` or `initialize`.
   */
  def dispose(): Unit

  /** Helper method correctly calling `dispose`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doDispose(): Unit = {
    dispose()
    disposeAssets()
    disposeTweens()
  }

  /** Method called when size of screen changes.
   *
   * It is guaranteed to be called at least once, before first call to
   * `enter`.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit

  /** Method processing state.
   *
   * @param dt Time of simulation step.
   */
  def process(dt: Float): Unit

  /** Helper method correctly calling `process`, used by
   * [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doProcess(dt: Float): Unit = {
    process(dt)
    tweenManager.update(dt)
  }

  /** Method rendering state.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def render(alpha: Float): Unit

  /** Method called when size of frozen screen changes.
   *
   * It is guaranteed to be called at least once, before first call to
   * `processFrozen` or `renderFrozen`.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resizeFrozen(x: Int, y: Int): Unit = {}

  /** Method processing frozen state.
   *
   * @param dt Time of simulation step.
   */
  def processFrozen(dt: Float): Unit = {}

  /** Method rendering frozen state.
   *
   * @param alpha Render interpolation between two last frozen simulation steps (0..1).
   * @param simulationAlpha Last value of `alpha` called when state was active (0..1).
   */
  def renderFrozen(alpha: Float, simulationAlpha: Float): Unit = {}

  /** Method called when size of screen changes during or before loading.
   *
   * It is guaranteed to be called at least once, before first call to
   * `processLoading` or `renderLoading`.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resizeLoading(x: Int, y: Int): Unit = {
    stateManager.userLoader.resize(x, y)
  }

  /** Method processing loading animation.
   *
   * @param dt Time of simulation step.
   */
  def processLoading(dt: Float): Unit = {
    stateManager.userLoader.processLoading(dt)
  }

  /** Method rendering loading screen.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   * @param progress Percentage of loaded resources (0..1).
   */
  def renderLoading(alpha: Float, progress: Float): Unit = {
    stateManager.userLoader.renderLoading(alpha, progress)
  }
}
