package com.specdevs.specgine.core

/** Trait for implementations of [[com.specdevs.specgine.core.Config `Config`]] managers.
 *
 * Example implementation based on LibGDX is present in
 * [[com.specdevs.specgine.core.gdx]] package.
 */
trait AbstractConfigManager extends Getter[Config] with Setter[Config] {
  /** Method ensuring data persistence. */
  def store(): Unit
}
