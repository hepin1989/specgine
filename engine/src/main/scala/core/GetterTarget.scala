package com.specdevs.specgine.core

/** Trait used for accessing stored values in optional target.
 *
 * @tparam A Type of stored values.
 */
trait GetterTarget[A] {
  /** Method returning value.
   *
   * @param key Key pointing to value.
   * @param target Where to look for value, `None` for default target.
   * @return Value associated with `key`, `None` if not found.
   * @throws IllegalArgumentException If target is wrong.
   */
  def getFrom(key: String, target: Option[String] = None): Option[A]
}
