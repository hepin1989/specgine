package com.specdevs.specgine.core

import scala.concurrent.Future

/** Trait used for storing future values.
 *
 * @tparam A Type of stored values.
 */
trait SetterFuture[A] {
  /** Method storing value.
   *
   * @param key Key pointing to value.
   * @param value Future value to store.
   */
  def setFuture(key: String, value: Future[A]): Unit
}
