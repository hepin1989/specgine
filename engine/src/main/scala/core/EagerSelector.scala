package com.specdevs.specgine.core

/** Eager selector of implementation for type `T`, aimed to be used as mixin.
 *
 * @note This may fail if implementation is not valid.
 * @tparam T Type to validate.
 */
trait EagerSelector[T] extends ImplementationSelector[T] {
  this: T =>

  /** Get instance of `T` with implementation.
   *
   * @return Instance of `T` implemented with selector.
   */
  def getInstance: T = this
}
