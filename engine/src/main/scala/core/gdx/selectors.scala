package com.specdevs.specgine.core.gdx

import com.specdevs.specgine.core._

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Application.{ApplicationType => GdxApplicationType}

private[gdx] trait CheckDesktop {
  /** Check if application runs on Desktop platform.
   *
   * @return True if application runs on Desktop.
   */
  def isValid: Boolean = {
    Gdx.app.getType == GdxApplicationType.Desktop
  }
}

private[gdx] trait CheckAndroid {
  /** Check if application runs on Android platform.
   *
   * @return True if application runs on Android.
   */
  def isValid: Boolean = {
    Gdx.app.getType == GdxApplicationType.Android
  }
}

private[gdx] trait CheckIOS {
  /** Check if application runs on iOS platform.
   *
   * @return True if application runs on iOS.
   */
  def isValid: Boolean = {
    Gdx.app.getType == GdxApplicationType.iOS
  }
}

/** Eager selector of Desktop implementation for type `T`, aimed to be used as mixin.
 *
 * @note This may fail if implementation is not valid on other platforms.
 * @tparam T Type to validate.
 */
trait DesktopImplementation[T] extends EagerSelector[T] with CheckDesktop {
  this: T =>
}

/** Eager selector of Android implementation for type `T`, aimed to be used as mixin.
 *
 * @note This may fail if implementation is not valid on other platforms.
 * @tparam T Type to validate.
 */
trait AndroidImplementation[T] extends EagerSelector[T] with CheckAndroid {
  this: T =>
}

/** Eager selector of iOS implementation for type `T`, aimed to be used as mixin.
 *
 * @note This may fail if implementation is not valid on other platforms.
 * @tparam T Type to validate.
 */
trait IOSImplementation[T] extends EagerSelector[T] with CheckIOS {
  this: T =>
}

/** Lazy selector of Desktop implementation wrapper for type `T`.
 *
 * @tparam T Type to validate.
 * @param implementation Implementation of type `T`, passed by name, working on Desktop platform.
 * @constructor Create lazy wrapper for type `T`.
 */
class IfDesktop[T](implementation: => T) extends LazySelector[T](implementation) with CheckDesktop

/** `IfDesktop` companion object to ease wrapping. */
object IfDesktop {
  /** Apply method, wrapping implementation in `IfDesktop`.
   *
   * @tparam T Type to validate.
   * @param implementation Implementation of type `T`, passed by name, working on Desktop platform.
   * @return New `IfDesktop` lazy wrapper for type `T`.
   */
  def apply[T](implementation: => T): IfDesktop[T] = {
    new IfDesktop(implementation)
  }
}

/** Lazy selector of Android implementation wrapper for type `T`.
 *
 * @tparam T Type to validate.
 * @param implementation Implementation of type `T`, passed by name, working on Android platform.
 * @constructor Create lazy wrapper for type `T`.
 */
class IfAndroid[T](implementation: => T) extends LazySelector[T](implementation) with CheckAndroid

/** `IfAndroid` companion object to ease wrapping. */
object IfAndroid {
  /** Apply method, wrapping implementation in `IfAndroid`.
   *
   * @tparam T Type to validate.
   * @param implementation Implementation of type `T`, passed by name, working on Android platform.
   * @return New `IfAndroid` lazy wrapper for type `T`.
   */
  def apply[T](implementation: => T): IfAndroid[T] = {
    new IfAndroid(implementation)
  }
}

/** Lazy selector of iOS implementation wrapper for type `T`.
 *
 * @tparam T Type to validate.
 * @param implementation Implementation of type `T`, passed by name, working on iOS platform.
 * @constructor Create lazy wrapper for type `T`.
 */
class IfIOS[T](implementation: => T) extends LazySelector[T](implementation) with CheckIOS

/** `IfIOS` companion object to ease wrapping. */
object IfIOS {
  /** Apply method, wrapping implementation in `IfIOS`.
   *
   * @tparam T Type to validate.
   * @param implementation Implementation of type `T`, passed by name, working on iOS platform.
   * @return New `IfIOS` lazy wrapper for type `T`.
   */
  def apply[T](implementation: => T): IfIOS[T] = {
    new IfIOS(implementation)
  }
}
