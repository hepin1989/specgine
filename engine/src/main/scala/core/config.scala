package com.specdevs.specgine.core

/** Trait used for values stored by [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]]. */
trait Config extends Any

/** `Boolean` implementation for [[com.specdevs.specgine.core.Config `Config`]].
 *
 * @param self `Boolean` value.
 */
case class BooleanConfig(val self: Boolean) extends AnyVal with Config

/** `Float` implementation for [[com.specdevs.specgine.core.Config `Config`]].
 *
 * @param self `Float` value.
 */
case class FloatConfig(val self: Float) extends AnyVal with Config

/** `Int` implementation for [[com.specdevs.specgine.core.Config `Config`]].
 *
 * @param self `Int` value.
 */
case class IntConfig(val self: Int) extends AnyVal with Config

/** `Long` implementation for [[com.specdevs.specgine.core.Config `Config`]].
 *
 * @param self `Long` value.
 */
case class LongConfig(val self: Long) extends AnyVal with Config

/** `String` implementation for [[com.specdevs.specgine.core.Config `Config`]].
 *
 * @param self `String` value.
 */
case class StringConfig(val self: String) extends AnyVal with Config
