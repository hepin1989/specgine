package com.specdevs.specgine.core

/** Trait mixed-in into classes that use [[com.specdevs.specgine.core.StateManager `StateManager`]]. */
trait StateManagerUser {
  private var theStateManager: Option[StateManager] = None

  /** Getter for instance of [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @return State manager instance.
   */
  implicit def stateManager: StateManager = {
    if (theStateManager.isEmpty) {
      throw new IllegalStateException("You haven't set stateManager.")
    } else {
      theStateManager.get
    }
  }

  /** Setter for instance of [[com.specdevs.specgine.core.StateManager `StateManager`]].
   *
   * @param manager State manager instance.
   */
  def stateManager_=(manager: StateManager): Unit = {
    theStateManager = Some(manager)
  }

  /** Method requesting push of new state.
   *
   * If new state is already on stack, all states between current and this
   * state will be disposed. If requested state is not on stack, current state
   * will be frozen and new state will be added on top.
   *
   * @param name Name of [[com.specdevs.specgine.core.State `State`]].
   * @throws IllegalArgumentException If name does not exists.
   */
  def pushState(name: String): Unit = {
    stateManager.push(name)
  }

  /** Method requesting pop of current state.
   *
   * Current state will be disposed, and top frozen state will become active
   * again.
   */
  def popState(): Unit = {
    stateManager.pop()
  }

  /** Method requesting change of current state.
   *
   * Current state will be disposed, and replaced with given state. If new
   * state is already on stack, `changeState` is equal to `pushState`.
   *
   * @param name Name of [[com.specdevs.specgine.core.State `State`]].
   * @throws IllegalArgumentException If name does not exists.
   */
  def changeState(name: String): Unit = {
    stateManager.change(name)
  }

  /** Method requesting closing of game. */
  def quit(): Unit = {
    stateManager.quit()
  }

  /** Method used to get state info from manager.
   *
   * @param key Key poinint to value.
   * @param target Name of group to look for value, or `None` for direct parent.
   * @return Value associated with key, or `None` if not found.
   * @throws IllegalArgumentException If target is wrong.
   */
  def getStateInfo(key: String, target: Option[String] = None): Option[StateInfo] = {
    stateManager.getFrom(key, target)
  }

  /** Method used to set state info in manager.
   *
   * @param key Key poinint to value.
   * @param value Value to set.
   * @param target Name of group to look for value, or `None` for direct parent.
   * @throws IllegalArgumentException If target is wrong.
   */
  def setStateInfo(key: String, value: StateInfo, target: Option[String] = None): Unit = {
    stateManager.setIn(key, value, target)
  }

  /** Method calling `processFrozen` on frozen state.
   *
   * @param dt Time since last simulation step.
   */
  def processFrozenState(dt: Float): Unit = {
    stateManager.processFrozen(dt)
  }

  /** Method calling `renderFrozen` of frozen state.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def renderFrozenState(alpha: Float): Unit = {
    stateManager.renderFrozen(alpha)
  }

  /** Getter for profiling data.
   *
   * @return Profiling data if `enableProfiling` is set to `true` in instance of `Game` class else `None`.
   */
  def getProfilingData: Option[ProfilingData] = {
    stateManager.getProfilingData
  }
}
