package com.specdevs.specgine.core

/** Trait used for accessing and storing values.
 *
 * It can be easily generated based on mutable values, fields
 * or another accessors using `access` macro from
 * [[com.specdevs.specgine.macros.core `com.specdevs.specgine.macros.core`]]
 * package.
 *
 * @tparam A Type of stored values.
 */
trait Accessor[A] {
  /** Method returning value.
   *
   * @return Stored value.
   */
  def value: A

  /** Method setting a new value.
   *
   * @param what New value associated with `key`.
   */
  def value_=(what: A): Unit
}
