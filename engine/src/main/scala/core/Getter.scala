package com.specdevs.specgine.core

/** Trait used for accessing stored values.
 *
 * @tparam A Type of stored values.
 */
trait Getter[A] {
  /** Method returning value.
   *
   * @param key Key pointing to value.
   * @return Value associated with `key`, `None` if not found.
   */
  def get(key: String): Option[A]
}
