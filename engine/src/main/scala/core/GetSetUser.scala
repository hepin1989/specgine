package com.specdevs.specgine.core

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future,Promise}
import scala.util.{Failure,Success}

import java.util.NoSuchElementException

/** Trait mixed-in into classes that use common access method in SpecGine.
 *
 * All those methods will look for implicit `Getter[A]`, `Setter[A]`,
 * `GetterTarget[A]` or `SetterTarget[A]` and `Provider[A,B]` to get or set
 * value of type `B`. All standard classes in SpecGine come with implicit
 * `Getter`s where it makes sense, but user is responsible for importing
 * implicit `Provider`s.
 *
 * Example use case for those methods include getting configuration options:
 * {{{
 * // assumes, that com.specdevs.specgine.core._ is visible
 * val width: Option[Int] = get[Config,Int]("screenWidth")
 * }}}
 * Such call will look for implicit `Getter[Config]`, which in most cases
 * will be already defined as field inherited from
 * [[com.specdevs.specgine.core.AbstractConfigManager `AbstractConfigManager`]].
 * Also correct `Provider[Config,Int]` will be located, in this case
 * [[com.specdevs.specgine.core.ImplicitIntConfig `ImplicitIntConfig`]].
 * Then `AbstractConfigManager` will be asked if it knows value with key
 * `screenWidth`. If no, it will return `None`, if yes, it will return one of
 * subclasses of [[com.specdevs.specgine.core.Config `Config`]], representing
 * correct type of value. This value will be then matched against
 * [[com.specdevs.specgine.core.IntConfig `IntConfig`]] using
 * `ImplicitIntConfig`s unpack method. In short above call will return
 * `Some(value)` if there is `Int` value defined for key named `screenWidth`,
 * and `None` otherwise.
 *
 * Another use case includes storing of
 * [[com.specdevs.specgine.core.StateInfo `StateInfo`]] values in
 * [[com.specdevs.specgine.core.StateGroup `StateGroup`]]s. In this case,
 * version with target should be used, with `target` equal to group name, or
 * `None` meaning direct parent of currently active group.
 * {{{
 * // to store "score" equal to 0 in group "game states"
 * setIn[StateInfo,Int]("score", 0, "game states")
 * // to get "playerName" value from parent group of current state
 * getFrom[StateInfo,String]("playerName")
 * }}}
 */
trait GetSetUser {
  /** Method getting value, returning `Option`.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param getter Implicit instance of `Getter` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @return Value associated with key, or `None` if key was not found.
   */
  def getOption[A,B](key: String)(implicit getter: Getter[A], provider: Provider[A,B]): Option[B] = {
    provider.unpack(getter.get(key))
  }

  /** Method getting value.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param getter Implicit instance of `Getter` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @return Value associated with key.
   * @throws NoSuchElementException If key was not found.
   */
  def get[A,B](key: String)(implicit getter: Getter[A], provider: Provider[A,B]): B = {
    getOption(key)(getter, provider) match {
      case Some(value) => value
      case _ => throw new NoSuchElementException(s"failed to get value $key")
    }
  }

  /** Method setting value.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param value Value to store.
   * @param setter Implicit instance of `Setter` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   */
  def set[A,B](key: String, value: B)(implicit setter: Setter[A], provider: Provider[A,B]): Unit = {
    setter.set(key, provider.pack(value))
  }

  /** Method setting value if unset.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param value Value to store.
   * @param getter Implicit instance of `Getter` for values of type `A`.
   * @param setter Implicit instance of `Setter` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   */
  def setIfNotSet[A,B](
    key: String,
    value: B
  )(
    implicit
      getter: Getter[A],
      setter: Setter[A],
      provider: Provider[A,B]
  ): Unit = {
    getOption(key)(getter, provider) match {
      case None => set(key, value)(setter, provider)
      case _ => ()
    }
  }

  /** Method getting value from optional target, returning `Option`.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param target Where to look for value, default target if `None`.
   * @param getter Implicit instance of `GetterTarget` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @return Value associated with key, or `None` if key was not found.
   * @throws IllegalArgumentException If target is wrong.
   */
  def getFromOption[A,B](
    key: String,
    target: Option[String] = None
  )(
    implicit
      getter: GetterTarget[A],
      provider: Provider[A,B]
  ): Option[B] = {
    provider.unpack(getter.getFrom(key, target))
  }

  /** Method getting value from optional target.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param target Where to look for value, default target if `None`.
   * @param getter Implicit instance of `GetterTarget` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @return Value associated with key.
   * @throws NoSuchElementException If key was not found.
   * @throws IllegalArgumentException If target is wrong.
   */
  def getFrom[A,B](
    key: String,
    target: Option[String] = None
  )(
    implicit
      getter: GetterTarget[A],
      provider: Provider[A,B]
  ): B = {
    getFromOption(key, target)(getter, provider) match {
      case Some(value) => value
      case _ => throw new NoSuchElementException(s"failed to get value $key")
    }
  }

  /** Method setting value in optional target.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param target Where the value should be set, default target if `None`.
   * @param value Value to store.
   * @param setter Implicit instance of `Setter` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @throws IllegalArgumentException If target is wrong.
   */
  def setIn[A,B](
    key: String,
    value: B,
    target: Option[String] = None
  )(
    implicit
      setter: SetterTarget[A],
      provider: Provider[A,B]
  ): Unit = {
    setter.setIn(key, provider.pack(value), target)
  }

  /** Method setting value in optional target if unset.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param value Value to store.
   * @param target Where the value should be set, default target if `None`.
   * @param getter Implicit instance of `GetterTarget` for values of type `A`.
   * @param setter Implicit instance of `SetterTarget` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @throws IllegalArgumentException If target is wrong.
   */
  def setInIfNotSet[A,B](
    key: String,
    value: B,
    target: Option[String] = None
  )(
    implicit
      getter: GetterTarget[A],
      setter: SetterTarget[A],
      provider: Provider[A,B]
  ): Unit = {
    getFromOption(key, target)(getter, provider) match {
      case None => setIn(key, value, target)(setter, provider)
      case _ => ()
    }
  }

  /** Method getting value from target, returning `Option`.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param target Where the value should be set.
   * @param getter Implicit instance of `GetterTarget` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @return Value associated with key, or `None` if key was not found.
   * @throws IllegalArgumentException If target is wrong.
   */
  def getOption[A,B](
    key: String,
    target: String
  )(
    implicit
      getter: GetterTarget[A],
      provider: Provider[A,B]
  ): Option[B] = {
    getFromOption(key, Some(target))(getter, provider)
  }

  /** Method getting value from target.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param target Where the value should be set.
   * @param getter Implicit instance of `GetterTarget` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @return Value associated with key.
   * @throws NoSuchElementException If key was not found.
   * @throws IllegalArgumentException If target is wrong.
   */
  def get[A,B](key: String, target: String)(implicit getter: GetterTarget[A], provider: Provider[A,B]): B = {
    getFrom(key, Some(target))(getter, provider)
  }

  /** Method setting value in target.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param target Where the value should be set.
   * @param value Value to store.
   * @param setter Implicit instance of `Setter` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @throws IllegalArgumentException If target is wrong.
   */
  def set[A,B](
    key: String,
    value: B,
    target: String
  )(
    implicit
      setter: SetterTarget[A],
      provider: Provider[A,B]
  ): Unit = {
    setIn(key, value, Some(target))(setter, provider)
  }

  /** Method setting value in target if unset.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param value Value to store.
   * @param target Where the value should be set.
   * @param getter Implicit instance of `GetterTarget` for values of type `A`.
   * @param setter Implicit instance of `SetterTarget` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @throws IllegalArgumentException If target is wrong.
   */
  def setIfNotSet[A,B](
    key: String,
    value: B,
    target: Option[String]
  )(
    implicit
      getter: GetterTarget[A],
      setter: SetterTarget[A],
      provider: Provider[A,B]
  ): Unit = {
    setInIfNotSet(key, value, target)(getter, setter, provider)
  }

  /** Method getting value, returning `Future`.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param getter Implicit instance of `GetterFuture` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   * @return Future value associated with key.
   */
  def getFuture[A,B](key: String)(implicit getter: GetterFuture[A], provider: Provider[A,B]): Future[B] = {
    val promise = Promise[B]()
    getter.getFuture(key) onComplete {
      case Success(a) => provider.unpack(Some(a)) match {
        case Some(b) => promise success b
        case None => promise failure (new IllegalStateException)
      }
      case Failure(t) => promise failure t
    }
    promise.future
  }

  /** Method setting value.
   *
   * @tparam A Type of values.
   * @tparam B Type of return value.
   * @param key Key pointing to value.
   * @param value Value to store.
   * @param setter Implicit instance of `SetterFuture` for values of type `A`.
   * @param provider Implicit instance of `Provider` converting between `A` and `B`.
   */
  def setFuture[A,B](key: String, value: Future[B])(implicit setter: SetterFuture[A], provider: Provider[A,B]): Unit = {
    val promise = Promise[A]()
    value onComplete {
      case Success(b) => promise success provider.pack(b)
      case Failure(t) => promise failure t
    }
    setter.setFuture(key, promise.future)
  }
}
