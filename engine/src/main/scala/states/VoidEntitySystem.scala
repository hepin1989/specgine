package com.specdevs.specgine.states

/** A zero dimmensional [[com.specdevs.specgine.states.EntitySystem `EntitySestem`]].
 *
 * This type of [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * refuses to accept any components, i.e. `systemWantsEntity` always
 * returns `false`.
 *
 * It is useful for example to display debug information or render GUI
 * elements that are not modeled using entities.
 *
 * @constructor Create new `VoidEntitySystem` with given priorities.
 * @param inputPriority Priority of input handling. Lower values comes first.
 * @param processPriority Processing priority. The smaller number, the sooner entity is processed.
 * @param renderPriority Rendering priority. The smaller number, the sooner entity is rendered.
 */
abstract class VoidEntitySystem(
    val inputPriority: Int = 0,
    val processPriority: Int = 0,
    val renderPriority: Int = 0)
    extends EntitySystem with Processor0 with Renderer0 {
  def systemWantsEntity(cs: Set[Component]): Boolean = false

  def systemHasEntity(e: Entity): Boolean = false

  def addEntityToSystem(e: Entity, cs: Set[Component]): Unit = {}

  def removeEntityFromSystem(e: Entity): Unit = {}
}
