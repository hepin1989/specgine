package com.specdevs.specgine.states

/** Declaration of rendering method implementation.
 *
 * `renderAll` is common interface called by
 * [[com.specdevs.specgine.states.World `World`]] during its `render`
 * method for all [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s
 * mixning-in [[com.specdevs.specgine.states.Renderer `Renderer`]]
 * trait.
 *
 * All default [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s
 * already provide `RendererImplementation` with suitable `renderAll`
 * method.
 */
trait RendererImplementation {
  /** Method rendering all entities.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def renderAll(alpha: Float): Unit
}

/** Trait for with renderer implementation for zero dimmensional
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s.
 *
 * This implementation is used for example in
 * [[com.specdevs.specgine.states.VoidEntitySystem `VoidEntitySystem`]].
 */
trait Renderer0 extends RendererImplementation {
  /** Rendering method called once every frame.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def render(alpha: Float): Unit = {}

  def renderAll(alpha: Float): Unit = {
    render(alpha)
  }
}

/** Trait for with renderer implementation for one dimmensional
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s.
 *
 * To iterate, it uses `foreachEntity1` from
 * [[com.specdevs.specgine.states.EntityLoop1 `EntityLoop1`]].
 *
 * This implementation is used for example in
 * [[com.specdevs.specgine.states.SingleEntitySystem `SingleEntitySystem`]].
 */
trait Renderer1 extends RendererImplementation with EntityLoop1 {
  /** Rendering method called once every frame, before rendering of entities start.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def beginRender(alpha: Float): Unit = {}

  /** Rendering method called once every frame, after rendering of entities finish.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def endRender(alpha: Float): Unit = {}

  /** Rendering method called for every entity each frame.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   * @param e1 [[com.specdevs.specgine.states.Entity `Entity`]] to render.
   */
  def render(alpha: Float, e1: Entity): Unit = {}

  def renderAll(alpha: Float): Unit = {
    beginRender(alpha)
    foreachEntity1(e1 => render(alpha, e1))
    endRender(alpha)
  }
}

/** Trait for with renderer implementation for two dimmensional
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s.
 *
 * To iterate, it uses `foreachEntity2` from
 * [[com.specdevs.specgine.states.EntityLoop2 `EntityLoop2`]].
 *
 * This implementation is used for example in
 * [[com.specdevs.specgine.states.PairEntitySystem `PairEntitySystem`]].
 */
trait Renderer2 extends RendererImplementation with EntityLoop2 {
  /** Rendering method called once every frame, before rendering of entities start.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def beginRender(alpha: Float): Unit = {}

  /** Rendering method called once every frame, after rendering of entities finish.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   */
  def endRender(alpha: Float): Unit = {}

  /** Rendering method called for every pair of entities each frame.
   *
   * @param alpha Render interpolation between two last simulation steps (0..1).
   * @param e1 [[com.specdevs.specgine.states.Entity `Entity`]] to render.
   * @param e2 [[com.specdevs.specgine.states.Entity `Entity`]] to render.
   */
  def render(alpha: Float, e1: Entity, e2: Entity): Unit = {}

  def renderAll(alpha: Float): Unit = {
    beginRender(alpha)
    foreachEntity2((e1, e2) => render(alpha, e1, e2))
    endRender(alpha)
  }
}
