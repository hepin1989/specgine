package com.specdevs.specgine.states

import com.specdevs.specgine.core.State
import com.specdevs.specgine.input.{KeyReceiver,PointerReceiver}

/** State for managing, processing and rendering of [[com.specdevs.specgine.states.Slide `Slide`]]s.
 *
 * SlideShowState stores a list of
 * [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]]s, which will be
 * shown. [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]] is a list of
 * [[com.specdevs.specgine.states.Slide `Slide`]]s. Instance of
 * [[com.specdevs.specgine.states.Slide `Slide`]] contains infromation about
 * its duration, whether it is brakeable and implementation for assets
 * managment and rendering.

 * If current [[com.specdevs.specgine.states.Slide `Slide`]] is breakable,
 * then when user presses any key, clicks or taps, current
 * [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]] is forced to
 * immediately finish.
 *
 * If [[com.specdevs.specgine.states.Slide `Slide`]] is finished, it is
 * removed from [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]]. If it
 * was broken or [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]] is empty,
 * than whole [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]] is removed from
 * `SlideShowState`.
 *
 * Example implementation of [[com.specdevs.specgine.states.Slide `Slide`]]
 * can be found in [[com.specdevs.specgine.states.gdx]] sub-package.
 *
 * @constructor Creates new state for managing slides.
 * @param nextState Name of the next state which will be pushed on stack next. If an empty
 *        string is supplied, `SlideShowState` will pop from stack.
 */
abstract class SlideShowState(val nextState: String = "") extends State with KeyReceiver with PointerReceiver {
  private var toPlay: List[SlideDeck] = Nil

  private def toPlayUpdate(slideDecks: List[SlideDeck]): Unit = {
    if (slideDecks.isEmpty) {
      if (nextState != "") {
        changeState(nextState)
      } else {
        popState()
      }
    } else {
      if (slideDecks.head.isEmpty) {
        slideDecks.head.doDispose()
        toPlayUpdate(slideDecks.tail)
      } else {
        toPlay = slideDecks
      }
    }
  }
  /** Add [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]]s.
   *
   * @param slideDecks Vararg with [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]]s to add.
   */
  protected def addSlideDecks(slideDecks: SlideDeck*): Unit = {
    slideDecks foreach (_.assetManager = assetManager)
    toPlayUpdate(toPlay ++ slideDecks)
  }

  /** Add [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]], constructed from
   * given [[com.specdevs.specgine.states.Slide `Slide`]]s.
   *
   * New [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]] containing all
   * provided slides is created.
   *
   * @param slides Vararg with [[com.specdevs.specgine.states.SlideDeck `Slides`]]s to add.
   */
  protected def addSlideDeck(slides: Slide*): Unit = {
    addSlideDecks(new SlideDeck(slides: _*))
  }

  /** Add [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]]s, constructed from
   * given [[com.specdevs.specgine.states.Slide `Slide`]]s.
   *
   * New [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]] is created
   * for each provided slide.
   *
   * @param slides Vararg with [[com.specdevs.specgine.states.SlideDeck `Slides`]]s to add.
   */
  protected def addSlides(slides: Slide*): Unit = {
    addSlideDecks(slides.toList map (new SlideDeck(_)): _*)
  }

  override def doInitialize(): Unit = {
    super.doInitialize()
    toPlay foreach (_.doInitialize())
  }

  override def doCreate(): Unit = {
    super.doCreate()
    toPlay foreach (_.doCreate())
  }

  def enter(): Unit = {}

  def leave(): Unit = {}

  override def doDispose(): Unit = {
    toPlay foreach (_.doDispose())
    toPlay = Nil
    super.doDispose()
  }

  override def keyUp(keycode: Int): Boolean = {
    break()
  }

  override def touchUp(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
    break()
  }

  /** Tries to break current slide.
   *
   * Tries to break current slide if it is breakable. It performs all
   * needed cleanup.
   *
   * @return `true` current slide was, `false` if it was impossible.
   */
  protected def break(): Boolean = {
    if (toPlay.head.breakable) {
      toPlay.head.doDispose()
      toPlayUpdate(toPlay.tail)
      true
    } else {
      false
    }
  }

  /** Executes `process` method on current slide.
   *
   * After method execution, processed
   * finished [[com.specdevs.specgine.states.Slide `Slide`]]s
   * and empty [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]]s
   * are cleared from list and disposed.
   *
   * @param dt Time interval.
   */
  def process(dt: Float): Unit = {
    toPlay.head.process(dt)
    toPlayUpdate(toPlay)
  }

  /** Executes `render` method on current slide.
   *
   * @param alpha Time proportion.
   */
  def render(alpha: Float): Unit = {
    if (toPlay.nonEmpty) {
      toPlay.head.render(alpha)
    }
  }

  /** Executes `resize` method on all slides.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit = {
    toPlay foreach (_.resize(x, y))
  }
}
