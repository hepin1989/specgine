package com.specdevs.specgine.states

/** Trait used to signalize [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * that responds to queries.
 */
trait Responder {
  /** Method used to respond to a given message.
   *
   * @param message A message to respond to.
   * @return possible reply (if such exists).
   */
  def respond(message: Message): Option[Reply]
}
