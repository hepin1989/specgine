package com.specdevs.specgine.states

/** Represents messages for queries.
 *
 * Messages are responded to by [[com.specdevs.specgine.states.Responder `Responder`]]s.
 */
trait Message
