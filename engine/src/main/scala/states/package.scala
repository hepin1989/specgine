package com.specdevs.specgine

/** Package providing functionality and implementation of States objects in SpecGine.
 *
 * Classes in this package can be divided into following groups.
 *
 * == Game state ==
 *
 * [[com.specdevs.specgine.states.GameState `GameState`]] is the most
 * important state of every game written with SpecGine. It manages instance
 * of [[com.specdevs.specgine.states.World `World`]], class responsible for
 * bookkeeping instances of [[com.specdevs.specgine.states.Entity `Entity`]].
 *
 * Every single object that exists in the game (e.g. car, hero, enemy) is an
 * [[com.specdevs.specgine.states.Entity `Entity`]]. Each
 * [[com.specdevs.specgine.states.Entity `Entity`]] is associated with
 * a set of [[com.specdevs.specgine.states.Component `Component`]]s.
 * [[com.specdevs.specgine.states.Component `Component`]] represents unique
 * features or attributes (e.g. health points, score or position), shared by
 * objects in game.
 *
 * [[com.specdevs.specgine.states.World `World`]] also contains
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s responsible
 * for selective processing of entities.
 *
 * While [[com.specdevs.specgine.states.World `World`]] is not a `...Manager`
 * per see, access to its functionality can be mixed-in into other classes
 * thanks to [[com.specdevs.specgine.states.WorldUser `WorldUser`]].
 *
 * == Slide-show state ==
 *
 * Classes from this group are responsible for presenting
 * [[com.specdevs.specgine.states.Slide `Slide`]]s, used either for showing
 * a logo of producers at the beginning of a game, in between two different
 * game states to show part of a story or to display credits at the end of game.
 * Basically you can use it in all parts of game, for which interactivity is
 * limited to leaving current slide and going to next one.
 *
 * [[com.specdevs.specgine.states.SlideShowState `SlideShowState`]] class stores
 * and manages a list of [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]]s.
 * Each [[com.specdevs.specgine.states.SlideDeck `SlideDeck`]] is container of
 * [[com.specdevs.specgine.states.Slide `Slide`]]s, with functionality for
 * managing them.
 *
 * [[com.specdevs.specgine.states.Slide `Slide`]] is responsible for loading,
 * processing and rendering assets user wants to show. Example implementation
 * of [[com.specdevs.specgine.states.Slide `Slide`]] can be found in
 * [[com.specdevs.specgine.states.gdx]] sub-package.
 *
 * == Menu states ==
 *
 * [[com.specdevs.specgine.states.MenuState `MenuState`]] trait is a state
 * representing all kinds of menus, like main menu, save menu or configuration
 * screen. Menus are organized in screens, that can be switched between using
 * `changedMenu` method. They are somewhat similar to `Game` and `Screen`
 * classes from LibGDX framework (not to confuse with `Game` class from
 * SpecGine engine).
 *
 * Each menu screen should extend
 * [[com.specdevs.specgine.states.AbstractMenuScreen `AbstractMenuScreen`]]
 * and implement methods for loading assets as well as reading and processing
 * users inputs.
 *
 * Example menu screen based on LibGDX framework is present in
 * [[com.specdevs.specgine.states.gdx]] sub-package.
 *
 * @see [[http://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-1/ ''
 *      t-machine.org web page'']], for introduction to Entity frameworks.
 */
package object states
