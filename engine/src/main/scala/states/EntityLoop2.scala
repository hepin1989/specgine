package com.specdevs.specgine.states

/** Trait with two dimmensional [[com.specdevs.specgine.states.Entity `Entity`]] loop.
 *
 * It is used by [[com.specdevs.specgine.states.Processor2 `Processor2`]]
 * and [[com.specdevs.specgine.states.Renderer2 `Renderer2`]].
 */
trait EntityLoop2 {
  /** `foreach` iterating through set of pairs of [[com.specdevs.specgine.states.Entity `Entity`]] instances.
   *
   * @param f An action to perform on each pair of entities.
   */
  def foreachEntity2(f: (Entity, Entity) => Unit): Unit
}
