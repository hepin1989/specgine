package com.specdevs.specgine.states

import com.specdevs.specgine.core.{ConfigManagerUser,GetSetUser,StateManagerUser}
import com.specdevs.specgine.assets.AssetManagerUser
import com.specdevs.specgine.animation.TweenManagerUser

/** Trait for processing instances of [[com.specdevs.specgine.states.Entity `Entity`]].
 *
 * `EntitySystem` processes instances of [[com.specdevs.specgine.states.Entity `Entity`]].
 * It should itself manage a list of entities and its components it is interested in.
 * Instances of `EntitySystem` when registered in
 * [[com.specdevs.specgine.states.World `World`]] are informed about:
 *   - New registered entities (`addEntityToSystem` if `systemWantsEntity`).
 *   - Removed entities (`removeEntityFromSystem` if `systemHasEntity`).
 *   - Entities that changed set of components, but not components values
 *     (`removeEntityFromSystem` if `systemHasEntity` and not `systemWantsEntity`,
 *      `addEntityToSystem` if `systemWantsEntity`).
 *
 * `EntitySystem`s can be used for processing and rendering entities.
 * Systems that should process must mix-in
 * [[com.specdevs.specgine.states.Processor `Processor`]]
 * and those that want to render, should mix-in
 * [[com.specdevs.specgine.states.Renderer `Renderer`]].
 * Only `EntitySystem`s mixing-in those traits will be considered
 * for processing or rendering. It allows us to spare some time,
 * because usually system will do only processing or rendering, not both.
 *
 * There are three default types of `EntitySystem`s:
 *   - [[com.specdevs.specgine.states.VoidEntitySystem `VoidEntitySystem`]] - for
 *     systems not accepting entities, but processing or rendering
 *     once per frame (for example to display debug information).
 *   - [[com.specdevs.specgine.states.SingleEntitySystem `SingleEntitySystem`]] - for
 *     systems accepting entities and issuing process or render
 *     once per entity per frame (for example to move entities around or
 *     render them).
 *   - [[com.specdevs.specgine.states.PairEntitySystem `PairEntitySystem`]] - for
 *     systems accepting entities and issuing process or render
 *     once per pair of entities per frame (for example to handle
 *     collisions).
 *
 * `EntitySystem`s can also be used for receiving and responding to messages.
 * Systems that receive messages are [[com.specdevs.specgine.states.Receiver `Receiver`]]s
 * and those that respond to messages are [[com.specdevs.specgine.states.Responder `Responder`]]s.
 * Systems can be notified using `send` method, or queried using
 * `query`, `queryFirst`, `queryAsync` and `queryFirstAsync`.
 *
 * Example usage for synchronous query:
 * {{{
 *   case class Ping() extends Message
 *   case class Pong() extends Reply // ...
 *
 *   class RespondSystem with Responder {
 *     def respond(message: Message): Option[Reply] = Some(Pong())
 *     // ...
 *   }
 *
 *   queryFirst(Ping()) match {
 *     case Some(Pong()) =>  // got pong
 *     case _ =>  // no reply
 *   }
 * }}}
 *
 * @see [[http://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-1/ ''
 *      t-machine.org web page'']], for introduction to Entity frameworks.
 */
trait EntitySystem
  extends AssetManagerUser with ConfigManagerUser with GetSetUser with StateManagerUser with WorldUser
    with TweenManagerUser {
  /** Checks whether provided set of [[com.specdevs.specgine.states.Component `Component`]]s are wanted.
   *
   * @param cs Set of [[com.specdevs.specgine.states.Component `Component`]]s to check.
   * @return `true` if given set contains all necesary [[com.specdevs.specgine.states.Component `Component`]]s,
   *         `false` otherwise.
   */
  def systemWantsEntity(cs: Set[Component]): Boolean

  /** Declares whether provided [[com.specdevs.specgine.states.Entity `Entity`]] is in the `EntitySystem`.
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to check.
   * @return `true` if `EntitySystem` has given [[com.specdevs.specgine.states.Entity `Entity`]], `false` otherwise.
   */
  def systemHasEntity(e: Entity): Boolean

  /** Adds [[com.specdevs.specgine.states.Entity `Entity`]] and a set of
   * [[com.specdevs.specgine.states.Component `Component`]]s associated with it to the `EntitySystem`.
   *
   * This methos will be also called, when set of
   * [[com.specdevs.specgine.states.Component `Component`]]s associated with
   * [[com.specdevs.specgine.states.Entity `Entity`]] changed.
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to be added.
   * @param cs Set of [[com.specdevs.specgine.states.Component `Component`]]s to be added.
   */
  def addEntityToSystem(e: Entity, cs: Set[Component]): Unit

  /** Removes [[com.specdevs.specgine.states.Entity `Entity`]] from the `EntitySystem`.
   *
   * @param e [[com.specdevs.specgine.states.Entity `Entity`]] to be removed.
   */
  def removeEntityFromSystem(e: Entity): Unit

  /** Method called, when `EntitySystem` is initialized for the first time.
   *
   * This is perfect place to load your assets or set some configuration.
   */
  def initialize(): Unit

  /** Helper method correctly calling `initialize`, used by
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doInitialize(): Unit = {
    initialize()
  }

  /** Method called, when `EntitySystem` is loaded.
   *
   * This is perfect place store loaded assets or allocate
   * local data structures. You do not have guarantee, that `resize` was
   * called.
   */
  def create(): Unit

  /** Helper method correctly calling `create`, used by
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doCreate(): Unit = {
    create()
  }

  /** Method called, when `EntitySystem` becomes active.
   *
   * This is last method called before `process` or `render`,
   * you already can be sure that `resize` was called. It is perfect place to prepare
   * rendering. This can be called multiple times if `EntitySystem` becomes frozen and
   * then back active.
   */
  def enter(): Unit

  /** Helper method correctly calling `enter`, used by
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doEnter(): Unit = {
    enter()
  }

  /** Method called, when game state becomes frozen.
   *
   * This is perfect place to deallocate all resources allocated inside
   * `enter` method. It will be called when game state frozen, but also
   * just before it is going to be disposed.
   */
  def leave(): Unit

  /** Helper method correctly calling `leave`, used by
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doLeave(): Unit = {
    leave()
  }

  /** Method called, when state is permamently removed from [[com.specdevs.specgine.states.World `World`]].
   *
   * Remember to deallocate all resources you allocated yourself inside
   * `create` or `initialize`.
   */
  def dispose(): Unit

  /** Helper method correctly calling `dispose`, used by
   * [[com.specdevs.specgine.states.World `World`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doDispose(): Unit = {
    dispose()
    disposeAssets()
    disposeTweens()
  }

  /** Method called game states is frozen and screen size changed.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resizeFrozen(x: Int, y: Int): Unit = {}

  /** Method called when size of screen changed.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit = {}

  /** Defines priority of `EntitySystem` for inputs handling.
   *
   * @return Priority of input handling, lower values come earlier.
   */
  def inputPriority: Int

  /** Defines priority of `EntitySystem` for processing.
   *
   * @return Priority of processing, lower values come earlier.
   */
  def processPriority: Int

  /** Defines priority of `EntitySystem` for rendering.
   *
   * @return Priority of rendering, lower values come earlier.
   */
  def renderPriority: Int
}
