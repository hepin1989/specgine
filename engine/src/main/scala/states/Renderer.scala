package com.specdevs.specgine.states

/** Trait used to signalize, that [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * which should render entities.
 *
 * Only [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]s
 * mixning-in `Renderer` will be informed
 * [[com.specdevs.specgine.states.World `World`]], that they should
 * render entities.
 */
trait Renderer extends RendererImplementation
