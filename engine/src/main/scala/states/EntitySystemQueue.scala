package com.specdevs.specgine.states

import scala.collection.mutable.ArrayBuffer

private[states] class EntitySystemQueue[A <: EntitySystem](priority: (A => Int) = ((system: A) => 0)) {
  private val data = ArrayBuffer.empty[A]

  def +=(s: A): Unit = {
    val index = data.lastIndexWhere(priority(_) <= priority(s))
    data.insert(index+1, s)
  }

  def foreach(f: A => Unit): Unit = {
    data foreach f
  }

  def clear(): Unit = {
    data.clear()
  }
}
