package com.specdevs.specgine.states

import scala.collection.mutable.HashSet

/** A two dimmensional [[com.specdevs.specgine.states.EntitySystem `EntitySestem`]].
 *
 * This type of [[com.specdevs.specgine.states.EntitySystem `EntitySestem`]]
 * accepts entities and stores them in two entity containers in protected
 * values `components1` and `components2`. Those containers should be modeled using
 * "`ComponentsSpec with ComponentsManager`". Logic to determine if
 * [[com.specdevs.specgine.states.Entity `Entity`]] is wanted, is passed directly
 * to implementations in [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]].
 * One [[com.specdevs.specgine.states.Entity `Entity`]] can be added to
 * one or two entity containers.
 *
 * `PairEntitySystem` respects groups defined in
 * [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]].
 * First, all unordered entities are processed, then
 * all entities ordered with increasing group value. For entities
 * with equal group number, processing order is undefined.
 * Entities from `components1` are selected first, then from `components2`.
 * For example, if there are two groups, 1 and 2, processing will occur
 * in following order:
 *   - unordered from `components1`, unordered from `components2`,
 *   - unordered from `components1`, group 1 from `components2`,
 *   - unordered from `components1`, group 2 from `components2`,
 *   - group 1 from `components1`, unordered from `components2`,
 *   - group 2 from `components1`, unordered from `components2`,
 *   - group 1 from `components1`, group 1 from `components2`,
 *   - group 1 from `components1`, group 2 from `components2`,
 *   - group 2 from `components1`, group 1 from `components2`,
 *   - group 2 from `components1`, group 2 from `components2`.
 *
 * `components1` and `components2` containers are usually created using
 * macro `withManager` from [[com.specdevs.specgine.macros.states]] package.
 *
 * It is useful for example for collision detection or entity sensors system,
 * where one container represents `Colliders` or `Sensors` (active entities)
 * and another container `Colidees` or `VisibleObjects` (passive entities).
 *
 * @constructor Create new `PairEntitySystem` with given priorities.
 * @param inputPriority Priority of input handling. Lower values comes first.
 * @param processPriority Processing priority. The smaller number, the sooner entity is processed.
 * @param renderPriority Rendering priority. The smaller number, the sooner entity is rendered.
 */
abstract class PairEntitySystem(
    val inputPriority: Int = 0,
    val processPriority: Int = 0,
    val renderPriority: Int = 0)
    extends EntitySystem with Processor2 with Renderer2 {
  /** First container of entities and their associated components.
   *
   * It should mix its definition from [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]]
   * with implementation of its functionality from
   * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]].
   */
  protected val components1: ComponentsManager with ComponentsSpec

  /** Second container of entities and their associated components.
   *
   * It should mix its definition from [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]]
   * with implementation of its functionality from
   * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]].
   */
  protected val components2: ComponentsManager with ComponentsSpec

  private val unordered1 = new HashSet[Entity]

  private val unordered2 = new HashSet[Entity]

  private val grouped1 = new EntitySetQueue

  private val grouped2 = new EntitySetQueue

  def systemWantsEntity(cs: Set[Component]): Boolean = {
    (components1 wants cs) || (components2 wants cs)
  }

  def systemHasEntity(e: Entity): Boolean = {
    (components1 has e) || (components2 has e)
  }

  def addEntityToSystem(e: Entity, cs: Set[Component]): Unit = {
    if (components1 wants cs) {
      components1.add(e, cs)
      components1.group(e) match {
        case Some(group) => grouped1(group) += e
        case None => unordered1 += e
      }
    }
    if (components2 wants cs) {
      components2.add(e, cs)
      components2.group(e) match {
        case Some(group) => grouped2(group) += e
        case None => unordered2 += e
      }
    }
    ()
  }

  def removeEntityFromSystem(e: Entity): Unit = {
    if (components1 has e) {
      components1.remove(e)
      unordered1 -= e
      for (bag <- grouped1) {
        bag -= e
        ()
      }
    }
    if (components2 has e) {
      components2.remove(e)
      unordered2 -= e
      for (bag <- grouped2) {
        bag -= e
        ()
      }
    }
  }

  def foreachEntity2(f: (Entity, Entity) => Unit): Unit = {
    for (e1 <- unordered1; e2 <- unordered2) {
      f(e1, e2)
    }
    for (e1 <- unordered1; bag <- grouped2; e2 <- bag) {
      f(e1, e2)
    }
    for (bag <- grouped1; e1 <- bag; e2 <- unordered1) {
      f(e1, e2)
    }
    for (bag1 <- grouped1; e1 <- bag1; bag2 <- grouped2; e2 <- bag2) {
      f(e1, e2)
    }
  }
}
