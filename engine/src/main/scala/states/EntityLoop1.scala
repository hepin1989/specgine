package com.specdevs.specgine.states

/** Trait with one dimmensional [[com.specdevs.specgine.states.Entity `Entity`]] loop.
 *
 * It is used by [[com.specdevs.specgine.states.Processor1 `Processor1`]]
 * and [[com.specdevs.specgine.states.Renderer1 `Renderer1`]].
 */
trait EntityLoop1 {
  /** `foreach` iterating through set of [[com.specdevs.specgine.states.Entity `Entity`]] instances.
   *
   * @param f An action to perform on each entity.
   */
  def foreachEntity1(f: Entity => Unit): Unit
}
