package com.specdevs.specgine.states

import com.specdevs.specgine.core.State
import com.specdevs.specgine.input.{KeyReceiver,PointerReceiver}

import scala.collection.mutable.HashMap

/** State implementation for managing menu screens.
 *
 * `MenuState` stores multiple menu screens, that are implementations of
 * [[com.specdevs.specgine.states.AbstractMenuScreen `AbstractMenuScreen`]],
 * but only one of them is shown. To switch between screens, method
 * `changeMenu` should be used. It is visible from `MenuState` itself,
 * from any menu screen or classes extending
 * [[com.specdevs.specgine.states.MenuSwitcher `MenuSwitcher`]].
 *
 * To create a menu in game, user should first create a menu screen and
 * then add it to `MenuState` using `addMenuState` inside `initialize`
 * method. If no screen is added with `default` set to `true`, first added
 * screen becomes the current one.
 */
trait MenuState extends State with KeyReceiver with PointerReceiver with MenuSwitcher {
  private val screens = new HashMap[String, AbstractMenuScreen]

  private var currentScreen: Option[AbstractMenuScreen] = None

  private var width = 0

  private var height = 0

  /** Adds given menu screen to `MenuState`.
   *
   * If it is the first screen or is marked as default screen than it
   * is set as the ''current'' screen.
   *
   * @param name Name of screen.
   * @param screes Menu screen to be added.
   * @param default Declares wheteher this screen will be set as the current one.
   * @throws IllegalArgumentException If menu screen with given name already exists.
   */
  protected def addMenuScreen(name: String, screen: AbstractMenuScreen, default: Boolean = false): Unit = {
    if (screens.isDefinedAt(name)) {
      throw new IllegalArgumentException(s"MenuScreen $name is already added.")
    }
    screen.assetManager = assetManager
    screen.stateManager = stateManager
    screen.menuSwitcher = this
    screens += name -> screen
    if (currentScreen.isEmpty || default) {
      currentScreen = Some(screen)
    }
  }

  /** Changes menu screen to the one with given name.
   *
   * Executes `doLeave` on current screen, `resize` new screen and
   * executes `doEnter` method on it.
   *
   * @param name Name of new menu screen.
   */
  def changeMenu(name: String): Unit = {
    if (!screens.isDefinedAt(name)) {
      throw new IllegalArgumentException(s"MenuScreen $name doesn't exist.")
    }
    currentScreen foreach (_.doLeave())
    val screen = screens(name)
    screen.resize(width, height)
    screen.doEnter()
    currentScreen = Some(screen)
  }

  override def doInitialize(): Unit = {
    super.doInitialize()
    screens.values foreach (_.doInitialize())
  }

  override def doCreate(): Unit = {
    super.doCreate()
    screens.values foreach (_.doCreate())
  }

  /** Executed when current screen becomes active.
   *
   * Calls `doEnter` on current screen, which in turn calls its `enter` method.
   * @note Override on your own risk.
   */
  def enter(): Unit = {
    currentScreen foreach (_.doEnter())
  }

  /** Executed when current screen becomes inactive.
   *
   * Calls `doLeave` on current screen, which in turn calls its `leave` method.
   *
   * @note Override on your own risk.
   */
  def leave(): Unit = {
    currentScreen foreach (_.doLeave())
  }

  override def doDispose(): Unit = {
    screens.values foreach (_.doDispose())
    screens.clear()
    currentScreen = None
    super.doDispose()
  }

  /** Method called when size of screen changes.
   *
   * Executes `resize` method on current screen.
   *
   * @param X screen width.
   * @param Y screen height.
   */
  def resize(x: Int, y: Int): Unit = {
    width = x
    height = x
    currentScreen foreach (_.resize(x, y))
  }

  /** Executes `process` method on current screen.
   *
   * @param dt Time interval.
   */
  def process(dt: Float): Unit = {
    currentScreen foreach (_.process(dt))
  }

  /** Executes `render` method on current screen.
   *
   * @param alpha Time proportion.
   */
  def render(alpha: Float): Unit = {
    currentScreen foreach (_.render(alpha))
  }

  override def keyDown(keycode: Int): Boolean = {
    currentScreen exists (_.keyDown(keycode))
  }

  override def keyUp(keycode: Int): Boolean = {
    currentScreen exists (_.keyUp(keycode))
  }

  override def keyTyped(keycode: Char): Boolean = {
    currentScreen exists (_.keyTyped(keycode))
  }

  override def touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
    currentScreen exists (_.touchDown(x, y, pointer, button))
  }

  override def touchUp(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
    currentScreen exists (_.touchUp(x, y, pointer, button))
  }

  override def touchDragged(x: Int, y: Int, pointer: Int): Boolean = {
    currentScreen exists (_.touchDragged(x, y, pointer))
  }

  override def mouseMoved(x: Int, y: Int): Boolean = {
    currentScreen exists (_.mouseMoved(x, y))
  }

  override def scrolled(amount: Int): Boolean = {
    currentScreen exists (_.scrolled(amount))
  }
}
