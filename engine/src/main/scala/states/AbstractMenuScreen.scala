package com.specdevs.specgine.states

import com.specdevs.specgine.assets.AssetManagerUser
import com.specdevs.specgine.core.{GetSetUser,StateManagerUser}
import com.specdevs.specgine.input.{KeyReceiver,PointerReceiver}

/** Trait that is parent of all menu screens in [[com.specdevs.specgine.states.MenuState `MenuState`]].
 *
 * User should implement the following methods passed to menu screen from
 * [[com.specdevs.specgine.core.State `State`]]:
 *   - `initialize`,
 *   - `create`,
 *   - `enter`,
 *   - `leave`,
 *   - `dispose`.
 *
 *  And:
 *   - `resize`,
 *   - `process`,
 *   - `render`.
 *
 *  There are two extra methods to implement used to render background for menu:
 *   - `processBackground`,
 *   - `renderBackground`.
 *
 * Each menu screen should implement its own input handling routines.
 *
 * Example implementation using LibGDX framework can be found in
 * [[com.specdevs.specgine.states.gdx]] sub-package.
 */
trait AbstractMenuScreen
  extends KeyReceiver with PointerReceiver with AssetManagerUser with GetSetUser with StateManagerUser {
  private var theMenuSwitcher: Option[MenuSwitcher] = None

  /** Getter for instance of [[com.specdevs.specgine.states.MenuSwitcher `MenuSwitcher`]] object.
   *
   * @return Instance of [[com.specdevs.specgine.states.MenuSwitcher `MenuSwitcher`]].
   */
  def menuSwitcher: MenuSwitcher = {
    if (theMenuSwitcher.isEmpty) {
      throw new IllegalStateException("You haven't set menuSwitcher.")
    } else {
      theMenuSwitcher.get
    }
  }

  /** Setter for instance of [[com.specdevs.specgine.states.MenuSwitcher `MenuSwitcher`]] object in
   * `AbstractMenuScreen`.
   *
   * @param switcher [[com.specdevs.specgine.states.MenuSwitcher `MenuSwitcher`]] instance.
   */
  def menuSwitcher_=(switcher: MenuSwitcher): Unit = {
    theMenuSwitcher = Some(switcher)
  }

  /** Method called, when menu screen is initialized for the first time.
   *
   * This is perfect place to load your assets or set some configuration.
   */
  def initialize(): Unit

  /** Helper method correctly calling `initialize`, used by
   * [[com.specdevs.specgine.states.MenuState `MenuState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doInitialize(): Unit = {
    initialize()
  }

  /** Method called, when menu screen is loaded.
   *
   * This is perfect place store loaded assets or allocate
   * local data structures. You do not have guarantee, that `resize` was
   * called.
   */
  def create(): Unit

  /** Helper method correctly calling `create`, used by
   * [[com.specdevs.specgine.states.MenuState `MenuState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doCreate(): Unit = {
    create()
  }

  /** Method called, when menu screen becomes active.
   *
   * This is last method called before `process` or `render`,
   * you already can be sure that `resize` was called. It is perfect place to prepare
   * rendering.
   */
  def enter(): Unit

  /** Helper method correctly calling `enter`, used by
   * [[com.specdevs.specgine.states.MenuState `MenuState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doEnter(): Unit = {
    enter()
  }

  /** Method called, when menu screen becomes inactive.
   *
   * This is perfect place to deallocate all resources allocated inside
   * `enter` method. It will be called just before it is disposed or when it becomes inactive.
   */
  def leave(): Unit

  /** Helper method correctly calling `leave`, used by
   * [[com.specdevs.specgine.states.MenuState `MenuState`]].
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doLeave(): Unit = {
    leave()
  }

  /** Method called, when menu screen is permamently removed.
   *
   * Remember to deallocate all resources you allocated yourself inside
   * `create` or `initialize`.
   */
  def dispose(): Unit

  /** Helper method correctly calling `dispose`, used by
   * [[com.specdevs.specgine.states.MenuState `MenuState`]].
   *
   * It also dsiposes all assets loaded by menu screen.
   *
   * @note Do not use explicitly.
   * @note Overload only if you know what you are doing.
   */
  def doDispose(): Unit = {
    dispose()
    disposeAssets()
  }

  /** Method called when size of screen changes.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  def resize(x: Int, y: Int): Unit

  /** Method for processing elements in bacground.
   *
   * @param dt Time simulation step.
   */
  def processBackground(dt: Float): Unit

  /** Method for processing main elements.
   *
   * @param dt Time simulation step.
   */
  def process(dt: Float): Unit

  /** Method for rendering elements in bacground.
   *
   * @param alpha Render interpolation between two processing steps.
   */
  def renderBackground(alpha: Float): Unit

  /** Method for rendering main elements.
   *
   * @param alpha Render interpolation between two processing steps.
   */
  def render(alpha: Float): Unit

  /** Change menu to the one with given name.
   *
   * @param name Name of next menu.
   */
  def changeMenu(name: String): Unit = {
    menuSwitcher.changeMenu(name)
  }
}
