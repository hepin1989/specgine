package com.specdevs.specgine.assets

/** Special descriptor used for guessing asset type.
 *
 * If `GuessAsset` is provided as
 * [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]],
 * [[com.specdevs.specgine.assets.AbstractAssetManager `AbstractAssetManager`]]
 * implementation should try to guess
 * [[com.specdevs.specgine.assets.Asset `Asset`]] type based on its file
 * extension.
 */
case object GuessAsset extends AssetDescriptor {
  val extensions: List[String] = Nil
}
