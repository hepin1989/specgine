package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.scenes.scene2d.ui.{Skin => GdxSkin}

/** `Skin` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `Skin` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/scenes/scene2d/ui/Skin.html ''
 *      LibGDX API'']], for `Skin` description.
 */
case class SkinAsset(val self: GdxSkin) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
