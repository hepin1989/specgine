package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import spray.json.JsValue

/** `Json` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self `JsValue` object.
 * @see [[https://github.com/spray/spray-json ''Spray-JSON Home Page'']], for `JsValue` description.
 */
case class JsonAsset(val self: JsValue) extends AnyVal with Asset {
  def dispose(): Unit = {}
}
