package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.{Pixmap => GdxPixmap}
import com.badlogic.gdx.assets.loaders.PixmapLoader.{PixmapParameter => GdxPixmapParameter}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.PixmapAsset `PixmapAsset`]].
 *
 * @constructor New instance of `PixmapDescriptor`.
 */
case class PixmapDescriptor() extends AssetDescriptorGdx {
  type T = GdxPixmap

  type P = GdxPixmapParameter

  val extensions: List[String] = List("bmp")

  val assetClass: Class[GdxPixmap] = classOf[GdxPixmap]

  def loaderParameters: GdxPixmapParameter = new GdxPixmapParameter

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxPixmap) => Some(PixmapAsset(gdxAsset))
    case _ => None
  }
}
