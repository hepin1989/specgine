package com.specdevs.specgine.assets.gdx

import com.badlogic.gdx.assets.{AssetDescriptor => GdxAssetDescriptor,AssetManager => GdxAssetManager}
import com.badlogic.gdx.assets.{AssetLoaderParameters => GdxAssetLoaderParameters}
import com.badlogic.gdx.assets.loaders.{AsynchronousAssetLoader => GdxAsynchronousAssetLoader}
import com.badlogic.gdx.files.{FileHandle => GdxFileHandle}
import com.badlogic.gdx.utils.{Array => GdxArray}

import spray.json.{JsValue,JsonParser}

/** AssetLoader to load [[com.specdevs.specgine.assets.gdx.JsonAsset `JsonAsset`]].
 *
 * Passed to [[com.specdevs.specgine.assets.gdx.JsonAssetDescriptor `JsonAssetDescriptor`]].
 */
class JsonLoader
  extends GdxAsynchronousAssetLoader[JsValue, JsonLoader.JsonLoaderParameters](new AssetResolver) {
  private var jsValue: Option[JsValue] = None

  /** Method for asynchronous loading of [[com.specdevs.specgine.assets.gdx.JsonAsset `JsonAsset`]].
   *
   * @param manager LibGDX Asset manager.
   * @param fileName Name of file to be loaded.
   * @param fileHandle Handle to file to be loaded.
   * @param parameter Parameters used in loading asset.
   */
  def loadAsync(
    manager: GdxAssetManager,
    fileName: String,
    fileHandle: GdxFileHandle,
    parameter: JsonLoader.JsonLoaderParameters
  ): Unit = {
    jsValue = Some(JsonParser(fileHandle.readString()))
  }

  /** Method for loading of [[com.specdevs.specgine.assets.gdx.JsonAsset `JsonAsset`]].
   *
   * @param manager LibGDX Asset manager.
   * @param fileName Name of file to be loaded.
   * @param fileHandle Handle to file to be loaded.
   * @param parameter Parameters used in loading asset.
   */
  def loadSync(
    manager: GdxAssetManager,
    fileName: String,
    fileHandle: GdxFileHandle,
    parameter: JsonLoader.JsonLoaderParameters
  ): JsValue = {
    val tempJsValue = jsValue.get
    this.jsValue = None
    tempJsValue
  }

  /** Returns dependencies of loading [[com.specdevs.specgine.assets.gdx.JsonAsset `JsonAsset`]].
   *
   * @param fileName Name of file to be loaded.
   * @param fileHandle Handle to file to be loaded.
   * @param parameter Parameters used in loading asset.
   */
  def getDependencies(
    fileName: String,
    fileHandle: GdxFileHandle,
    parameter: JsonLoader.JsonLoaderParameters
  ): GdxArray[GdxAssetDescriptor[_]] = {
    new GdxArray[GdxAssetDescriptor[_]]
  }
}

/** Companion object for [[com.specdevs.specgine.assets.gdx.JsonLoader `JsonLoader`]] class. */
object JsonLoader {
  /** Parameter class to be passed to [[com.specdevs.specgine.assets.gdx.JsonAssetDescriptor `JsonAssetDescriptor`]]
   * if additional configuration is necessary.
   */
  class JsonLoaderParameters extends GdxAssetLoaderParameters[JsValue]
}
