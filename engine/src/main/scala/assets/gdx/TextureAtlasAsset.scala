package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.g2d.{TextureAtlas => GdxTextureAtlas}

/** `TextureAtlas` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `TextureAtlas` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g2d/TextureAtlas.html ''
 *      LibGDX API'']], for `TextureAtlas` description.
 */
case class TextureAtlasAsset(val self: GdxTextureAtlas) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
