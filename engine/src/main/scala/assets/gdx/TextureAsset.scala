package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.{Texture => GdxTexture}

/** `Texture` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `Texture` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Texture.html ''LibGDX API'']],
 *      for `Texture` description.
 */
case class TextureAsset(val self: GdxTexture) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
