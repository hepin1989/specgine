package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

/** `List` of `String` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self List of strings.
 */
case class StringListAsset(val self: List[String]) extends AnyVal with Asset {
  def dispose(): Unit = {}
}
