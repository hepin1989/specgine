package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.files.{FileHandle => GdxFileHandle}
import com.badlogic.gdx.graphics.g2d.{ParticleEffect => GdxParticleEffect}
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader.{ParticleEffectParameter => GdxParticleEffectParameter}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.ParticleEffectAsset `ParticleEffectAsset`]].
 *
 * @constructor New instance of `ParticleEffectDescriptor`.
 * @param atlasFile File name of atlas file or `None` if default.
 * @param imagesDir `FileHandle` to images dir or `None` if default.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/files/FileHandle.html ''LibGDX API'']],
 *      for `FileHandle` description.
 */
case class ParticleEffectDescriptor(
    atlasFile: Option[String] = None,
    imagesDir: Option[GdxFileHandle] = None) extends AssetDescriptorGdx {
  type T = GdxParticleEffect

  type P = GdxParticleEffectParameter

  val extensions: List[String] = List("p")

  val assetClass: Class[GdxParticleEffect] = classOf[GdxParticleEffect]

  def loaderParameters: GdxParticleEffectParameter = {
    val params = new GdxParticleEffectParameter()
    if (atlasFile.isDefined) params.atlasFile = atlasFile.get
    if (imagesDir.isDefined) params.imagesDir = imagesDir.get
    params
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxParticleEffect) => Some(ParticleEffectAsset(gdxAsset))
    case _ => None
  }
}
