package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.graphics.g2d.{BitmapFont => GdxBitmapFont}

/** `BitmapFont` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `BitmapFont` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g2d/BitmapFont.html ''
 *      LibGDX API'']], for `BitmapFont` description.
 */
case class BitmapFontAsset(val self: GdxBitmapFont) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
