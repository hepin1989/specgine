package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.audio.{Sound => GdxSound}

/** `Sound` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `Sound` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/audio/Sound.html ''LibGDX API'']],
 *      for `Sound` description.
 */
case class SoundAsset(val self: GdxSound) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
