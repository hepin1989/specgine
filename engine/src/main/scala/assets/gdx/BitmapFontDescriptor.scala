package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.assets.loaders.BitmapFontLoader.{BitmapFontParameter => GdxBitmapFontParameter}
import com.badlogic.gdx.graphics.Texture.{TextureFilter => GdxTextureFilter}
import com.badlogic.gdx.graphics.g2d.BitmapFont.{BitmapFontData => GdxBitmapFontData}
import com.badlogic.gdx.graphics.g2d.{BitmapFont => GdxBitmapFont}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.BitmapFontAsset `BitmapFontAsset`]].
 *
 * @constructor New instance of `BitmapFontDescriptor`.
 * @param flip If `true` font will be vertically flipped.
 * @param minFilter Filter used for zooming font texture.
 * @param magFilter Filter used for zooming font texture.
 * @param bitmapFontData Font data used instead of loading texture directly.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/g2d/BitmapFont.BitmapFontData.html ''
 *  LibGDX API'']], for `BitmapFontData` description.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/graphics/Texture.TextureFilter.html ''
 *      LibGDX API'']], for `TextureFilter` description.
 */
case class BitmapFontDescriptor(
    flip: Boolean = false,
    minFilter: GdxTextureFilter = GdxTextureFilter.Nearest,
    magFilter: GdxTextureFilter = GdxTextureFilter.Nearest,
    bitmapFontData: Option[GdxBitmapFontData] = None) extends AssetDescriptorGdx {
  type T = GdxBitmapFont

  type P = GdxBitmapFontParameter

  val extensions: List[String] = List("fnt")

  val assetClass: Class[GdxBitmapFont] = classOf[GdxBitmapFont]

  def loaderParameters: GdxBitmapFontParameter = {
    val params = new GdxBitmapFontParameter()
    params.flip = flip
    params.minFilter = minFilter
    params.magFilter = magFilter
    if (bitmapFontData.isDefined) params.bitmapFontData = bitmapFontData.get
    params
  }

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxBitmapFont) => Some(BitmapFontAsset(gdxAsset))
    case _ => None
  }
}
