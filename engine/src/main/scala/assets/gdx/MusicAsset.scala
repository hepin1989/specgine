package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.audio.{Music => GdxMusic}

/** `Music` implementation for [[com.specdevs.specgine.assets.Asset `Asset`]].
 *
 * @param self LibGDX `Music` value.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/audio/Music.html ''LibGDX API'']],
 *      for `Music` description.
 */
case class MusicAsset(val self: GdxMusic) extends AnyVal with Asset {
  def dispose(): Unit = {
    self.dispose()
  }
}
