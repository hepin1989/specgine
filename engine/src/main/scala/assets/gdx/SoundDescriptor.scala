package com.specdevs.specgine.assets.gdx

import com.specdevs.specgine.assets._

import com.badlogic.gdx.audio.{Sound => GdxSound}
import com.badlogic.gdx.assets.loaders.SoundLoader.{SoundParameter => GdxSoundParameter}

/** [[com.specdevs.specgine.assets.AssetDescriptor `AssetDescriptor`]] for
 * [[com.specdevs.specgine.assets.gdx.SoundAsset `SoundAsset`]].
 *
 * @constructor New instance of `SoundDescriptor`.
 */
case class SoundDescriptor() extends AssetDescriptorGdx {
  type T = GdxSound

  type P = GdxSoundParameter

  val extensions: List[String] = List("wav")

  val assetClass: Class[GdxSound] = classOf[GdxSound]

  def loaderParameters: GdxSoundParameter = new GdxSoundParameter

  def packAsset(asset: Any): Option[Asset] = asset match {
    case (gdxAsset: GdxSound) => Some(SoundAsset(gdxAsset))
    case _ => None
  }
}
