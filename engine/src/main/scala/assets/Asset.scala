package com.specdevs.specgine.assets

/** Trait that is supertype of all assets in SpecGine.
 *
 * It is universal trait, aimed to be extended with framework-specific
 * functionality.
 */
trait Asset extends Any {
  /** Method disposing asset. */
  def dispose(): Unit
}
