package com.specdevs.specgine.assets

/** That trait is supertype of all asset descriptors in SpecGine.
 *
 * It is used to determine what type and loading method should be used for
 * given asset type. Framework-specific implementations can extend this, ideally
 * with all loading options present in constructor.
 */
trait AssetDescriptor {
  /** List of file extensions used for guessing asset type.
   *
   * It is defined for each asset descriptor, without prefixing dot.
   */
  val extensions: List[String]
}
