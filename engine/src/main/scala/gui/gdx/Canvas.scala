package com.specdevs.specgine.gui.gdx

import com.specdevs.specgine.input.{KeyReceiver,PointerReceiver}

import com.badlogic.gdx.scenes.scene2d.{Stage => GdxStage}
import com.badlogic.gdx.scenes.scene2d.ui.{Table => GdxTable}
import com.badlogic.gdx.utils.viewport.{ExtendViewport => GdxExtendViewport}
import com.badlogic.gdx.utils.viewport.{ScreenViewport => GdxScreenViewport}
import com.badlogic.gdx.utils.viewport.{ScalingViewport => GdxScalingViewport}
import com.badlogic.gdx.utils.{Scaling => GdxScaling}

/** Trait used to place GUI widgets on.
 *
 * `Canvas` is mixed-in into classes requiring to build Graphical User Interfaces
 * based on LibGDX `Scene2d.ui`, including:
 *   - [[com.specdevs.specgine.states.gdx.GUISystem `GUISystem`]], class designed for
 *     easy integration of overlay GUI elements into game states based on
 *     entity systems as implemented in [[com.specdevs.specgine.states.GameState `GameState`]],
 *   - [[com.specdevs.specgine.states.gdx.MenuScreen `MenuScreen`]], class designed
 *     for easy creation of menus using [[com.specdevs.specgine.states.MenuState `MenuState`]].
 *
 * `Canvas` extends and implements [[com.specdevs.specgine.input.KeyReceiver `KeyReceiver`]]
 * and [[com.specdevs.specgine.input.PointerReceiver `PointerReceiver`]],
 * passing events to internally managed instance of `Stage`.
 * All building should happen inside `create` method, by adding widgets to
 * `Table` retuned by `table` method.
 *
 * Due to extensive use of bitmap fonts in GUIs, they are rendered
 * in screen coordinates. It means, that all sizes does not scale by
 * default. During `create` method optional width and height can be specified,
 * by using `canvasWidth` and `canvasHeight` setters. Those methods
 * describe "design size" of canvas. For example, if design width is set
 * to 800px, dimension of 100px will occupy 1/8 of screen no matter
 * what size it will actually be. Notice, that setting both
 * optional width and height during screen creation can lead to
 * unequal aspect ratio if actual screen proportion is different.
 *
 * @see [[https://github.com/libgdx/libgdx/wiki/Scene2d.ui ''LibGDX Wiki'']],
 *      for `Scene2d.ui` description.
 * @see [[https://github.com/EsotericSoftware/tablelayout ''TableLayout page'']],
 *      for `Table` manual.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/scenes/scene2d/Stage.html ''
 *      LibGDX API'']], for `Stage` description.
 * @see [[http://libgdx.com/nightlies/docs/api/com/badlogic/gdx/scenes/scene2d/ui/Table.html ''
 *      LibGDX API'']], for `Table` description.
 * @note Currently one has to use LibGDX syntax to build GUI, but this
 *       might change in near future, with introduction of GUI-building DSL.
 */
trait Canvas extends KeyReceiver with PointerReceiver {
  private var stage: Option[GdxStage] = None

  private var theTable: Option[GdxTable] = None

  private var width: Option[Float] = None

  private var height: Option[Float] = None

  /** Getter for design size width.
   *
   * @return Some with design size width in pixels if specified.
   */
  def canvasWidth: Option[Float] = {
    width
  }

  /** Setter for design size width.
   *
   * It makes sure to call `resizeCanvas` if needed.
   *
   * @param x Some with design size width in pixels, or None if non specified.
   */
  def canvasWidth_=(x: Option[Float]): Unit = {
    width = x
    if (stage.nonEmpty) {
      resizeCanvas(stage.get.getWidth.toInt, stage.get.getHeight.toInt)
    }
  }

  /** Getter for design size height.
   *
   * @return Some with design size height if specified.
   */
  def canvasHeight: Option[Float] = {
    height
  }

  /** Setter for design size height.
   *
   * It makes sure to call `resizeCanvas` if needed.
   *
   * @param y Some with design size height in pixels, or None if non specified.
   */
  def canvasHeight_=(y: Option[Float]): Unit = {
    height = y
    if (stage.nonEmpty) {
      resizeCanvas(stage.get.getWidth.toInt, stage.get.getHeight.toInt)
    }
  }

  /** Method returning instance of LibGDX `Table`, to which new widgets can be plugged in.
   *
   * This method can be used no earlier than inside `create`.
   * It always returns the same root table, that is set to fill
   * whole scene.
   *
   * @return Instance of LibGDX `Table`.
   */
  protected def table: GdxTable = {
    if (!theTable.isDefined) {
      throw new IllegalStateException("Cannot access table before it is created.")
    }
    theTable.get
  }

  /** Method initializing `Canvas`.
   *
   * It creates new instances of `Stage` and `Table` assigned to it.
   */
  protected def initializeCanvas(): Unit = {
    stage = Some(new GdxStage)
    theTable = Some(new GdxTable)
    theTable.get.setFillParent(true)
    stage.get.addActor(theTable.get)
    stage.get.setViewport((width, height) match {
      case (Some(x), Some(y)) => new GdxExtendViewport(x, y)
      case (None, None) => new GdxScreenViewport
      case (Some(x), None) => {
        val y = x*stage.get.getHeight/stage.get.getWidth
        new GdxScalingViewport(GdxScaling.fillX, x.toFloat, y)
      }
      case (None, Some(y)) => {
        val x = y*stage.get.getWidth/stage.get.getHeight
        new GdxScalingViewport(GdxScaling.fillY, x, y.toFloat)
      }
    })
  }

  /** Method disposing `Canvas`.
   *
   * It releases resources assigned by `Stage` and `Table`.
   */
  protected def disposeCanvas(): Unit = {
    theTable foreach (_.clearChildren())
    theTable = None
    stage.get.dispose()
    stage = None
  }

  /** Method for processing widgets on `Canvas`.
   *
   * @param dt Time simulation step.
   */
  protected def processCanvas(dt: Float): Unit = {
    stage.get.act(dt)
  }

  /** Method called when size of screen changes.
   *
   * It takes into account design size of `Canvas` set with
   * `canvasWidth` and `canvasHeight`.
   *
   * @param x Screen width.
   * @param y Screen height.
   */
  protected def resizeCanvas(x: Int, y: Int): Unit = {
    stage.get.getViewport().update(x, y, true)
  }

  /** Method for rendering widgets on `Canvas`.
   *
   * @param alpha Render interpolation between two processing steps.
   */
  protected def renderCanvas(alpha: Float): Unit = {
    stage.get.draw()
  }

  override def keyDown(keycode: Int): Boolean = {
    stage.get.keyDown(keycode)
  }

  override def keyUp(keycode: Int): Boolean = {
    stage.get.keyUp(keycode)
  }

  override def keyTyped(keycode: Char): Boolean = {
    stage.get.keyTyped(keycode)
  }

  override def touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
    stage.get.touchDown(x, y, pointer, button)
  }

  override def touchUp(x: Int, y: Int, pointer: Int, button: Int): Boolean = {
    stage.get.touchUp(x, y, pointer, button)
  }

  override def touchDragged(x: Int, y: Int, pointer: Int): Boolean = {
    stage.get.touchDragged(x, y, pointer)
  }

  override def mouseMoved(x: Int, y: Int): Boolean = {
    stage.get.mouseMoved(x, y)
  }

  override def scrolled(amount: Int): Boolean = {
    stage.get.scrolled(amount)
  }
}
