package com.specdevs.specgine.gui

/** Package providing GUI functionality in SpecGine based on LibGDX.
 *
 * Currently, [[com.specdevs.specgine.gui.gdx.Canvas `Canvas`]] is the only
 * class provided within this package. It contains utilities to easily
 * mix in GUI based on LibGDX `Scene2d.ui`.
 *
 * @see [[https://github.com/libgdx/libgdx/wiki/Scene2d.ui ''LibGDX Wiki'']],
 *      for `Scene2d.ui` description.
 */
package object gdx
