[SpecGine] is a cross-platform game engine made by [SpecDevs]. It is written
in Scala and is utilizing [LibGDX] to support multiple platforms.

[SpecGine]: http://bitbucket.org/specdevs/specgine "SpecGine repository"
[SpecDevs]: http://specdevs.com/ "SpecDevs group"
[LibGDX]: http://libgdx.badlogicgames.com/ "LibGDX framework"

