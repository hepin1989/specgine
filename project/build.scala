import sbt._
import Keys._

import sbtunidoc.Plugin._
import UnidocKeys._
import scoverage.ScoverageSbtPlugin._
import ScoverageKeys._
import org.scalastyle.sbt.ScalastylePlugin._

object Settings {
  lazy val testScalaStyle = taskKey[Unit]("testScalaStyle")

  lazy val testScalaStyleInTest = taskKey[Unit]("testScalaStyleInTest")

  lazy val testing = taskKey[Unit]("Run scalastyle and scoverage tests")

  lazy val verifyAlias = addCommandAlias("verify", ";testing;coverageAggregate")

  import SpecGineBuild.{libgdxVersion,scalatestVersion,sprayJsonVersion}

  lazy val common = plugins.JvmPlugin.projectSettings ++ unidocSettings ++ Seq(
    version := (version in LocalProject("all-platforms")).value,
    scalaVersion := (scalaVersion in LocalProject("all-platforms")).value,
    libgdxVersion := (libgdxVersion in LocalProject("all-platforms")).value,
    sprayJsonVersion := (sprayJsonVersion in LocalProject("all-platforms")).value,
    scalatestVersion := (scalatestVersion in LocalProject("all-platforms")).value,
    javacOptions ++= Seq("-encoding", "UTF-8", "-source", "1.6", "-target", "1.6", "-Xlint"),
    scalacOptions ++= Seq(
      "-encoding", "UTF-8", "-target:jvm-1.6", "-Xlint", "-unchecked", "-deprecation", "-feature",
      "-Ywarn-dead-code", "-Ywarn-value-discard", "-Ywarn-numeric-widen", "-Ywarn-unused", "-Ywarn-unused-import"
    ),
    resolvers += "spray" at "http://repo.spray.io/",
    libraryDependencies += "io.spray" %% "spray-json" % sprayJsonVersion.value % "optional",
    // test settings
    libraryDependencies += "org.scalatest" %% "scalatest" % scalatestVersion.value % "test",
    testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u", "target/test-reports"),
    parallelExecution in Test := false,
    coverageMinimum := 100,
    coverageExcludedPackages := ".*\\.gdx\\..*",
    testScalaStyle := (scalastyle in Compile).toTask("").value,
    testScalaStyleInTest := (scalastyle in Test).toTask("").value,
    testing in Test := {
      (clean, coverageReport dependsOn (test in Test) dependsOn coverage dependsOn testScalaStyleInTest dependsOn testScalaStyle) {
        (cleaning, tests) => cleaning doFinally tests
      }.value
    },
    // publish settings
    publishMavenStyle := true,
    homepage := Some(url("http://specdevs.bitbucket.org/specgine/")),
    organization := "com.specdevs",
    organizationName := "SpecDevs",
    organizationHomepage := Some(url("http://specdevs.com/")),
    startYear := Some(2013),
    licenses := Seq(
      "The BSD 2-Clause License"->url("http://www.opensource.org/licenses/bsd-license.php")
    ),
    scmInfo := Some(ScmInfo(
      url("http://bitbucket.org/specdevs/specgine/"),
      "scm:git:https://bitbucket.org/specdevs/specgine.git",
      Some("scm:git:ssh://git@bitbucket.org:specdevs/specgine.git")
    )),
    pomExtra := Seq(
      <developers>
        <developer>
          <id>aginiewicz</id>
          <name>Andrzej Giniewicz</name>
          <url>https://bitbucket.org/aginiewicz</url>
          <email>Andrzej.Giniewicz@specdevs.com</email>
        </developer>
        <developer>
          <id>pkupczyk</id>
          <name>Piotr Kupczyk</name>
          <url>https://bitbucket.org/pkupczyk</url>
          <email>Piotr.Kupczyk@specdevs.com</email>
        </developer>
        <developer>
          <id>martabech</id>
          <name>Marta Bech</name>
          <url>https://bitbucket.org/martabech</url>
          <email>Marta.Bech@specdevs.com</email>
        </developer>
      </developers>
    ),
    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (version.value.trim.endsWith("SNAPSHOT")) {
        Some("snapshots" at nexus + "content/repositories/snapshots")
      } else {
        Some("releases"  at nexus + "service/local/staging/deploy/maven2")
      }
    },
    resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
    // doc settings
    scalacOptions in (ScalaUnidoc, unidoc) ++= Seq("-Ymacro-expand:none", "-doc-root-content", "rootdoc.txt"),
    doc in Compile := (doc in ScalaUnidoc).value,
    target in unidoc in ScalaUnidoc := crossTarget.value/"api",
    apiMappings += {
      scalaInstance.value.libraryJar -> url(s"http://www.scala-lang.org/api/${scalaVersion.value}/")
    },
    apiURL := {
      if (version.value.trim.endsWith("SNAPSHOT")) {
        None
      } else {
        Some(url("http://specdevs.bitbucket.org/specgine/api/"+version.value.toString))
      }
    },
    scalacOptions in (Compile, doc) ++= {
      val base = "https://bitbucket.org/specdevs/specgine/src/"
      val tag = if(version.value.trim.endsWith("SNAPSHOT")) "HEAD" else "v"+version.value.toString
      Seq("-sourcepath", baseDirectory.value.getAbsolutePath, "-doc-source-url", base+tag+"€{FILE_PATH}.scala")
    }
  )

  lazy val engine = common ++ Seq(
    name := "SpecGine",
    normalizedName := "specgine",
    description := "Scala game engine based on LibGDX framework",
    libraryDependencies ++= Seq(
      "com.badlogicgames.gdx" % "gdx" % libgdxVersion.value,
      "com.badlogicgames.gdx" % "gdx-backend-headless" % libgdxVersion.value % "test",
      "com.badlogicgames.gdx" % "gdx-platform" % libgdxVersion.value % "test" classifier "natives-desktop"
    )
  )

  lazy val macros = common ++ Seq(
    name := "SpecGine (macros)",
    normalizedName := "specgine-macros",
    description := "Optional macros package for SpecGine game engine",
    libraryDependencies <+= (scalaVersion)("org.scala-lang" % "scala-reflect" % _)
  )

  lazy val all = common ++ verifyAlias ++ Seq(
    name := "specgine-all",
    publish := {},
    publishArtifact in (Compile, packageBin) := false,
    publishArtifact in (Compile, packageSrc) := false,
    publishMavenStyle := false
  )
}

object SpecGineBuild extends Build {
  lazy val libgdxVersion = settingKey[String]("version of LibGDX library")

  lazy val sprayJsonVersion = settingKey[String]("version of spray-json library")

  lazy val scalatestVersion = settingKey[String]("version of ScalaTest library")

  lazy val engine = Project(
    "engine",
    file("engine"),
    settings = Settings.engine)

  lazy val macros = Project(
    "macros",
    file("macros"),
    settings = Settings.macros)
    .dependsOn(engine % "test->test;compile->compile")

  lazy val all = Project(
    "all-platforms",
    file("."),
    settings = Settings.all)
    .aggregate(engine, macros)
}
