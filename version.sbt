version := "0.2-SNAPSHOT"

scalaVersion := "2.11.4"

libgdxVersion := "1.4.1"

sprayJsonVersion := "1.3.1"

scalatestVersion := "2.2.1"
