package com.specdevs.specgine.macros.test

import com.specdevs.specgine.macros.gdx.assets.handleJson
import com.specdevs.specgine.assets.gdx.JsonAsset

import com.specdevs.specgine.test._

import org.scalatest.{FunSpec,Matchers}

import spray.json._
import DefaultJsonProtocol._

class AssetIntegration extends FunSpec with Matchers {
  object PositionProtocole extends DefaultJsonProtocol {
    implicit val positionFormat = jsonFormat2(Position)
  }

  case class Animation(val id: String, val bones: Array[Int])
  case class TestAnimation(val version: Array[Int], val id: String, val animations: Array[Animation])

  object TestAnimationProtocol extends DefaultJsonProtocol {
    implicit val animationFromat = jsonFormat2(Animation)
    implicit val testFormat = jsonFormat3(TestAnimation)
  }

  describe("Access macro") {
    it("should compile for basic types") {
      assertCompiles("handleJson[Int]")
      assertCompiles("handleJson[Float]")
      assertCompiles("handleJson[String]")
      assertCompiles("handleJson[Char]")
      assertCompiles("handleJson[Double]")
      assertCompiles("handleJson[Option[Int]]")
      assertCompiles("handleJson[Tuple2[Float,String]]")
      assertCompiles("handleJson[Tuple3[Float,String,Int]]")
      assertCompiles("handleJson[Tuple4[Float,String,Int,Char]]")
      assertCompiles("handleJson[Tuple5[Float,String,Int,Char,Double]]")
      assertCompiles("handleJson[List[Int]]")
      assertCompiles("handleJson[Map[Int,Double]]")
      assertCompiles("handleJson[Array[String]]")
    }

    it("should compile for custom case class with implemented protocol") {
      import PositionProtocole._
      assertCompiles("handleJson[Position](positionFormat)")
    }

    it("should not compile for a custom case class without implemented protocol") {
      case class DummyCaseClass(val i: Int, val s: String, val c: Char, val f: Float)
      assertDoesNotCompile("handleJson[DummyCaseClass]")
    }

    it("should unpack Json to Position") {
      import PositionProtocole._
      val handler = handleJson[Position]
      val pos = Position(3, 14)
      handler.unpack(Some(JsonAsset(JsonParser("""{"x":3,"y":14}""")))) should equal (Some(pos))
      handler.unpack(Some(handler.pack(pos))) should equal (Some(pos))
    }

    it("should handle nested case classes") {
      import TestAnimationProtocol._
      val handler = handleJson[TestAnimation]
      val animation = Animation("Wu Kong", Array(3,14))
      val testAnimation = TestAnimation(Array(0,1),"Braum",Array(animation))
      val jsonString =
        """{
          "version": [  0,   1],
          "id": "Braum",
          "animations": [
            {
              "id": "Wu Kong",
              "bones": [  3,   14]
            }
          ]
        }"""
      val testAnimationFromJsonString = handler.unpack(Some(JsonAsset(JsonParser(jsonString)))).get
      testAnimationFromJsonString.version should equal (Array(0,1))
      testAnimationFromJsonString.id should equal ("Braum")
      val testAnimationFromJsonStringAnimations = testAnimationFromJsonString.animations(0)
      testAnimationFromJsonStringAnimations.id should equal ("Wu Kong")
      testAnimationFromJsonStringAnimations.bones should equal (Array(3,14))
      val testAnimationFromPackedType = handler.unpack(Some(handler.pack(testAnimation))).get
      testAnimationFromPackedType.version should equal (Array(0,1))
      testAnimationFromPackedType.id should equal ("Braum")
      val testAnimationFromPackedTypeAnimations = testAnimationFromJsonString.animations(0)
      testAnimationFromPackedTypeAnimations.id should equal ("Wu Kong")
      testAnimationFromPackedTypeAnimations.bones should equal (Array(3,14))
    }
  }
}
