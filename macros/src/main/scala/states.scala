package com.specdevs.specgine.macros

import com.specdevs.specgine.states.{ComponentsManager,ComponentsSpec}

import scala.language.experimental.macros
import scala.reflect.macros.blackbox.Context

// $COVERAGE-OFF$Disable coverage reports of macro invocation.

/** Macro package for [[com.specdevs.specgine.states]].
 *
 * This package provides macro `withManager` to automate creation of
 * [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]] and
 * thus, to simplify configuration of
 * [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]].
 * There are two types of information one can provide to the macro:
 *   - [[com.specdevs.specgine.macros.states.FieldDescription `FieldDescription`]]
 *     instances should be used in
 *     [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]] to
 *     describe fields. They will be used to access components from system.
 *     All instances behave like simplified `Map` from
 *     [[com.specdevs.specgine.states.Entity `Entity`]] to appropriate
 *     [[com.specdevs.specgine.states.Component `Component`]] or container of
 *     [[com.specdevs.specgine.states.Component `Component`]]s. There needs to
 *     be at least one
 *     [[com.specdevs.specgine.macros.states.FieldDescription `FieldDescription`]].
 *   - [[com.specdevs.specgine.macros.states.ComponentsRule `ComponentsRule`]]s
 *     are annotations that can be added to
 *     [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]],
 *     to provide more detailed information about what type of entities will
 *     be processed by given system. This is also the only way to specify which
 *     entities will not be processed. This type of information is optional.
 *
 * Below we present example components specification which uses both
 * [[com.specdevs.specgine.macros.states.FieldDescription `FieldDescription`]]s and
 * [[com.specdevs.specgine.macros.states.ComponentsRule `ComponentsRule`]]:
 * {{{
 *   @NoneOf2[Dead,Paralyzed]
 *   class MovementSpec extends ComponentsSpec {
 *     val pos = new Needed[Position]
 *     val vel = new Needed[Velocity]
 *   }
 * }}}
 * Above class can be used with macro:
 * {{{
 * val components: MovementSpec with ComponentsManager = withManager[MovementSpec]
 * //...
 * override def process(dt: Float, e: Entity): Unit = {
 *   components.pos(e).x += components.vel(e).x * dt
 *   components.pos(e).y += components.vel(e).y * dt
 * }
 * }}}
 * Macro `withManager` will automatically write optimized version of all
 * required [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]s
 * methods. [[com.specdevs.specgine.states.EntitySystem `EntitySystem`]]
 * filled with such `components` definition will accept all entities that
 * are associated with components `Position` and `Velocity`, but not with
 * `Dead` or `Paralyzed` components.
 */
package object states {
  import StatesImpl._

  /** Macro automatically providing [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]]
   * based on [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]].
   *
   * Implementation is automatically optimized, and statically checked for
   * correctness at compile time.
   *
   * @tparam T subtype of [[com.specdevs.specgine.states.ComponentsSpec `ComponentsSpec`]], containing specification.
   * @return instance of `T` with [[com.specdevs.specgine.states.ComponentsManager `ComponentsManager`]].
   */
  def withManager[T <: ComponentsSpec]: T with ComponentsManager = macro withManagerImpl[T]
}

// $COVERAGE-ON$

private object StatesImpl {
  class StatesHelper[C <: Context](val c: C) {
    import c.universe._

    private val com = Ident(TermName("com"))

    private val specdevs = Select(com, TermName("specdevs"))

    private val specgine = Select(specdevs, TermName("specgine"))

    private val states = Select(specgine, TermName("states"))

    private val componentsManager = Select(states, TypeName("ComponentsManager"))

    private val entity = Select(states, TypeName("Entity"))

    private val component = Select(states, TypeName("Component"))

    private type Field = (TermName, Boolean, Name)

    private type Rules = (List[List[Name]], List[Name])

    private def getMethods(theClass: ClassSymbol): List[MethodSymbol] = {
      def valid(method: MethodSymbol): Boolean = {
        val constructor = method.returnType.typeConstructor
        constructor.baseClasses map (_.name) contains TypeName("FieldDescription")
      }
      val memberScope = theClass.typeSignature.decls.sorted
      val publicMethods = memberScope filter (symbol => symbol.isPublic && symbol.isMethod)
      publicMethods.map(_.asMethod) filter valid
    }

    private def checkMethod(validName: String => Boolean)(method: MethodSymbol): Boolean = {
      validName(method.returnType.typeConstructor.typeSymbol.name.decodedName.toString)
    }

    private def getAnnotations(theClass: ClassSymbol): List[Annotation] = {
      def valid(annotation: Annotation): Boolean = {
        val constructor = annotation.tree.tpe.typeConstructor
        constructor.baseClasses map (_.name) contains TypeName("ComponentsRule")
      }
      theClass.annotations filter valid
    }

    private def checkAnnotation(validName: String => Boolean)(annotation: Annotation): Boolean = {
      validName(annotation.tree.tpe.typeConstructor.typeSymbol.name.decodedName.toString)
    }

    private def getFromAnnotation(annotation: Annotation): List[Name] = {
      val TypeRef(_, _, args) = annotation.tree.tpe
      args map (_.typeSymbol.name)
    }

    private def optimize[A](trueRules: List[List[A]], falseRules: List[A]): (List[List[A]], List[A]) = {
      def withoutSupersets(in: Set[Set[A]]): Set[Set[A]] = {
        def properSuperset(x: Set[A])(y: Set[A]): Boolean = {
          (x != y) && (x subsetOf y)
        }
        in.foldLeft(in)((acc, x) => acc filterNot properSuperset(x))
      }
      val out = falseRules.toSet
      val in = trueRules.map(_.toSet diff out).toSet
      val optimiesdIn = withoutSupersets(in).map(_.toList).toList.sortBy(_.length)
      val optimisedOut = out.toList
      (optimiesdIn, optimisedOut)
    }

    private def createConstructor: Tree = {
      val superConstructor = Select(Super(This(typeNames.EMPTY), typeNames.EMPTY), termNames.CONSTRUCTOR)
      val constructorBody = Block(List(Apply(superConstructor, Nil)), Literal(Constant(())))
      DefDef(NoMods, termNames.CONSTRUCTOR, Nil, List(Nil), TypeTree(), constructorBody)
    }

    private def createHasMethod(allField: Option[TermName], fields: List[Field]): Tree = {
      val hasCheck = allField.getOrElse(fields.head._1)
      val hasField = Select(This(TypeName("$anon")), hasCheck)
      val hasBody = Apply(Select(hasField, TermName("isDefinedAt")), List(Ident(TermName("e"))))
      val hasParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("e"), entity, EmptyTree)
      ))
      val hasType = Ident(TypeName("Boolean"))
      DefDef(NoMods, TermName("has"), Nil, hasParams, hasType, hasBody)
    }

    private def createRemoveMethod(allField: Option[TermName], fields: List[Field]): Tree = {
      def removeCommand(entity: TermName)(name: TermName): Apply = {
        val ourObject = This(TypeName("$anon"))
        val field = Select(ourObject, name)
        val removeMethod = Select(field, TermName("remove"))
        Apply(removeMethod, List(Ident(entity)))
      }
      val fieldsToRemove = if (allField.isDefined) {
        fields.map(_._1) :+ allField.get
      } else {
        fields.map(_._1)
      }
      val removeCommands = fieldsToRemove.map(removeCommand(TermName("e"))(_))
      val removeBody = Block(removeCommands, Literal(Constant(())))
      val removeParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("e"), entity, EmptyTree)
      ))
      val removeType = Ident(TypeName("Unit"))
      DefDef(NoMods, TermName("remove"), Nil, removeParams, removeType, removeBody)
    }

    private def createAddComponentMethod(fields: List[Field]): Tree = {
      def valueForAdd(option: Boolean): Tree = {
        val value = Ident(TermName("v"))
        if (option) {
          Apply(Select(Ident(TermName("Some")), TermName("apply")), List(value))
        } else {
          value
        }
      }
      def updateForAdd(name: TermName, option: Boolean): Tree = {
        val updateMethod = Select(Select(This(TypeName("$anon")), name), TermName("update"))
        Apply(updateMethod, List(Ident(TermName("e")), valueForAdd(option)))
      }
      def caseDefForAdd(singleCase: Field): CaseDef = {
        val (fieldName, option, fieldType) = singleCase
        CaseDef(
          Bind(TermName("v"), Typed(Ident(termNames.WILDCARD), Ident(fieldType))),
          EmptyTree,
          updateForAdd(fieldName, option)
        )
      }
      val caseDefsForAdd = fields.map(caseDefForAdd(_))
      val defaultCase = CaseDef(Ident(termNames.WILDCARD), EmptyTree, Literal(Constant(())))
      val addComponentBody = Match(Ident(TermName("c")), caseDefsForAdd :+ defaultCase)
      val addComponentParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("e"), entity, EmptyTree),
        ValDef(Modifiers(Flag.PARAM), TermName("c"), component, EmptyTree)
      ))
      val addComponentType = Ident(TypeName("Unit"))
      DefDef(
        Modifiers(Flag.PRIVATE), TermName("addComponent"), Nil,
        addComponentParams, addComponentType, addComponentBody
      )
    }

    private def createInitOptionalMethod(fields: List[Field]): Tree = {
      def updateForInitOptional(name: TermName): Tree = {
        val updateMethod = Select(Select(This(TypeName("$anon")), name), TermName("update"))
        Apply(updateMethod, List(Ident(TermName("e")), Ident(TermName("None"))))
      }
      val initOptionalFields = fields.filter(_._2).map(_._1)
      val initOptionalCalls = initOptionalFields.map(updateForInitOptional(_))
      val initOptionalBody = Block(initOptionalCalls, Literal(Constant(())))
      val initOptionalParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("e"), entity, EmptyTree)
      ))
      val initOptionalType = Ident(TypeName("Unit"))
      DefDef(
        Modifiers(Flag.PRIVATE), TermName("initOptional"), Nil,
        initOptionalParams, initOptionalType, initOptionalBody
      )
    }

    private def wantsDef(fieldType: Name): DefDef = {
      val wantsDefParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("c"), component, EmptyTree)
      ))
      val wantsDefBody = Match(Ident(TermName("c")), List(
        CaseDef(Typed(Ident(termNames.WILDCARD), Ident(fieldType)), EmptyTree, Literal(Constant(true))),
        CaseDef(Ident(termNames.WILDCARD), EmptyTree, Literal(Constant(false)))
      ))
      val wantsDefType = Ident(TypeName("Boolean"))
      DefDef(NoMods, TermName("is"+fieldType), Nil, wantsDefParams, wantsDefType, wantsDefBody)
    }

    private def wantsApply(fieldType: Name): Apply = {
      val container = Ident(TermName("cs"))
      val exists = Select(container, TermName("exists"))
      val predicate = Function(List(
        ValDef(Modifiers(Flag.PARAM), TermName("c"), TypeTree(), EmptyTree)
      ), Apply(Ident(TermName("is"+fieldType)), List(Ident(TermName("c")))))
      Apply(exists, List(predicate))
    }

    private def createWantsMethod(rules: Rules): Tree = {
      val wantsDefs = (rules._1.flatten ++ rules._2).toSet.toList.map((x: Name) => wantsDef(x))
      val callWantsTrue = if (rules._1.length > 0) {
        val callsWantsTrue = rules._1.map(x=>x map (wantsApply(_)))
        val callWantsTrueIn = callsWantsTrue.map(x => x.reduceLeft((acc,el) =>
          Apply(Select(acc, TermName("$bar$bar")), List(el))))
        callWantsTrueIn.reduceLeft((acc,el) =>
          Apply(Select(acc, TermName("$amp$amp")), List(el)))
      } else {
        Literal(Constant(true))
      }
      val callWants = if (rules._2.length == 0) {
        callWantsTrue
      } else {
        val callsWantsFalse = rules._2.map(wantsApply(_))
        val callWantsFalse = callsWantsFalse.reduceLeft((acc,el) =>
          Apply(Select(acc, TermName("$bar$bar")), List(el)))
        val callWantsNotFalse = Select(callWantsFalse, TermName("unary_$bang"))
        if (rules._1.length > 0) {
          Apply(Select(callWantsTrue, TermName("$amp$amp")), List(callWantsNotFalse))
        } else {
          callWantsNotFalse
        }
      }
      val wantsBody = Block(wantsDefs, callWants)
      val wantsParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("cs"),
          AppliedTypeTree(Select(Ident(TermName("Predef")), TypeName("Set")),
          List(component)), EmptyTree)
      ))
      val wantsType = Ident(TypeName("Boolean"))
      DefDef(NoMods, TermName("wants"), Nil, wantsParams, wantsType, wantsBody)
    }

    private def createAddMethod(allField: Option[TermName]): Tree = {
      val addComponent = Select(This(TypeName("$anon")), TermName("addComponent"))
      val addFunctionBody = Apply(addComponent, List(Ident(TermName("e")), Ident(TermName("c"))))
      val addFunctionParam = ValDef(Modifiers(Flag.PARAM), TermName("c"), component, EmptyTree)
      val addFunction = Function(List(addFunctionParam), addFunctionBody)
      val addForeach = Apply(Select(Ident(TermName("cs")), TermName("foreach")), List(addFunction))
      val initOptional = Select(This(TypeName("$anon")), TermName("initOptional"))
      val addInit = Apply(initOptional, List(Ident(TermName("e"))))
      val addBody = if (allField.isDefined) {
        val updateMethod = Select(Select(This(TypeName("$anon")), allField.get), TermName("update"))
        val addAll = Apply(updateMethod, List(Ident(TermName("e")), Ident(TermName("cs"))))
        Block(List(addInit, addAll), addForeach)
      } else {
        Block(List(addInit), addForeach)
      }
      val addParams = List(List(
        ValDef(Modifiers(Flag.PARAM), TermName("e"), entity, EmptyTree),
        ValDef(Modifiers(Flag.PARAM), TermName("cs"),
          AppliedTypeTree(Select(Ident(TermName("Predef")), TypeName("Set")),
          List(component)), EmptyTree)
      ))
      val addType = Ident(TypeName("Unit"))
      DefDef(NoMods, TermName("add"), Nil, addParams, addType, addBody)
    }

    def findClass(className: Name): ClassSymbol = {
      val probe = Typed(Ident(TermName("$qmark$qmark$qmark")), Ident(className))
      c.typecheck(probe).tpe.typeSymbol.asClass
    }

    def extractFieldsFromClass(theClass: ClassSymbol): List[Field] = {
      def parseMethod(method: MethodSymbol): Field = {
        val fieldName = method.name.toTermName
        val optional = checkMethod(_ == "Optional")(method)
        val TypeRef(_, _, args) = method.returnType
        val typeName = args.head.typeSymbol.name
        (fieldName, optional, typeName)
      }
      val methods = getMethods(theClass) filter checkMethod(m => m == "Needed" || m == "Optional")
      methods map (m => parseMethod(m))
    }

    def extractAllFieldFromClass(theClass: ClassSymbol): Option[TermName] = {
      val methods = getMethods(theClass) filter checkMethod(_ == "All")
      val fields = methods map (_.name.toTermName)
      if (fields.length > 1) {
        c.abort(c.enclosingPosition, "All descriptor can occur only once")
      }
      fields.headOption
    }

    def extractRulesFromClass(theClass: ClassSymbol): List[(Int, List[Name])] = {
      val annotations = getAnnotations(theClass)
      val trueAnnotations = annotations filter checkAnnotation(a => a == "The" || a.startsWith("AllOf"))
      val falseAnnotations = annotations filter checkAnnotation(a => a == "No" || a.startsWith("NoneOf"))
      val someAnnotations = annotations filter checkAnnotation(_.startsWith("OneOf"))
      val trueRules = trueAnnotations map (a => (1, getFromAnnotation(a)))
      val someRules = someAnnotations map (a => (0, getFromAnnotation(a)))
      val falseRules = falseAnnotations map (a => (-1, getFromAnnotation(a)))
      falseRules ++ someRules ++ trueRules
    }

    def mergeRulesWithFields(rules: List[(Int, List[Name])], fields: List[Field]): Rules = {
      val trueFromFields = fields flatMap {
        case (_, false, name) => List(List(name))
        case _ => Nil
      }
      val trueFromRules = rules flatMap {
        case (1, names) => names map (name => List(name))
        case (0, names) => List(names)
        case _ => Nil
      }
      val falseFromRules = rules flatMap {
        case (-1, names) => names
        case _ => Nil
      }
      optimize(trueFromRules ++ trueFromFields, falseFromRules)
    }

    def checkRulesAndFields(rules: Rules, fields: List[Field], allField: Option[TermName]): Unit = {
      val sureIn = rules._1.filter(_.length == 1).flatten.toSet
      val sure = sureIn ++ rules._2
      val needed = fields.filter(!_._2).map(_._3).toSet
      val optional = fields.filter(_._2).map(_._3).toSet
      val correct = needed subsetOf sureIn
      val optimal = (optional intersect sure).isEmpty
      if (!correct) {
        c.abort(c.enclosingPosition, "needed field cannot be excluded by rules")
      }
      if (fields.isEmpty && allField.isEmpty) {
        c.abort(c.enclosingPosition, "specifications without fields are unsupported")
      }
      if (!optimal) {
        c.abort(
          c.enclosingPosition,
          "optional field is always present or always absent, remove or change into needed field"
        )
      }
    }

    def createMethods(className: Name, allField: Option[TermName], fields: List[Field], rules: Rules): List[Tree] = {
      val constructor = createConstructor
      val has = createHasMethod(allField, fields)
      val remove = createRemoveMethod(allField, fields)
      val addComponent = createAddComponentMethod(fields)
      val initOptional = createInitOptionalMethod(fields)
      val wants = createWantsMethod(rules)
      val add = createAddMethod(allField)
      List(constructor, addComponent, wants, initOptional, has, remove, add)
    }

    def createClass(className: Name, methods: List[Tree]): Tree = {
      val parents = List(Ident(className), componentsManager)
      val template = Template(parents, noSelfType, methods)
      ClassDef(Modifiers(Flag.FINAL), TypeName("$anon"), Nil, template)
    }
  }

  def withManagerImpl[T: c.WeakTypeTag](c: Context): c.Expr[T with ComponentsManager] = {
    import c.universe._
    val helper = new StatesHelper[c.type](c)
    val theType: Type = weakTypeOf[T]
    val className = theType.typeSymbol.name
    val theClass = helper.findClass(className)
    val fields = helper.extractFieldsFromClass(theClass)
    val allField = helper.extractAllFieldFromClass(theClass)
    val rawRules = helper.extractRulesFromClass(theClass)
    val rules = helper.mergeRulesWithFields(rawRules, fields)
    helper.checkRulesAndFields(rules, fields, allField)
    val methods = helper.createMethods(className, allField, fields, rules)
    val classDef = helper.createClass(className, methods)
    val classInit = Apply(Select(New(Ident(TypeName("$anon"))), termNames.CONSTRUCTOR), Nil)
    val generatedCode = Block(List(classDef), classInit)
    c.Expr[T with ComponentsManager](generatedCode)
  }
}
