package com.specdevs.specgine.macros

import com.specdevs.specgine.core.Accessor

import scala.language.experimental.macros
import scala.reflect.macros.blackbox.Context

// $COVERAGE-OFF$Disable coverage reports of macro invocation.

/** Macro package for [[com.specdevs.specgine.core]].
 *
 * This package provides macro `access` to automate creation of
 * [[com.specdevs.specgine.core.Accessor `Accessor`]] based on any mutable
 * `var`, class field or another accessor implemented in class.
 * It does not work for variables local to function, because they can
 * disappear as soon as the function finishes.
 *
 * Its main purpose is to automate creation of tween variables, but its
 * uses does not have to be limited to that.
 *
 * Below we present a simple example of how to use `access` macro:
 * {{{
 *   case class Test(var field: Int)
 *   val caseClass = Test(2)
 *   var variable = 1
 *   val accessField = access(caseClass.field)
 *   val accessVariable = access(variable)
 *   accessField.value = 3
 *   accessVariable.value = 3
 *   accessVariable.value == accessField.value // both are equal to 3
 *   variable == caseClass.field // also original mutable variable and field are equal to 3
 * }}}
 */
object core {
  import CoreImpl._

  /** Macro automatically providing [[com.specdevs.specgine.core.Accessor `Accessor`]]
   * based on mutable value.
   *
   * @tparam A Type of mutable value.
   * @param value Reference to mutable value.
   * @return Instance of [[com.specdevs.specgine.core.Accessor `Accessor`]] to mutable value.
   */
  def access[A](value: A): Accessor[A] = macro accessImpl[A]
}

// $COVERAGE-ON$

private object CoreImpl {
  class CoreHelper[C <: Context](val c: C) {
    import c.universe._

    private val com = Ident(TermName("com"))

    private val specdevs = Select(com, TermName("specdevs"))

    private val specgine = Select(specdevs, TermName("specgine"))

    private val core = Select(specgine, TermName("core"))

    private val accessor = Select(core, TypeName("Accessor"))

    private val className = TypeName("$anon")

    private val classInit: Tree = Apply(Select(New(Ident(className)), termNames.CONSTRUCTOR), Nil)

    private def typeTree(theType: Type): Tree = {
      Ident(theType.typeSymbol.name)
    }

    private def parents(theType: Type): List[Tree] = {
      List(AppliedTypeTree(accessor, List(typeTree(theType))))
    }

    private val constructor: Tree = {
      val superConstructor = Select(Super(This(typeNames.EMPTY), typeNames.EMPTY), termNames.CONSTRUCTOR)
      val constructorBody = Block(List(Apply(superConstructor, Nil)), Literal(Constant(())))
      DefDef(NoMods, termNames.CONSTRUCTOR, Nil, List(Nil), TypeTree(), constructorBody)
    }

    private def getter(theType: Type, theValue: Tree): Tree = {
      DefDef(NoMods, TermName("value"), Nil, Nil, typeTree(theType), theValue)
    }

    private def setter(theType: Type, theValue: Tree): Tree = {
      val setValue = theValue match {
        case Select(path, name) => Select(path,TermName(name.encodedName.toString+"_$eq"))
        case _ => {
          c.abort(c.enclosingPosition, "Argument you want to use for accessor does not look like class variable")
        }
      }
      val parameters = List(List(ValDef(Modifiers(Flag.PARAM), TermName("what"), typeTree(theType), EmptyTree)))
      val setterType = Ident(TypeName("Unit"))
      val setterBody = Apply(setValue, List(Ident(TermName("what"))))
      DefDef(NoMods, TermName("value_$eq"), Nil, parameters, setterType, setterBody)
    }

    private def methods(theType: Type, theValue: Tree): List[Tree] = {
      List(constructor, getter(theType, theValue), setter(theType, theValue))
    }

    private def classTemplate(theType: Type, theValue: Tree): Template = {
      Template(parents(theType), noSelfType, methods(theType, theValue))
    }

    private def classDef(theType: Type, theValue: Tree): Tree = {
      ClassDef(Modifiers(Flag.FINAL), className, Nil, classTemplate(theType, theValue))
    }

    def generateClass(theType: Type, theValue: Tree): Tree = {
      Block(List(classDef(theType, theValue)), classInit)
    }
  }

  def accessImpl[A: c.WeakTypeTag](c: Context)(value: c.Expr[A]): c.Expr[Accessor[A]] = {
    import c.universe._
    val helper = new CoreHelper[c.type](c)
    val theType: Type = weakTypeOf[A]
    val theValue: Tree = value.tree
    c.Expr[Accessor[A]](helper.generateClass(theType, theValue))
  }
}
